<?php
/**
 * @author Ovunc Tukenmez <ovunct@live.com>
 *
 * This class is used to get translated strings from the MYSQL database.
 * Logging must be enabled for the table in order to track changes.
 * It class creates triggers to store current state of the inserted/updated/deleted row in the time of change.
 * Also a stored procedure for the table is created to make possible to get previous state of the rows before the specified time span defined with start_date and end_date parameters.
 */
require_once(dirname(__FILE__) . '/config-multilanguage.php');

class PHPMLC
{
    private $_conn;
    private $_db_host;
    private $_db_name;
    private $_db_username;
    private $_db_password;
    private $_driver_options;
    private $_last_error_msg;
    private $_table_prefix = 'ml_';
    private $_translation_keys;
    private $_translated_strings;
    private $_languages;
    private $_language_ids;
    private $_current_language_id;
    private $_current_language_code;

    /**
     * @param       $db_host database host
     * @param       $db_name database name
     * @param       $db_username database user name
     * @param       $db_password database user password
     * @param array $driver_options PDO driver options
     */
    public function __construct(
        $db_host = CONN_HOST,
        $db_name = CONN_DATABASE,
        $db_username = CONN_USER,
        $db_password = CONN_PASSWORD,
        $driver_options = array()
    ) {
        $this->setConnectionDetails($db_host, $db_name, $db_username, $db_password, $driver_options);
    }

    /**
     * Sets connection details
     * @param       $db_host database host
     * @param       $db_name database name
     * @param       $db_username database user name
     * @param       $db_password database user password
     * @param array $driver_options PDO driver options
     */
    public function setConnectionDetails($db_host, $db_name, $db_username, $db_password, $driver_options = array())
    {
        $this->_db_host        = $db_host;
        $this->_db_name        = $db_name;
        $this->_db_username    = $db_username;
        $this->_db_password    = $db_password;
        $this->_driver_options = $driver_options;
        $this->_conn           = null;
        $this->_last_error_msg = '';

        $this->_translation_keys      = null;
        $this->_translated_strings    = null;
        $this->_languages             = null;
        $this->_language_ids          = null;
        $this->_current_language_id   = null;
        $this->_current_language_code = null;

        $this->autoSelectCurrentLanguage();
    }

    private function getDBConn()
    {
        if (!is_a($this->_conn, 'PDO')) {
            $dsn = 'mysql:dbname=' . $this->_db_name . ';charset=utf8;host=' . $this->_db_host;

            try {
                $this->_conn = new PDO($dsn, $this->_db_username, $this->_db_password, $this->_driver_options);
                // $this->_conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
                $this->_conn->exec("SET CHARACTER SET utf8");
                return $this->_conn;
            } catch (PDOException $e) {
                $this->_last_error_msg = 'Connection failed: ' . $e->getMessage();
                return false;
            }
        } else {
            return $this->_conn;
        }
    }

    /**
     * returns last error messages
     *
     */
    public function getLastErrorMessage()
    {
        return $this->_last_error_msg;
    }

    /**
     * creates tables to store translation data
     */
    public function createTables()
    {
        $q = <<<EOF
CREATE TABLE IF NOT EXISTS {$this->_table_prefix}languages (
id int(11) UNSIGNED NOT NULL auto_increment,
name varchar(50) NOT NULL default '',
language_code varchar(20) NOT NULL default '',
is_default tinyint(1) NOT NULL DEFAULT 0,
display_order tinyint UNSIGNED NOT NULL default 0,
is_deleted tinyint(1) NOT NULL DEFAULT 0,
PRIMARY KEY (id),
KEY (is_default),
KEY (display_order),
KEY (is_deleted)
) ENGINE=MyISAM CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->_table_prefix}translation_keys (
id int(11) UNSIGNED NOT NULL auto_increment,
label varchar(100) NOT NULL default '',
is_deleted tinyint(1) NOT NULL DEFAULT 0,
PRIMARY KEY (id),
UNIQUE KEY (label),
KEY (is_deleted)
) ENGINE=MyISAM CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->_table_prefix}translation_values (
id int(11) UNSIGNED NOT NULL auto_increment,
translation_key_id int(11) UNSIGNED NOT NULL default 0,
language_id int(11) UNSIGNED NOT NULL default 0,
value text,
is_deleted tinyint(1) NOT NULL DEFAULT 0,
PRIMARY KEY (id),
KEY (translation_key_id),
KEY (is_deleted)
) ENGINE=MyISAM CHARSET=utf8;
EOF;

        $stmt = $this->getDBConn()->query($q);
        if (!$stmt) {
            $this->_last_error_msg = implode(' ', $this->getDBConn()->errorInfo());
            return false;
        } else {
            $stmt->closeCursor();
            //return true;
        }

        //insert default language if there is no record in the database
        $q = <<<EOF
SELECT
COUNT(1)
FROM {$this->_table_prefix}languages
WHERE
is_deleted = 0
EOF;

        $stmt = $this->getDBConn()->query($q);
        if (!$stmt) {
            $this->_last_error_msg = implode(' ', $this->getDBConn()->errorInfo());
            return false;
        } else {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            $stmt->closeCursor();

            if ($row[0] == 0) {
                $q = <<<EOF
INSERT INTO {$this->_table_prefix}languages (id, name, language_code, is_default, display_order, is_deleted)
VALUES (1, 'English', 'en', 1, 0, 0);
EOF;

                $stmt = $this->getDBConn()->query($q);
                if (!$stmt) {
                    $this->_last_error_msg = implode(' ', $this->getDBConn()->errorInfo());
                    return false;
                } else {
                    $stmt->closeCursor();
                    return true;
                }
            } else {
                return true;
            }
        }

    }

    private function fetchTranslationData()
    {
        if ($this->_translated_strings != null) {
            return true;
        }

        //translation keys
        $q = <<<EOF
SELECT
t1.id,
t1.label
FROM {$this->_table_prefix}translation_keys t1
WHERE
t1.is_deleted = 0
EOF;

        $stmt = $this->getDBConn()->query($q);
        if (!$stmt) {
            $this->_last_error_msg = implode(' ', $this->getDBConn()->errorInfo());
            return false;
        } else {
            $translation_keys = array();

            while ($row_value = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $translation_keys[] = $row_value;
            }
            $stmt->closeCursor();

            $this->_translation_keys = $translation_keys;
        }

        //translation values
        $q = <<<EOF
SELECT
t1.id,
t1.value,
t1.translation_key_id,
t1.language_id
FROM {$this->_table_prefix}translation_values t1
WHERE
t1.is_deleted = 0
EOF;

        $stmt = $this->getDBConn()->query($q);
        if (!$stmt) {
            $this->_last_error_msg = implode(' ', $this->getDBConn()->errorInfo());
            return false;
        } else {
            $translation_values = array();

            while ($row_value = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $translation_values[$row_value['translation_key_id']][$row_value['language_id']] = $row_value['value'];
            }
            $stmt->closeCursor();
        }

        //languages
        $q = <<<EOF
SELECT
id,
name,
language_code,
display_order,
is_default
FROM {$this->_table_prefix}languages
WHERE
is_deleted = 0
ORDER BY display_order ASC, name ASC
EOF;

        $stmt = $this->getDBConn()->query($q);
        if (!$stmt) {
            $this->_last_error_msg = implode(' ', $this->getDBConn()->errorInfo());
            return false;
        } else {
            $languages    = array();
            $language_ids = array();

            while ($row_value = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $languages[]                                           = $row_value;
                $language_ids[strtolower($row_value['language_code'])] = $row_value['id'];
            }
            $stmt->closeCursor();

            $this->_languages    = $languages;
            $this->_language_ids = $language_ids;
        }

        //translated strings
        $translated_strings = array();

        foreach ($translation_keys as $translation_key) {
            foreach ($languages as $language) {
                $translated_strings[$language['id']][$translation_key['label']] =
                    (isset($translation_values[$translation_key['id']][$language['id']]) ?
                        $translation_values[$translation_key['id']][$language['id']] : '');
            }
        }

        $this->_translated_strings = $translated_strings;

        return true;
    }

    /**
     * returns translated strings for all languages
     *
     * @return null|array translated strings
     */
    private function getTranslatedStringsForAllLanguages()
    {
        $this->fetchTranslationData();
        return $this->_translated_strings;
    }

    /**
     * returns translated strings for current language
     *
     * @return array translated strings
     */
    public function getTranslatedStringsForCurrentLanguage()
    {
        $this->fetchTranslationData();

        $current_language_id = $this->getCurrentLanguageId();

        $translated_panel_strings = array();

        if (array_key_exists($current_language_id, $this->_translated_strings)) {
            $translated_panel_strings = $this->_translated_strings[$current_language_id];
        }

        return $translated_panel_strings;
    }

    /**
     * returns translated string for specified key
     * if language_code parameter is ommitted, the translated string
     * for the current language will be returned
     *
     * @param string $key translation key
     * @param string $language_code =''
     *
     * @return string translated string
     */
    public function getTranslatedString($key, $language_code = '')
    {
        $translated_string = '';

        if ($language_code == '') {
            $translated_strings  = $this->getTranslatedStringsForCurrentLanguage();
            $current_language_id = $this->getCurrentLanguageId();

            if (array_key_exists($key, $translated_strings)) {
                $translated_string = $translated_strings[$current_language_id][$key];
            }
        } else {
            $language_code      = strtolower($language_code);
            $translated_strings = $this->getTranslatedStringsForAllLanguages();
            $language_ids       = $this->getLanguageIds();

            if (array_key_exists($language_code, $language_ids)
                && array_key_exists($key, $translated_strings[$language_ids[$language_code]])
            ) {
                $translated_string = $translated_strings[$language_ids[$language_code]][$key];
            }
        }

        return $translated_string;
    }

    /**
     * returns translation keys
     *
     * @return array translation keys
     */
    public function getTranslationKeys()
    {
        $translation_keys = $this->_translation_keys;

        if (is_array($translation_keys)) {
            return $translation_keys;
        } else {
            return array();
        }
    }

    /**
     * returns available languages
     *
     * @return null|array languages
     */
    public function getLanguages()
    {
        $this->fetchTranslationData();
        return $this->_languages;
    }

    /**
     * returns available language ids
     *
     * @return null|array language ids
     */
    public function getLanguageIds()
    {
        $this->fetchTranslationData();
        return $this->_language_ids;
    }

    private function setCurrentLanguageId($id)
    {
        $this->_current_language_id = $id;

        return true;
    }

    private function setCurrentLanguageCode($language_code)
    {
        $this->_current_language_code = $language_code;

        @session_start();
        $_SESSION['multilanguage' . $this->_table_prefix] = $language_code;

        return true;
    }

    /**
     * sets table prefix
     *
     * @param string $prefix table prefix
     * @return bool
     */
    public function setTablePrefix($prefix)
    {
        $this->_table_prefix = $prefix;
        return true;
    }

    /**
     * get table prefix
     *
     */
    public function getTablePrefix()
    {
        return $this->_table_prefix;
    }

    /**
     * returns selected language id
     *
     * @return null|int selected language id
     */
    public function getCurrentLanguageId()
    {
        return $this->_current_language_id;
    }

    /**
     * returns selected language code
     *
     * @return null|string selected language code
     */
    public function getCurrentLanguageCode()
    {
        return $this->_current_language_code;
    }

    /**
     * sets current language
     *
     * @return bool
     */
    public function setCurrentLanguage($language_code)
    {
        $languages = $this->getLanguages();

        if ($languages == null) {
            return false;
        }

        foreach ($languages as $language) {
            if ($language['language_code'] == $language_code) {
                $this->setCurrentLanguageId($language['id']);
                $this->setCurrentLanguageCode($language_code);
                return true;
            }
        }

        return false;
    }

    /**
     * automatically selects current language
     *
     * follows the following steps and stops if successfully selects
     *
     * - checks if the $_GET[$get_key] exists
     * - checks the session data
     * - checks browser
     * - selects default language
     *
     * @return bool
     */
    public function autoSelectCurrentLanguage($get_key = 'lang')
    {
        $languages    = $this->getLanguages();
        $language_ids = $this->getLanguageIds();

        if ($languages == null) {
            return false;
        }

        // check $_GET
        if (isset($_GET[$get_key])) {
            foreach ($languages as $language) {
                if ($_GET[$get_key] == $language['language_code']) {
                    $this->setCurrentLanguageId($language['id']);
                    $this->setCurrentLanguageCode($language['language_code']);
                    return true;
                }
            }
        }

        // check $_SESSION
        @session_start();
        if (isset($_SESSION['multilanguage' . $this->_table_prefix])) {
            foreach ($languages as $language) {
                if ($_SESSION['multilanguage' . $this->_table_prefix] == $language['language_code']) {
                    $this->setCurrentLanguageId($language['id']);
                    $this->setCurrentLanguageCode($language['language_code']);
                    return true;
                }
            }
        }

        // check browser
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $accepted_languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            foreach ($languages as $language) {
                $language_code = strtolower($language['language_code']);

                foreach ($accepted_languages as $accepted_language) {
                    if (array_key_exists($accepted_language, $this->_language_ids)) {
                        $this->setCurrentLanguageId($this->_language_ids[$accepted_language]);
                        $this->setCurrentLanguageCode($language_code);
                        return true;
                    }

                    if (strpos($accepted_language, '-') !== false) {
                        $accepted_language = substr(
                            $accepted_language,
                            0,
                            strlen($accepted_language) - strpos($accepted_language, '-') - 1
                        );
                        $accepted_language = strtolower($accepted_language);

                        if ($language_code == $accepted_language) {
                            $this->setCurrentLanguageId($language['id']);
                            $this->setCurrentLanguageCode($language_code);
                            return true;
                        }
                    }
                }
            }
        }

        // select default language
        foreach ($languages as $language) {
            if ($language['is_default'] == true) {
                $this->setCurrentLanguageId($language['id']);
                $this->setCurrentLanguageCode($language['language_code']);
                return true;
            }
        }

        return false;
    }
}
