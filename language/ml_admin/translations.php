<?php
require_once('config/panel.php');

session_start();
if (!isset($_SESSION['script_user'])) {
    header('Location:index.php');
    exit();
}

require_once('Connections/conn.php');

require_once('site/php/translated_panel_strings.php');

$_SESSION['csrf_d'] = md5(uniqid());

if (isset($_POST['key'])
    && trim($_POST['key']) != ''
    && isset($_POST['csrf_i'])
    && isset($_SESSION['csrf_i'])
    && $_POST['csrf_i'] == $_SESSION['csrf_i']
) {
    $key = prepare_for_db($_POST['key']);

    $sql = "INSERT INTO {$strings_table_prefix}translation_keys(label) VALUES ('{$key}')";

    //SQL
    mysqli_query($conn, $sql);

    //insert translation values
    $translation_key_id = mysqli_insert_id($conn);

    foreach ($_POST as $key => $value) {
        if (preg_match('/n_t_([0-9]*)_([0-9]*)/', $key, $result)) {
            $value       = prepare_for_db($value);
            $language_id = prepare_for_db($result[2]);

            //insert
            $sql = <<<EOF
INSERT INTO {$strings_table_prefix}translation_values
(
translation_key_id,
language_id,
value
)
VALUES
(
'{$translation_key_id}',
'{$language_id}',
'{$value}'
)
EOF;
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
        }
    }

    unset($_SESSION['csrf_i']);

    header("Location: translations.php");
    exit();
}

$_SESSION['csrf_i'] = md5(uniqid());

//available languages
$sql = <<<EOF
SELECT
t1.id,
t1.name,
t1.language_code
FROM {$strings_table_prefix}languages t1
WHERE
t1.is_deleted = 0
ORDER BY t1.display_order ASC, t1.name ASC
EOF;

$Recordset_Languages = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$languages = array();

while ($row_language = mysqli_fetch_assoc($Recordset_Languages)) {
    $languages[] = $row_language;
}

//translation keys
$sql = <<<EOF
SELECT
t1.id,
t1.label
FROM {$strings_table_prefix}translation_keys t1
WHERE
t1.is_deleted = 0
ORDER BY t1.label ASC
EOF;

$Recordset_TranslationKeys = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$translation_keys = array();

while ($row_key = mysqli_fetch_assoc($Recordset_TranslationKeys)) {
    $translation_keys[] = $row_key;
}

//translation values
$sql = <<<EOF
SELECT
t1.id,
t1.value,
t1.translation_key_id,
t1.language_id
FROM {$strings_table_prefix}translation_values t1
WHERE
t1.is_deleted = 0
EOF;

$Recordset_TranslationValues = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$translation_values = array();

while ($row_value = mysqli_fetch_assoc($Recordset_TranslationValues)) {
    $translation_values[$row_value['translation_key_id']][$row_value['language_id']] = $row_value['value'];
}

//update records - begin
if (isset($_POST['update_records'])
    && isset($_POST['csrf_e'])
    && isset($_SESSION['csrf_e'])
    && $_POST['csrf_e'] == $_SESSION['csrf_e']
) {
    foreach ($_POST as $key => $value) {
        if (preg_match('/t_([0-9]*)_([0-9]*)/', $key, $result)) {
            $value              = prepare_for_db($value);
            $translation_key_id = prepare_for_db($result[1]);
            $language_id        = prepare_for_db($result[2]);

            if (isset($translation_values[$translation_key_id][$language_id])) {
                //update
                $sql = <<<EOF
UPDATE {$strings_table_prefix}translation_values
SET
value = '{$value}'
WHERE
translation_key_id = '{$translation_key_id}'
AND language_id = '{$language_id}'
EOF;
                mysqli_query($conn, $sql) or die(mysqli_error($conn));
            } else {
                //insert
                $sql = <<<EOF
INSERT INTO {$strings_table_prefix}translation_values
(
translation_key_id,
language_id,
value
)
VALUES
(
'{$translation_key_id}',
'{$language_id}',
'{$value}'
)
EOF;
                mysqli_query($conn, $sql) or die(mysqli_error($conn));
            }
        }
    }

    unset($_SESSION['csrf_e']);

    header("Location: translations.php");

    exit();
}
//update records - end

$_SESSION['csrf_e'] = md5(uniqid());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $translated_panel_strings['Translations']; ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.js"></script>
    <link rel="stylesheet" href="css/summernote-lite.css">
    <script src="js/summernote-lite.js"></script>
</head>

<body>
<?php include('menu_section.php'); ?>
<div class="container">
    <div class="row" style="margin-top: 10px;">
        <p><strong><?php echo $translated_panel_strings['NewTranslation']; ?></strong></p>

        <form action="" method="post" enctype="multipart/form-data" role="form">
            <table class="table table-bordered" style="width: 600px;">
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['Key']; ?>
                    </td>
                    <td>
                        <input type="text" name="key" style="width:160px;"/>
                    </td>
                </tr>
                <?php
                foreach ($languages as $language) {
                    ?>
                    <tr>
                        <td style="background-color: #F5F5F5;" colspan="2">
                            <?php echo $language['name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $translated_panel_strings['Value']; ?>
                        </td>
                        <td>
                            <textarea class="editor" name="<?php echo 'n_t_' . $translation_key['id'] . '_' . $language['id']; ?>"
                                    rows="5" cols="25"></textarea>
                        </td>
                    </tr>
                <?php
                }
                ?>
                <tr>
                    <td><input type="hidden" name="csrf_i" value="<?php echo $_SESSION['csrf_i']; ?>"/></td>
                    <td>
                        <input type="submit" name="Submit" value="<?php echo $translated_panel_strings['Insert']; ?>"/>
                    </td>
                </tr>
            </table>
        </form>
        <p><strong><?php echo $translated_panel_strings['Translations']; ?></strong></p>

        <form action="" method="post" enctype="multipart/form-data" role="form">
            <table class="table table-bordered"
                   <?php /* ?> style="<?php echo 'width: ' . (180 * count($languages) + 350) . 'px;'; ?>" <?php */ ?>
                   >
                <tr class="info">
                    <th><?php echo $translated_panel_strings['Key']; ?></th>
                    <?php foreach ($languages as $language) { ?>
                        <th><?php echo htmlspecialchars(
                                $language['name'] . ' (' . $language['language_code'] . ')'
                            ); ?></th>
                    <?php } ?>
                    <th><?php echo $translated_panel_strings['EditKey']; ?></th>
                    <th><?php echo $translated_panel_strings['Delete']; ?></th>
                </tr>
                <?php foreach ($translation_keys as $translation_key) { ?>
                    <tr>
                        <td><?php echo htmlspecialchars($translation_key['label']); ?></td>
                        <?php foreach ($languages as $language) { ?>
                            <td>
                                <textarea class="editor" name="<?php echo 't_' . $translation_key['id'] . '_' . $language['id']; ?>" rows="5" cols="25"><?php
                                    echo(isset($translation_values[$translation_key['id']][$language['id']]) ?
                                        htmlspecialchars_decode(
                                        $translation_values[$translation_key['id']][$language['id']]
                                    ) : '');
                                    ?></textarea>

                            </td>
                        <?php } ?>
                        <td>
                            <a href="edit_translation_key.php?id=<?php echo $translation_key['id']; ?>"><?php echo
                                $translated_panel_strings['EditKey']; ?></a>
                        </td>
                        <td>
                            <a href="#" onclick="<?php echo
"if (confirm('{$translated_panel_strings['AreYouSure']}')) {" .
"document.getElementById('d_tk_{$translation_key['id']}').submit(); } return false;";
                            ?>"><?php echo $translated_panel_strings['Delete']; ?></a>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>
                        <input type="hidden" name="csrf_e" value="<?php echo $_SESSION['csrf_e']; ?>"/>
                        <input type="hidden" name="update_records" value="1">
                        <!-- <input type="submit" name="Submit" value="<?php echo $translated_panel_strings['Save']; ?>"/> -->
                        <button type="submit"><?php echo $translated_panel_strings['Save']; ?></button>
                    </td>
                </tr>
            </table>
        </form>

        <?php foreach ($translation_keys as $translation_key) { ?>
            <form id="d_tk_<?php echo $translation_key['id']; ?>"
                  action="<?php echo 'delete_translation_key.php?id=' . $translation_key['id']; ?>"
                  method="post" role="form">
                <input type="hidden" name="csrf_d" value="<?php echo $_SESSION['csrf_d']; ?>"/>
            </form>
        <?php } ?>
    </div>
</div>
    <script>
        $(document).ready(function() {
          $('.editor').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'link']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol']],
                ['codeview']
            ]
          });
        });
    </script>
</body>
</html>