<?php
require_once('config/panel.php');

session_start();
if (!isset($_SESSION['script_user'])) {
    header('Location:index.php');
    exit();
}

require_once('Connections/conn.php');

require_once('site/php/translated_panel_strings.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $translated_panel_strings['Welcome']; ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body style="margin:0;padding:0;">
<?php
include('menu_section.php');
?>

<div class="container">
    <div class="row" style="margin-top: 10px;">
        <p><?php echo $translated_panel_strings['Welcome']; ?></p>
    </div>
</div>
</body>
</html>