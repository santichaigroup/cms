<?php
require_once('config/panel.php');

session_start();
session_destroy();
header("Location: index.php");