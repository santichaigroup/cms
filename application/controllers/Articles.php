<?php
class Articles extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
			parent::__construct();
			$lang 					= $this->uri->segment_array();
			$lang 					= end($lang);
			(empty($lang))?$lang 	= "TH" : $lang = "EN";
			$this->_data['lang']	= $lang;

			// Footer
				$this->_data['analytic'] 	= $this->footer_model->getAnalytic();
				$this->_data['address']		= $this->footer_model->address()->row_array();
	}

	public function index()
	{
		$lang = $this->_data['lang'];

		// Articles
		$this->_data['articles'] 			= $this->settings_model->articles($lang)->result_array();
// pre($this->_data['articles']);
		foreach ($this->_data['articles'] as $key => $value) {
			
			$this->_data['articles'][$key] = $value;
			$this->_data['articles'][$key]['image'] = $this->settings_model->articles_img($value['main_id'], 'highlight')
																			->row_array();
		}

		//Set Headder
		$this->_data['class'] 			= "fact";
		$this->_data['meta_title'] 		= text_lang('FACT', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_articles', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_articles', $lang);

		$this->_data['lang_en']			= "articles/EN";
		$this->_data['lang_th']			= "articles/TH";
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('articles/list',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function articles_detail($id, $lang)
	{
		if($id) {
			$lang = $this->_data['lang'];

			// Articles
			$articles_all_lang					= $this->settings_model->articles_detail($id)->result_array();
			$this->_data['articles'] 			= $this->settings_model->articles_detail($id, $lang)->row_array();
			$this->_data['articles']['image'] 	= $this->settings_model->articles_img($this->_data['articles']['main_id'], 'cover')
																		->result_array();

			$this->_data['articles_relate']		= $this->settings_model->articles_relate($id, $lang)->result_array();
			if(isset($this->_data['articles_relate'])) {
				foreach ($this->_data['articles_relate'] as $key => $value) {
					
					$this->_data['articles_relate'][$key] = $value;
					$this->_data['articles_relate'][$key]['image'] = $this->settings_model->articles_img($value['main_id'], 'highlight')
																							->row_array();
				}
			}

			//Set Headder
			$this->_data['class'] 			= "factDetail";
			$this->_data['meta_title'] 		= $this->_data['articles']['content_title'];
			$this->_data['keyword'] 		= $this->_data['articles']['content_keyword'];
			$this->_data['tagDescription'] 	= $this->_data['articles']['content_description'];

			foreach ($articles_all_lang as $value) {
				
				$this->_data['lang_'.strtolower($value['lang_id'])]			= $value['content_seo'];
			}

			$this->load->view('include/header',$this->_data);
			$this->load->view('articles/detail',$this->_data);
			$this->load->view('include/footer',$this->_data);
		} else {

			redirect('articles/'.$lang);
		}
	}

/* End of file compress.php */
/* Location: ./system/application/hooks/compress.php */

}
?>