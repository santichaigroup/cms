<?php
class Home extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
			parent::__construct();
			$lang 					= $this->uri->segment_array();
			$lang 					= end($lang);
			(empty($lang))?$lang 	= "EN" : $lang = $lang;
			$this->_data['lang']	= $lang;

			// Footer
				$this->_data['analytic'] 	= $this->footer_model->getAnalytic();
				$this->_data['address']		= $this->footer_model->address($lang)->row_array();
	}

	public function index($lang="EN")
	{
		$lang = $this->_data['lang'];

		// Banner
		$this->_data['banner'] 			= $this->settings_model->banner()->result_array();

		//Set Headder
		$this->_data['class'] 			= "index";
		$this->_data['meta_title'] 		= text_lang('HOME', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_home', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_home', $lang);

		$this->_data['lang_en']			= "home/EN";
		$this->_data['lang_th']			= "home/TH";
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('index',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function about($lang="EN")
	{
		$lang = $this->_data['lang'];

		// Banner
		$this->_data['banner'] 			= $this->settings_model->banner()->result_array();

		//Set Headder
		$this->_data['class'] 			= "about";
		$this->_data['meta_title'] 		= text_lang('POCARI SWEAT', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_about', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_about', $lang);

		$this->_data['lang_en']			= "about/EN";
		$this->_data['lang_th']			= "about/TH";
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('about',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function product($lang="EN")
	{
		$lang = $this->_data['lang'];

		//Set Headder
		$this->_data['class'] 			= "product";
		$this->_data['meta_title'] 		= text_lang('PRODUCT', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_product', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_product', $lang);

		$this->_data['lang_en']			= "product/EN";
		$this->_data['lang_th']			= "product/TH";
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('product',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function faq($lang="EN")
	{
		$lang = $this->_data['lang'];

		//Set Headder
		$this->_data['class'] 			= "faq";
		$this->_data['meta_title'] 		= text_lang('FAQS', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_faq', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_faq', $lang);

		$this->_data['lang_en']			= "faq/EN";
		$this->_data['lang_th']			= "faq/TH";

		$this->_data['wheretobuy'] 			= $this->settings_model->wheretobuy()->result_array();
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('faq',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function whereToBuy($lang="EN")
	{
		$lang = $this->_data['lang'];

		//Set Headder
		$this->_data['class'] 			= "whereToBuy";
		$this->_data['meta_title'] 		= text_lang('WHERE TO BUT', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_whereToBuy', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_whereToBuy', $lang);

		$this->_data['lang_en']			= "whereToBuy/EN";
		$this->_data['lang_th']			= "whereToBuy/TH";

		$this->_data['wheretobuy'] 			= $this->settings_model->wheretobuy()->result_array();
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('wheretobuy',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function contact($lang="EN")
	{
		$lang = $this->_data['lang'];

		//Set Headder
		$this->_data['class'] 			= "contact";
		$this->_data['meta_title'] 		= text_lang('CONTACT US', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_contact', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_contact', $lang);

		$this->_data['lang_en']			= "contact/EN";
		$this->_data['lang_th']			= "contact/TH";
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('contact',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

/* End of file compress.php */
/* Location: ./system/application/hooks/compress.php */

}
?>