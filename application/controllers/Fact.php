<?php
class Fact extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
			parent::__construct();
			$lang 					= $this->uri->segment_array();
			$lang 					= end($lang);
			(empty($lang))?$lang 	= "TH" : $lang = "EN";
			$this->_data['lang']	= $lang;

			// Footer
				$this->_data['analytic'] 	= $this->footer_model->getAnalytic();
				$this->_data['address']		= $this->footer_model->address()->row_array();
	}

	public function index()
	{
		$lang = $this->_data['lang'];

		// fact

		//Set Headder
		$this->_data['class'] 			= "fact";
		$this->_data['meta_title'] 		= text_lang('FACT', $lang);
		$this->_data['keyword'] 		= "";
		$this->_data['tagDescription'] 	= "";

		$this->_data['lang_en']			= "articles/EN";
		$this->_data['lang_th']			= "articles/TH";
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('fact/list',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function fact_detail($id, $lang)
	{
		$lang = $this->_data['lang'];

		// fact

		//Set Headder
		$this->_data['class'] 			= "factDetail";
		$this->_data['meta_title'] 		= text_lang('FACT', $lang);
		$this->_data['keyword'] 		= "";
		$this->_data['tagDescription'] 	= "";

		switch ($id) {
			case '1':
				$this->_data['lang_en']			= "water-is-important/EN";
				$this->_data['lang_th']			= "น้ำเป็นองค์ประกอบหลักในร่างกายของเรา/TH";

				$this->load->view('include/header',$this->_data);
				$this->load->view('fact/detail_1',$this->_data);
				break;
			case '2':
				$this->_data['lang_en']			= "roles-of-body-fluids/EN";
				$this->_data['lang_th']			= "หน้าที่ของของเหลวในร่างกาย/TH";

				$this->load->view('include/header',$this->_data);
				$this->load->view('fact/detail_2',$this->_data);
				break;
			case '3':
				$this->_data['lang_en']			= "don’t-get-dehydration/EN";
				$this->_data['lang_th']			= "อย่าให้ร่างกายขาดน้ำ/TH";

				$this->load->view('include/header',$this->_data);
				$this->load->view('fact/detail_3',$this->_data);
				break;
			case '4':
				$this->_data['lang_en']			= "thirst-mechanism/EN";
				$this->_data['lang_th']			= "ความกระหายคืออะไร/TH";

				$this->load->view('include/header',$this->_data);
				$this->load->view('fact/detail_4',$this->_data);
				break;
			case '5':
				$this->_data['lang_en']			= "voluntary-dehydration/EN";
				$this->_data['lang_th']			= "ภาวะการขาดน้ำแบบแฝง​/TH";

				$this->load->view('include/header',$this->_data);
				$this->load->view('fact/detail_5',$this->_data);
				break;
			
			default:
				$this->_data['lang_en']			= "water-is-important/EN";
				$this->_data['lang_th']			= "น้ำเป็นองค์ประกอบหลักในร่างกายของเรา/TH";

				$this->load->view('include/header',$this->_data);
				$this->load->view('fact/detail_1',$this->_data);
				break;
		}

		$this->load->view('include/footer',$this->_data);
	}

/* End of file compress.php */
/* Location: ./system/application/hooks/compress.php */

}
?>