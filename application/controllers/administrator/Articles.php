<?php 
class Articles extends CI_Controller {
	
	var $_data = array();
	var $_menu_name = '';
	var $menu;
	var $submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('administrator/Articles_model');
		$this->load->library('form_validation');

		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(2));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(2), $this->uri->segment(3));
	}
	
	public function index($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("articles/listview",$this->_data); 
		$this->admin_library->output();
	}

	public function load_datatable($lang_id="TH")
	{
		$result_data 	= $this->Articles_model->dataTable($lang_id);

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	public function add($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;
		
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");
		// $this->form_validation->set_rules("content_detail","รายละเอียด","trim|required");

		// เช็คอัพโหลดไฟล์
		// $this->form_validation->set_rules("image_thumb[]","รูปภาพ","callback_fileupload_images");
		$this->form_validation->set_rules(
											"file_highlight[]",
											"อัพโหลดรูปภาพ Highlight (ปก) หน้า Articles list",
											"callback_fileupload_file_highlight"
										);
		$this->form_validation->set_rules("file_thumb[]","อัพโหลดรูปภาพ หน้า Articles detail","callback_fileupload_files");
		
		if($this->form_validation->run()===false) {
			
			$this->_data['_menu_name']	= "Add ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " เพิ่ม".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];
			
			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("articles/add",$this->_data); 
			$this->admin_library->output();

		} else {
			
			$main_id 		= $this->Articles_model->addData();
			$content_id 	= $this->Articles_model->addLanguage($main_id,$lang_id);

			$this->Articles_model->setDefaultContent($main_id,$lang_id,$content_id);  
			$this->Articles_model->setDate($main_id);

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_detail_2'		=> $this->input->post("content_detail_2"),
						'content_detail_3'		=> $this->input->post("content_detail_3"),
						'content_short'			=> $this->input->post("content_short"),
						'content_title'			=> ($this->input->post("content_title") ? $this->input->post("content_title") : $this->input->post("content_subject")),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> $this->input->post("content_subject"),
						'content_rewrite_id'	=> md5($this->menu['menu_id'].$main_id.time())
					);

			$checkUpdate = $this->Articles_model->updateContent($data);

			// ############# Images Upload ############# //
				// if($this->_data['img_thumbnail']) {

				// 	$data['img_thumbnail'] = $this->_data['img_thumbnail'];
				// 	if($data['img_thumbnail'])
				// 	{
				// 		// อัพโหลดครั้งเดียวทุกภาษา
				// 		foreach($data['img_thumbnail'] as $thumbnail)
				// 		{
				// 			$this->Articles_model->insertContent(
				// 				$main_id,
				// 				$thumbnail
				// 			);
				// 		}
				// 		// อัพโหลดภาษาละครั้ง
				// 		// foreach($data['img_thumbnail'] as $thumbnail)
				// 		// {
				// 		// 	$this->Articles_model->insertContent(
				// 		// 		$main_id,
				// 		// 		$thumbnail,
				// 		// 		$lang_id
				// 		// 	);
				// 		// }
				// 	}
				// }

			// ############# Files Highlight ############# //
				if(isset($this->_data['img_highlight'])) {

					$data['file_thumb'] = $this->_data['img_highlight'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this->Articles_model->insertContent(
								$main_id,
								$thumbnail,
								'highlight'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this->Articles_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			// ############# Files Cover ############# //
				if(isset($this->_data['img_thumb'])) {

					$data['file_thumb'] = $this->_data['img_thumb'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this->Articles_model->insertContent(
								$main_id,
								$thumbnail,
								'cover'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this->Articles_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't create",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);
				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //



			} else {

				$this->session->set_flashdata("success_message","Content has been create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been create",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$seo = array(
								'lang_id'				=> $data['lang_id'],
								'menu_link' 			=> $this->menu['menu_link'],
								'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
								'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
								'content_subject'		=> $data['content_seo'],
								'content_rewrite_id'	=> $data['content_rewrite_id']
							);

					$this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit/".$main_id."/".$lang_id);
			}
		}
	}

	public function edit($main_id,$lang_id=NULL)
	{	
		(empty($lang_id))?$lang_id 	= "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 	= $lang_id;
		
		$this->_data['row'] = $this->Articles_model->getDetail($main_id,$lang_id);

		if(!$this->_data['row']) {
			$this->Articles_model->addLanguage($main_id,$lang_id);
			$this->_data['row'] = $this->Articles_model->getDetail($main_id,$lang_id);
		}

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");	
		}

		$this->admin_library->add_breadcrumb($this->admin_library->getLanguagename($this->_data['row']['lang_id']),"Articles/edit/".$this->_data['row']['main_id']."/".$this->_data['row']['lang_id'],"icon-globe");
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");
		// $this->form_validation->set_rules("content_detail","รายละเอียด","trim|required");

		// $rs_img 		= $this->Articles_model->getDetail_img($main_id, 'gallery');
		$rs_highlight 	= $this->Articles_model->getDetail_img($main_id, 'highlight');
		$rs_file 		= $this->Articles_model->getDetail_img($main_id, 'cover');

		// เช็คอัพโหลดไฟล์
		// if(!count($rs_img->result_array())) {
		// 	$this->form_validation->set_rules("image_thumb[]","อัพโหลดรูปภาพแกลเลอรี","callback_fileupload_images");
		// }

		if(!count($rs_highlight->result_array())) {
			$this->form_validation->set_rules("file_highlight[]","อัพโหลดรูปภาพ Highlight (ปก) หน้า Articles list","callback_fileupload_file_highlight");
		}

		if(!count($rs_file->result_array())) {
			$this->form_validation->set_rules("file_thumb[]","อัพโหลดรูปภาพ หน้า Articles detail","callback_fileupload_files");
		}
	
		if($this->form_validation->run()===false) {
			
			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " แก้ไข".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];
			
			// $this->_data['rs_img'] 			= ( $rs_img ? $rs_img->result_array() : null );
			$this->_data['rs_highlight'] 	= ( $rs_highlight ? $rs_highlight->row_array() : null );
			$this->_data['rs_file'] 		= ( $rs_file ? $rs_file->result_array() : null );
			
			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("articles/edit",$this->_data); 
			$this->admin_library->output();

		} else {
			
			$this->Articles_model->setDate($main_id);
			
			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_detail_2'		=> $this->input->post("content_detail_2"),
						'content_detail_3'		=> $this->input->post("content_detail_3"),
						'content_short'			=> $this->input->post("content_short"),
						'content_title'			=> $this->input->post("content_title"),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> ($this->input->post("content_seo")?$this->input->post("content_seo"):$this->input->post("content_subject")),
						'content_rewrite_id'	=> ($this->_data['row']['content_rewrite_id']?$this->_data['row']['content_rewrite_id']:md5($this->menu['menu_id'].$main_id.time()))
					);

			$checkUpdate = $this->Articles_model->updateContent($data);
			
			// ############# Images Upload ############# //
				// if($this->_data['img_thumbnail']) {

				// 	$data['img_thumbnail'] = $this->_data['img_thumbnail'];
				// 	if($data['img_thumbnail'])
				// 	{
				// 		// อัพโหลดครั้งเดียวทุกภาษา
				// 		foreach($data['img_thumbnail'] as $thumbnail )
				// 		{
				// 			$this->Articles_model->insertContent(
				// 				$main_id,
				// 				$thumbnail
				// 			);
				// 		}
				// 		// อัพโหลดภาษาละครั้ง
				// 		// foreach($data['img_thumbnail'] as $thumbnail )
				// 		// {
				// 		// 	$this->Articles_model->insertContent(
				// 		// 		$main_id,
				// 		// 		$thumbnail,
				// 		// 		$lang_id
				// 		// 	);
				// 		// }
				// 	}
				// }

			// ############# Files Highlight ############# //
				if(isset($this->_data['img_highlight'])) {

					$data['file_thumb'] = $this->_data['img_highlight'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this->Articles_model->insertContent(
								$main_id,
								$thumbnail,
								'highlight'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this->Articles_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				// Set Sequence Images
					$img_id 				= $this->input->post('attachment_id');
					$attachment_alt 		= $this->input->post('attachment_alt');
					$attachment_title 		= $this->input->post('attachment_title');

					if(is_array($img_id) && count($img_id)>0) {

						foreach($img_id as $id) {

							$data_update = array(
								'attachment_id'		=> $id,
								'attachment_alt' 	=> $attachment_alt[$id],
								'attachment_title' 	=> $attachment_title[$id],
								// 'lang_id'			=> $lang_id
							);

							$this->Articles_model->updateAttachmentContent($data_update);
						}
					}

			// ############# Files Cover ############# //
				if(isset($this->_data['img_thumb'])) {

					$data['file_thumb'] = $this->_data['img_thumb'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this->Articles_model->insertContent(
								$main_id,
								$thumbnail,
								'cover'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this->Articles_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				// Set Sequence Images
					$img_id 				= $this->input->post('attachment_id_dt');
					$attachment_alt 		= $this->input->post('attachment_alt_dt');
					$attachment_title 		= $this->input->post('attachment_title_dt');

					if(is_array($img_id) && count($img_id)>0) {

						foreach($img_id as $id) {

							$data_update = array(
								'attachment_id'		=> $id,
								'attachment_alt' 	=> $attachment_alt[$id],
								'attachment_title' 	=> $attachment_title[$id],
								// 'lang_id'			=> $lang_id
							);

							$this->Articles_model->updateAttachmentContent($data_update);
						}
					}
			 
				// $img_id = $this->input->post('attachment_id'); 
				// if(is_array($img_id) and count($img_id)>0)
				// {
				// 	$i=0;
				// 	foreach($img_id as $id){
				// 		$i++;
				// 		$this->Articles_model->setSequenceAttachment($id,$i);
				// 	}
				// }
			
			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't delete",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);

			} else {

				$this->session->set_flashdata("success_message","Content has been delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been update",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$seo = array(
								'lang_id'				=> $data['lang_id'],
								'menu_link' 			=> $this->menu['menu_link'],
								'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
								'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
								'content_subject'		=> $data['content_seo'],
								'content_rewrite_id'	=> $data['content_rewrite_id']
							);

					$this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit/".$main_id.'/'.$lang_id);
			}
		}
	}
	
	function delete($main_id)
	{
		$data = array(
					'main_id' => $main_id
				);

		$checkUpdate = $this->Articles_model->deleteContent($data);

		$queryAll = $this->Articles_model->getAllContent();
		$queryResult = $this->Articles_model->getAllContent()->result_array();

		for($i=0; $i<$queryAll->num_rows(); $i++) {

			$this->Articles_model->setSequence($queryResult[$i]['main_id'], ($i+1));
		}

		if(!$checkUpdate) {

			$this->session->set_flashdata("success_message","Cant' delete user.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Cant' delete user",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);
			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //		

		} else {

			$this->session->set_flashdata("success_message","Delete centent success.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete user success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

			//	$seo = array(
			//				'lang_id'				=> $data['lang_id'],
			// 				'menu_link' 			=> $this->menu['menu_link'],
			//				'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
			//				'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
			//				'content_subject'		=> $data['content_seo'],
			//				'content_rewrite_id'	=> $data['content_rewrite_id']
			//			);
			//
			//	$this->seo_library->rewrite_delete($seo);

			admin_redirect($this->menu['menu_link']."/index/");
		}
	}

	public function handle_delete()
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id")
				);
	
		foreach($data['main_id'] as $id) {
			$id = array('main_id' => $id);
			$this->Articles_model->deleteContent($id);
		}

		$queryAll = $this->Articles_model->getAllContent();
		$queryResult = $this->Articles_model->getAllContent()->result_array();

		for($i=0; $i<$queryAll->num_rows(); $i++) {

			$this->Articles_model->setSequence($queryResult[$i]['main_id'], ($i+1));
		}

		$this->session->set_flashdata("success_message","Delete centent success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'delete',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Delete user success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

		//	$seo = array(
		//				'lang_id'				=> $data['lang_id'],
		// 				'menu_link' 			=> $this->menu['menu_link'],
		//				'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
		//				'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
		//				'content_subject'		=> $data['content_seo'],
		//				'content_rewrite_id'	=> $data['content_rewrite_id']
		//			);
		//
		//	$this->seo_library->rewrite_delete($seo);

		admin_redirect($this->menu['menu_link']."/index");
	}

	public function handle_suspend()
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);
		
		foreach($data['main_id'] as $id) {
			$this->Articles_model->setStatus($id,$data['lang_id'],"pending");
		}

		$this->session->set_flashdata("success_message","Update centent success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update user success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);
		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

		admin_redirect($this->menu['menu_link']."/index/");
	}

	public function handle_unsuspend()
	{
		$data = array(
					'main_id'	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);
		
		foreach($data['main_id'] as $id) {
			$this->Articles_model->setStatus($id,$data['lang_id'],"active");
		}

		$this->session->set_flashdata("success_message","Update centent success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update user success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);
		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
		admin_redirect($this->menu['menu_link']."/index/");
	}

	function delete_img($attachment_id,$default_main_id,$attachment_name,$lang_id,$type_name)
	{
		$data = array(
					'attachment_id'		=> $attachment_id,
					'default_main_id'	=> $default_main_id,
					'attachment_name'	=> $attachment_name,
					'lang_id'			=> $lang_id,
					'type_name'			=> $type_name
				);

		if($type_name == "images") {
			$path = './public/uploads/Articles/images/'.$attachment_name;
		} else {
			$path = './public/uploads/Articles/files/'.$attachment_name;
		}

		$checkImage = $this->Articles_model->deleteImage($attachment_id);

		if(!$checkImage) {

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't delete image",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);
			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

		} else {

			if(unlink($path))
			{
				$this->session->set_flashdata("success_message","Delete images success.");
			}else{
				$this->session->set_flashdata("error_message","Error! Can't delete image.");
			}

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Update user success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);
			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

			admin_redirect($this->menu['menu_link']."/edit/".$default_main_id."/".$lang_id);
		}
	}

	function set_default_img($default_main_id,$content_id,$attachment_name,$lang_id)
	{
		$this->Articles_model->setDefaultImg($content_id,$attachment_name,$lang_id);
		admin_redirect($this->menu['menu_link']."/edit/".$default_main_id."/".$lang_id);
	}

	// ################### Check uploads Images. ######################

		// public function fileupload_images()
		// {
		// 	$this->_data['img_thumbnail'] = array();
		//     $number_of_files = sizeof(@$_FILES['image_thumb']['tmp_name']);
		//     $files = @$_FILES['image_thumb'];
		 
		//     // ถ้าต้องการเช็คอัพโหลดรูป
		//  	// if($files['tmp_name']) {
		// 	//     for($i=0;$i<$number_of_files;$i++)
		// 	//     {
		// 	//       if($_FILES['image_thumb']['error'][$i] != 0)
		// 	//       {
		// 	//         $this->form_validation->set_message('fileupload_images', 'กรุณาอัพโหลด"รูปภาพ"');
		// 	//         return FALSE;
		// 	//       }
		// 	//     }
		// 	// }

		// 	// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		// 	if($files['tmp_name']) {

		// 		if($files['tmp_name'][0]==NULL) {

		// 			return TRUE;
		// 		}
		// 	} else {

		// 		return TRUE;
		// 	}

		// 	$config['upload_path'] 		= FCPATH.'./public/uploads/articles/images';
		// 	$config['encrypt_name']		= true;
		// 	$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		// 	$config['max_size']			= '5024';
		// 	$config['max_width']  		= '640';
		// 	$config['max_height']  		= '360';

		//     for ($i = 0; $i < $number_of_files; $i++)
		//     {
		//       $_FILES['image_thumb']['name'] 		= $files['name'][$i];
		//       $_FILES['image_thumb']['type'] 		= $files['type'][$i];
		//       $_FILES['image_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
		//       $_FILES['image_thumb']['error'] 		= $files['error'][$i];
		//       $_FILES['image_thumb']['size'] 		= $files['size'][$i];
		      
		//       $this->upload->initialize($config);
		//       if ($this->upload->do_upload('image_thumb'))
		//       {
		// 		$data  								= $this->upload->data();
		// 		$this->_data['img_thumbnail'][] 	= $data['file_name'];
		//       }
		//       else
		//       {
		// 		$this->form_validation->set_message('fileupload_images', $this->upload->display_errors());
		//         return FALSE;
		//       }
		//     }

		//     return TRUE;
		// }

	// ################### Check uploads Images. ######################

	public function fileupload_file_highlight()
	{
		$this->_data['img_highlight'] = array();
	    $number_of_files = sizeof(@$_FILES['file_highlight']['tmp_name']);
	    $files = @$_FILES['file_highlight'];
	 
	    // ถ้าต้องการเช็คอัพโหลดรูป
	 	// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['file_highlight']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message(__FUNCTION__, 'กรุณาอัพโหลด"รูปภาพ"');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/articles/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '556';
		$config['max_height']  		= '313';

	    for ($i = 0; $i < $number_of_files; $i++)
	    {
	      $_FILES['file_highlight']['name'] 		= $files['name'][$i];
	      $_FILES['file_highlight']['type'] 		= $files['type'][$i];
	      $_FILES['file_highlight']['tmp_name'] 	= $files['tmp_name'][$i];
	      $_FILES['file_highlight']['error'] 		= $files['error'][$i];
	      $_FILES['file_highlight']['size'] 		= $files['size'][$i];
	      
	      $this->upload->initialize($config);
	      if ($this->upload->do_upload('file_highlight'))
	      {
			$data  								= $this->upload->data();
			$this->_data['img_highlight'][] 	= $data['file_name'];
	      }
	      else
	      {
			$this->form_validation->set_message('fileupload_images', $this->upload->display_errors());
	        return FALSE;
	      }
	    }

	    return TRUE;
	}

	// ################### Check uploads Images. ######################

	public function fileupload_files()
	{
		$this->_data['img_thumb'] = array();
	    $number_of_files = sizeof(@$_FILES['file_thumb']['tmp_name']);
	    $files = @$_FILES['file_thumb'];
	 
	    // ถ้าต้องการเช็คอัพโหลดรูป
	 	if($files['tmp_name']) {
		    for($i=0;$i<$number_of_files;$i++)
		    {
		      if($_FILES['file_thumb']['error'][$i] != 0)
		      {
		        $this->form_validation->set_message(__FUNCTION__, 'กรุณาอัพโหลดรูปภาพ "หน้า Articles detail"');
		        return FALSE;
		      }
		    }
		}

		// ถ้าไม่ต้องการเช็คอัพโหลดไฟล์
		// if($files['tmp_name']) {

		// 	if($files['tmp_name'][0]==NULL) {

		// 		return TRUE;
		// 	}
		// } else {

		// 	return TRUE;
		// }

		$config['upload_path'] 		= FCPATH.'./public/uploads/articles/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '556';
		$config['max_height']  		= '361';

	    for ($i = 0; $i < $number_of_files; $i++)
	    {
	      $_FILES['file_thumb']['name'] 		= $files['name'][$i];
	      $_FILES['file_thumb']['type'] 		= $files['type'][$i];
	      $_FILES['file_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
	      $_FILES['file_thumb']['error'] 		= $files['error'][$i];
	      $_FILES['file_thumb']['size'] 		= $files['size'][$i];
	      
	      $this->upload->initialize($config);
	      if ($this->upload->do_upload('file_thumb'))
	      {
			$data  								= $this->upload->data();
			$this->_data['img_thumb'][] 	= $data['file_name'];
	      }
	      else
	      {
			$this->form_validation->set_message('fileupload_images', $this->upload->display_errors());
	        return FALSE;
	      }
	    }

	    return TRUE;
	}

} 
?>