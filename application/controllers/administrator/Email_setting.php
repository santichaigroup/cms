<?php
class Email_setting extends CI_Controller{
	
	var $_data = array();
	var $_menu_name = "";
	var $menu;
	var $submenu;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('upload');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('administrator/setting_email_model');
		$this->load->library('form_validation');

		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(2));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(2), $this->uri->segment(3));
	}
	public function index()
	{
		admin_redirect();
	}
	
	/************************************************** Setting Email Admin ************************************************/

	public function setting_email()
	{
		$rs = $this->setting_email_model->dataTable();
		
		if($rs->num_rows() > 0){
			$row = $rs->row_array();
			admin_redirect("email_setting/edit_mail/".$row['email_id']);
		}else{
			admin_redirect("email_setting/add_email");
				
		}
	}
	public function add_email($lang_id="TH"){
		
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;

		$this->_data['_menu_name'] 		= "New Contact Category";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "เพิ่มประเภทอีเมลติดต่อ";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->load->library('form_validation');
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");

		if($this->form_validation->run()===false){
			
			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("address/setting_email/add",$this->_data); 
			$this->admin_library->output();
		}else{
			
			$email_id = $this->setting_email_model->addData();
			$content_id = $this->setting_email_model->addLanguage($email_id,$lang_id);
			$this->setting_email_model->setDefaultContent($email_id,$content_id); 
			
			$this->setting_email_model->setDate(
				$email_id,
				$this->input->post("email_date")
			);

			$this->setting_email_model->updateContent(
				$email_id,
				$lang_id,
				$this->input->post("content_subject"),
				$this->input->post("content_email"),
				$this->input->post("content_email_cc"),
				$this->input->post("content_keyword"),
				$this->input->post("content_all_menu")
			);
			$this->setting_email_model->updateContentStatus(
				$email_id,
				$lang_id,
				$this->input->post("content_status")
			);
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("email_setting/setting_email");
		}
	}
	public function edit_mail($email_id,$lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;

		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$lang_id = strtoupper($lang_id);
		$this->setting_email_model->checkExistst($email_id);
		$this->_data['row'] = $this->setting_email_model->getDetail($email_id,$lang_id);

		if(!$this->_data['row']){
			$this->setting_email_model->addLanguage($email_id,$lang_id);
			$this->_data['row'] = $this->setting_email_model->getDetail($email_id,$lang_id);
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}

		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("address/setting_email/edit",$this->_data); 
			$this->admin_library->output();

		}else{

			$this->setting_email_model->setDate(
				$this->input->post("email_id"),
				$this->input->post("email_date")
			);
			
			$this->setting_email_model->updateContent(
				$this->input->post("email_id"),
				$this->input->post("lang_id"),
				$this->input->post("content_subject"),
				$this->input->post("content_email"),
				$this->input->post("content_email_cc"),
				$this->input->post("content_keyword"),
				$this->input->post("content_all_menu"),
				$this->input->post("content_sub_category")
			);
			$this->setting_email_model->updateContentStatus(
				$this->input->post("email_id"),
				$this->input->post("lang_id"),
				$this->input->post("content_status")
			);
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("email_setting/edit_mail/".$email_id);
		}
	}

}