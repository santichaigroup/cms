<?php 
class Template extends CI_Controller {
	
	var $_data = array();
	var $_menu_name = '';
	var $menu;
	var $submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('administrator/template_model');
		$this->load->library('form_validation');

		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(2));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(2), $this->uri->segment(3));
	}
	
	public function index($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("template_test/listview",$this->_data); 
		$this->admin_library->output();
	}

	public function load_datatable($lang_id="TH")
	{
		$result_data 	= $this->template_model->dataTable($lang_id);

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	public function add($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;
		
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_detail","รายละเอียด","trim|required");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","รูปภาพ","callback_fileupload_images");
		$this->form_validation->set_rules("file_thumb[]","อัพโหลดไฟล์","callback_fileupload_files");
		
		if($this->form_validation->run()===false) {
			
			$this->_data['_menu_name']	= "Add ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " เพิ่ม".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];
			
			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("template_test/add",$this->_data); 
			$this->admin_library->output();

		} else {
			
			$main_id = $this->template_model->addData();
			$content_id = $this->template_model->addLanguage($main_id,$lang_id);
			$this->template_model->setDefaultContent($main_id,$lang_id,$content_id);  
			
			$this->template_model->setDate(
				$main_id,
				$this->input->post("main_date")
			);

			$this->template_model->updateContent(
				$main_id,
				$lang_id,
				$this->input->post("content_subject"),
				$this->input->post("content_detail"), 
				$this->input->post("content_title"),
				$this->input->post("content_keyword"),
				$this->input->post("content_description")
			);
			
			$this->template_model->updateContentStatus(
				$main_id,
				$lang_id,
				$this->input->post("main_status")
			);

			$img_thumbnail = $this->_data['img_thumbnail'];
			if($img_thumbnail)
			{
				// อัพโหลดครั้งเดียวทุกภาษา
				foreach($img_thumbnail as $thumbnail )
				{
					$this->template_model->imsertContent(
						$main_id,
						$thumbnail
					);
				}
				// อัพโหลดภาษาละครั้ง
				// foreach($img_thumbnail as $thumbnail )
				// {
				// 	$this->template_model->imsertContent(
				// 		$main_id,
				// 		$thumbnail,
				// 		$lang_id
				// 	);
				// }
			}

			$file_thumb = $this->_data['file_thumbnail'];
			if($file_thumb)
			{
				// อัพโหลดครั้งเดียวทุกภาษา
				foreach($file_thumb as $thumbnail )
				{
					$this->template_model->imsertContent(
						$main_id,
						$thumbnail
					);
				}
				// อัพโหลดภาษาละครั้ง
				// foreach($file_thumb as $thumbnail )
				// {
				// 	$this->template_model->imsertContent(
				// 		$main_id,
				// 		$thumbnail,
				// 		$lang_id
				// 	);
				// }
			}

			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("template/edit/".$main_id."/".$lang_id);

		}
	}

	public function edit($main_id,$lang_id=NULL)
	{	
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;
		
		$this->_data['row'] = $this->template_model->getDetail($main_id,$lang_id);

		if(!$this->_data['row']){
			$this->template_model->addLanguage($main_id,$lang_id);
			$this->_data['row'] = $this->template_model->getDetail($main_id,$lang_id);
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}
		$this->admin_library->add_breadcrumb($this->admin_library->getLanguagename($this->_data['row']['lang_id']),"template/edit/".$this->_data['row']['main_id']."/".$this->_data['row']['lang_id'],"icon-globe");
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_detail","รายละเอียด","trim|required");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","รูปภาพ","callback_fileupload_images");
		$this->form_validation->set_rules("file_thumb[]","อัพโหลดไฟล์","callback_fileupload_files");
	
		if($this->form_validation->run()===false) {
			
			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " แก้ไข".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];
			
			$this->_data['rs_img'] = $this->template_model->getDetail_img($main_id);
			$this->_data['rs_file'] = $this->template_model->getDetail_file($main_id);
			
			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("template/edit",$this->_data); 
			$this->admin_library->output();

		} else {
			
			$this->template_model->setDate(
				$main_id,
				$this->input->post("main_date")
			);
			
			$this->template_model->updateContent(
				$main_id,
				$lang_id,
				$this->input->post("content_subject"),
				$this->input->post("content_detail"), 
				$this->input->post("content_title"),
				$this->input->post("content_keyword"),
				$this->input->post("content_description"),
				$this->input->post("update_date")
			);
			$this->template_model->updateContentStatus(
				$main_id,
				$lang_id,
				$this->input->post("main_status")
			);
			 
			$img_thumbnail = $this->_data['img_thumbnail'];
			if($img_thumbnail)
			{
				// อัพโหลดครั้งเดียวทุกภาษา
				foreach($img_thumbnail as $thumbnail )
				{
					$this->template_model->imsertContent(
						$main_id,
						$thumbnail
					);
				}
				// อัพโหลดภาษาละครั้ง
				// foreach($img_thumbnail as $thumbnail )
				// {
				// 	$this->template_model->imsertContent(
				// 		$main_id,
				// 		$thumbnail,
				// 		$lang_id
				// 	);
				// }
			}

			$file_thumb = $this->_data['file_thumbnail'];
			if($file_thumb)
			{
				// อัพโหลดครั้งเดียวทุกภาษา
				foreach($file_thumb as $thumbnail )
				{
					$this->template_model->imsertContent(
						$main_id,
						$thumbnail
					);
				}
				// อัพโหลดภาษาละครั้ง
				// foreach($file_thumb as $thumbnail )
				// {
				// 	$this->template_model->imsertContent(
				// 		$main_id,
				// 		$thumbnail,
				// 		$lang_id
				// 	);
				// }
			}
			 
			$img_id = $this->input->post('attachment_id'); 
			if(is_array($img_id) and count($img_id)>0)
			{
				$i=0;
				foreach($img_id as $id){
					$i++; 
					$this->template_model->setSequence($id,$i);
				}
			}
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("template/edit/".$main_id.'/'.$lang_id);
		}
	}
	
	function delete($main_id)
	{
		$this->template_model->deleteContent($main_id);

		$queryAll = $this->template_model->getAllContent();
		$queryResult = $this->template_model->getAllContent()->result_array();

		for($i=0; $i<$queryAll->num_rows(); $i++) {

			$this->template_model->set_sequence($queryResult[$i]['main_id'], ($i+1));
		}

		admin_redirect("template/index/");
	}

	public function handle_delete()
	{
		$main_id = $this->input->post("main_id");
		$lang_id = $this->input->post("lang_id");
	
		foreach($main_id as $id){
			$this->template_model->deleteContentSelect($id,$lang_id);
		}

		$queryAll = $this->template_model->getAllContent();
		$queryResult = $this->template_model->getAllContent()->result_array();

		for($i=0; $i<$queryAll->num_rows(); $i++) {

			$this->template_model->set_sequence($queryResult[$i]['main_id'], ($i+1));
		}

		admin_redirect("template/index");
	}

	public function handle_suspend()
	{
		$main_id = $this->input->post("main_id");
		$lang_id = $this->input->post("lang_id");
		
		foreach($main_id as $id){
			$this->template_model->setStatus($id,$lang_id,"pending");
		}
		admin_redirect("template/index/");
	}

	public function handle_unsuspend()
	{
		$main_id = $this->input->post("main_id");
		$lang_id = $this->input->post("lang_id");
		
		foreach($main_id as $id){
			$this->template_model->setStatus($id,$lang_id,"active");
		}
		admin_redirect("template/index/");
	}

	function delete_img($attachment_id,$default_main_id,$attachment_name,$lang_id,$type_name)
	{
		if($type_name == "images") {
			$path = './public/uploads/template/images/'.$attachment_name;
		} else {
			$path = './public/uploads/template/files/'.$attachment_name;
		}

		$rs = $this->template_model->deleteImage($attachment_id);
		if($rs)
		{
			if(unlink($path))
			{
				$this->session->set_flashdata("success_message","Content has been create image.");
			}else{
				$this->session->set_flashdata("error_message",'Error! Can not Delete Image');
			}
		}
		admin_redirect("template/edit/".$default_main_id."/".$lang_id);
	}

	function set_default_img($default_main_id,$content_id,$attachment_name,$lang_id)
	{
		$this->template_model->setDefaultImg($content_id,$attachment_name,$lang_id);

		admin_redirect("template/edit/".$default_main_id."/".$lang_id);
	}

	// ################### Check uploads Images. ######################

	public function fileupload_images()
	{
	    $number_of_files = sizeof($_FILES['image_thumb']['tmp_name']);
	 
	    $files = $_FILES['image_thumb'];
	 
	    // ถ้าต้องการเช็คอัพโหลดรูป
	 	// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['image_thumb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_images', 'กรุณาอัพโหลด"รูปภาพ"');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/template/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '640';
		$config['max_height']  		= '360';

	    for ($i = 0; $i < $number_of_files; $i++)
	    {
	      $_FILES['image_thumb']['name'] 		= $files['name'][$i];
	      $_FILES['image_thumb']['type'] 		= $files['type'][$i];
	      $_FILES['image_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
	      $_FILES['image_thumb']['error'] 		= $files['error'][$i];
	      $_FILES['image_thumb']['size'] 		= $files['size'][$i];
	      
	      $this->upload->initialize($config);
	      if ($this->upload->do_upload('image_thumb'))
	      {
			$data  								= $this->upload->data();
			$this->_data['img_thumbnail'][] 	= $data['file_name'];
	      }
	      else
	      {
			$this->form_validation->set_message('fileupload_images', $this->upload->display_errors());
	        return FALSE;
	      }
	    }

	    return TRUE;
	}

	// ################### Check uploads Images. ######################

	public function fileupload_files()
	{
	    $number_of_files = sizeof($_FILES['file_thumb']['tmp_name']);
	 
	    $files = $_FILES['file_thumb'];
	 
	    // ถ้าต้องการเช็คอัพโหลดรูป
	 	// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['file_thumb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_files', 'กรุณาอัพโหลด"ไฟล์เอกสาร"');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดไฟล์
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/template/files';
		$config['allowed_types'] 	= 'pdf|doc|docx|rar|zip|'; 
		$config['max_size']			= 204800;
		$config['encrypt_name'] 	= TRUE;

	    for ($i = 0; $i < $number_of_files; $i++)
	    {
	      $_FILES['file_thumb']['name'] 	= $files['name'][$i];
	      $_FILES['file_thumb']['type'] 	= $files['type'][$i];
	      $_FILES['file_thumb']['tmp_name'] = $files['tmp_name'][$i];
	      $_FILES['file_thumb']['error'] 	= $files['error'][$i];
	      $_FILES['file_thumb']['size'] 	= $files['size'][$i];
	      
	      $this->upload->initialize($config);
	      if ($this->upload->do_upload('file_thumb'))
	      {
			$data  								= $this->upload->data();
			$this->_data['file_thumbnail'][] 	= $data['file_name'];
	      }
	      else
	      {
			$this->form_validation->set_message('fileupload_files', $this->upload->display_errors());
	        return FALSE;
	      }
	    }

	    return TRUE;
	}

} 
?>