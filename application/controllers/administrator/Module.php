<?php
class Module extends CI_Controller {

	var $_data = array();
	var $_menu_name = '';
	var $menu;
	var $submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->load->library('template_library');
		$this->admin_library->forceLogin();
		$this->load->model('administrator/module_model');

		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(2));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(2), $this->uri->segment(3));
	}

	public function index()
	{
		admin_redirect('module/list_menu');
	}

	public function list_menu()
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		$this->_data['success_message'] = $this->session->flashdata('success_message');
		
		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("module/listview",$this->_data);
		$this->admin_library->output();
	}

	public function add_menu()
	{
		$this->_data['_menu_name'] 		= "Add New Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "เพิ่มเมนู";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("menu_label","Menu Label","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_title","Menu Detail","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_icon","Menu Icon","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_link","Menu Link","trim|required|max_length[255]");
		// $this->form_validation->set_rules("menu_seo","SEO","trim|required|max_length[255]");
		$this->form_validation->set_rules("menu_sequent","Menu Sequent","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("module/addview",$this->_data);
			$this->admin_library->output();

		} else {

			$menu_label 	= $this->input->post("menu_label");
			$menu_title 	= $this->input->post("menu_title");
			$menu_icon 		= $this->input->post("menu_icon");
			$menu_link 		= $this->input->post("menu_link");
			$menu_seo 		= $this->input->post("menu_link");
			$menu_sequent 	= $this->input->post("menu_sequent");

			$array_data		= 	array(
									'menu_label'	=>	ucfirst(strtolower($menu_label)),
									'menu_title'	=>	$menu_title,
									'menu_icon'		=>	$menu_icon,
									'menu_link'		=>	strtolower($menu_link),
									'menu_seo'		=>	$menu_seo,
									'menu_sequent'	=>	$menu_sequent
								);

			// Add Data to Database
			$add_menu 	=	$this->module_model->add_menu($array_data);
			// Create file controller
			$checked 	= 	$this->template_library->create_menu($array_data);
			// Check process
			if( $checked && $add_menu ) {

				$this->session->set_flashdata("success_message","Menu has been create.");
				admin_redirect($this->_data['_menu_link']."/list_menu");

			} else {

				$this->session->set_flashdata("error_message","Menu don't create.");
				admin_redirect($this->_data['_menu_link']."/list_menu");

			}

		}
	}

	public function edit_menu()
	{
		$this->_data['_menu_name'] 		= "Edit Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "แก้ไขเมนู";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		


		$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
		$this->admin_library->setDetail($this->_data['_menu_title']);
		$this->admin_library->view("module/editview",$this->_data);
		$this->admin_library->output();
	}

	public function add_sub_menu()
	{
		$this->_data['_menu_name'] 		= "Add Sub Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "เพิ่มเมนูย่อย";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		


		$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
		$this->admin_library->setDetail($this->_data['_menu_title']);
		$this->admin_library->view("module/addsubview",$this->_data);
		$this->admin_library->output();
	}

	public function edit_sub_menu()
	{
		$this->_data['_menu_name'] 		= "Edit Sub Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "แก้ไขเมนูย่อย";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		


		$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
		$this->admin_library->setDetail($this->_data['_menu_title']);
		$this->admin_library->view("module/editsubview",$this->_data);
		$this->admin_library->output();
	}

	public function cms_menu()
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("module/cmsview",$this->_data);
		$this->admin_library->output();
	}

	public function load_layout()
	{
		$result_data 	= $this->module_model->list_all_layout();

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "10",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	public function edit_cms_menu()
	{
		$this->_data['_menu_name'] 		= "Edit Sub Menu";
		$this->_data['_menu_icon']		= "glyphicon-plus-sign";
		$this->_data['_menu_title']		= "แก้ไขเมนูย่อย";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['result']			= $this->module_model->list_all_menu();
		


		$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
		$this->admin_library->setDetail($this->_data['_menu_title']);
		$this->admin_library->view("module/editsubview",$this->_data);
		$this->admin_library->output();
	}

	public function cms_template()
	{
		redirect(base_url().'public/core/');
	}

}
?>