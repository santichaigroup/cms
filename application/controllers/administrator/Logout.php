<?php
class Logout extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
	}
	function index()
	{
		$this->admin_library->logout();

		// IIIIIIIIIIIIIIIIIIIIII Access Logs IIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'action'		=> 'logout',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> 'User logout success.',
						'status' 		=> 'success'
					);
			
			$this->logs_library->access_log($logs);
		// IIIIIIIIIIIIIIIIIIIIII Access Logs IIIIIIIIIIIIIIIIIIIIII //

		admin_redirect("login");
	}
}