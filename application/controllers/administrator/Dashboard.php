<?php
class Dashboard extends CI_Controller{
	var $color;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->admin_library->forceLogin();
	}

	function index()
	{
		$this->admin_library->setTitle("Dashboard",'icon-home'); 
		$this->admin_library->setDetail("Stat &amp; Summary");
		// $this->admin_library->view("dashboard/index", $this->_data); 
		$this->admin_library->output();
	}

	function test()
	{
		// --- USER REPORT ---  //
		$_user_report = $this->report_library->get_user_report();
		$this->_data['user_report'] = $_user_report;

		$_user_chart_data = array();
		$_cnt = 0;
		foreach ($_user_report['member_gender'] as $key => $value) {
			$_user_chart_data[] = array(
				'value'   => $value, 
				'color'   => $this->color[$_cnt]['normal'], 
				'hilight' => $this->color[$_cnt]['hilight'], 
				'label'   => $key
			);

			$this->_data['user_report']['member_gender'][$key] = array(
				'number' => $value, 
				'css'    => $this->color[$_cnt]['css']
			);

			$_cnt++;
		}

		$this->_data['user_report']['gender_chart_data'] = json_encode($_user_chart_data);

		$_user_chart_data = array();
		$_cnt = 0;
		foreach ($_user_report['by_status'] as $key => $value) {
			$_user_chart_data[] = array(
				'value'   => $value, 
				'color'   => $this->color[$_cnt]['normal'], 
				'hilight' => $this->color[$_cnt]['hilight'], 
				'label'   => $key
			);

			$this->_data['user_report']['by_status'][$key] = array(
				'number' => $value, 
				'css'    => $this->color[$_cnt]['css']
			);

			$_cnt++;
		}

		$this->_data['user_report']['status_chart_data'] = json_encode($_user_chart_data);
		// --- USER REPORT ---  //

		// --- ORDER REPORT HOME COOK ---  //
		$_order_report = $this->report_library->get_order_report(1);
		$this->_data['order_report']['home_cook'] = $_order_report;

		$_order_chart_data = array();
		$_cnt = 0;
		foreach ($_order_report['by_status'] as $key => $value) {
			$_order_chart_data[] = array(
				'value'   => $value, 
				'color'   => $this->color[$_cnt]['normal'], 
				'hilight' => $this->color[$_cnt]['hilight'], 
				'label'   => $key
			);

			$this->_data['order_report']['home_cook']['by_status'][$key] = array(
				'number' => $value, 
				'css'    => $this->color[$_cnt]['css']
			);

			$_cnt++;
		}

		$this->_data['order_report']['home_cook']['order_chart_data'] = json_encode($_order_chart_data);
		// --- ORDER REPORT HOME COOK ---  //

		// --- ORDER REPORT FOOD SERVICE ---  //
		$_order_report = $this->report_library->get_order_report(2);
		$this->_data['order_report']['food_service'] = $_order_report;

		$_order_chart_data = array();
		$_cnt = 0;
		foreach ($_order_report['by_status'] as $key => $value) {
			$_order_chart_data[] = array(
				'value'   => $value, 
				'color'   => $this->color[$_cnt]['normal'], 
				'hilight' => $this->color[$_cnt]['hilight'], 
				'label'   => $key
			);

			$this->_data['order_report']['food_service']['by_status'][$key] = array(
				'number' => $value, 
				'css'    => $this->color[$_cnt]['css']
			);

			$_cnt++;
		}

		$this->_data['order_report']['food_service']['order_chart_data'] = json_encode($_order_chart_data);
		// --- ORDER REPORT FOOD SERVICE ---  //

		$_start_date = date('Y-m-01', time());
		$_end_date   = date('Y-m-t', time());
		$_lang_id    = 'TH';
		$_limit      = 10;

		// --- TOP 10 SKU REPORT HOME COOK ---  //
		$_sku_report = $this->report_library->get_top_ten_sell_by_sku(1, $_start_date, $_end_date, $_lang_id, $_limit);
		$this->_data['sku_report']['home_cook']['data'] = $_sku_report;

		$_sku_chart_data = array();
		$_cnt = 0;
		foreach ($_sku_report as $key => $value) {
			$_sku_chart_data[] = array(
				'value'   => $value['total_qty'], 
				'color'   => $this->color[$_cnt]['normal'], 
				'hilight' => $this->color[$_cnt]['hilight'], 
				'label'   => '(' . $value['sku_code'] . ') ' . $value['sku_name']
			);

			$this->_data['sku_report']['home_cook']['data'][$key]['css'] = $this->color[$_cnt]['css'];

			$_cnt++;
		}

		$this->_data['sku_report']['home_cook']['sku_chart_data'] = json_encode($_sku_chart_data);
		// --- TOP 10 SKU REPORT HOME COOK ---  //

		// --- TOP 10 SKU REPORT FOOD SERVICE ---  //
		$_sku_report = $this->report_library->get_top_ten_sell_by_sku(2, $_start_date, $_end_date, $_lang_id, $_limit);
		$this->_data['sku_report']['food_service']['data'] = $_sku_report;

		$_sku_chart_data = array();
		$_cnt = 0;
		foreach ($_sku_report as $key => $value) {
			$_sku_chart_data[] = array(
				'value'   => $value['total_qty'], 
				'color'   => $this->color[$_cnt]['normal'], 
				'hilight' => $this->color[$_cnt]['hilight'], 
				'label'   => '(' . $value['sku_code'] . ') ' . $value['sku_name']
			);

			$this->_data['sku_report']['food_service']['data'][$key]['css'] = $this->color[$_cnt]['css'];

			$_cnt++;
		}

		$this->_data['sku_report']['food_service']['sku_chart_data'] = json_encode($_sku_chart_data);
		// --- TOP 10 SKU REPORT FOOD SERVICE ---  //
		// echo "<pre>"; print_r($_sku_report); echo "</pre>"; exit;

		$this->admin_library->setTitle("Dashboard",'icon-home'); 
		$this->admin_library->setDetail("Stat &amp; Summary");
		$this->admin_library->view("dashboard/index", $this->_data); 
		$this->admin_library->output();
	}
}  