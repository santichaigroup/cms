<?php
class News extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
			parent::__construct();
			$lang 					= $this->uri->segment_array();
			$lang 					= end($lang);
			(empty($lang))?$lang 	= "TH" : $lang = "EN";
			$this->_data['lang']	= $lang;

			// Footer
				$this->_data['analytic'] 	= $this->footer_model->getAnalytic();
				$this->_data['address']		= $this->footer_model->address()->row_array();
	}

	public function index()
	{
		$lang = $this->_data['lang'];

		// News
		$this->_data['news'] 			= $this->settings_model->news($lang)->result_array();

		foreach ($this->_data['news'] as $key => $value) {
			
			$this->_data['news'][$key] = $value;
			$this->_data['news'][$key]['image'] = $this->settings_model->news_img($value['main_id'], 'highlight')
																		->row_array();
		}

		//Set Headder
		$this->_data['class'] 			= "news";
		$this->_data['meta_title'] 		= text_lang('NEWS & ACTIVITY', $lang);
		$this->_data['keyword'] 		= text_lang('meta_keyword_news_activity', $lang);
		$this->_data['tagDescription'] 	= text_lang('meta_description_news_activity', $lang);

		$this->_data['lang_en']			= "news/EN";
		$this->_data['lang_th']			= "news/TH";
		
		$this->load->view('include/header',$this->_data);
		$this->load->view('news/list',$this->_data);
		$this->load->view('include/footer',$this->_data);
	}

	public function news_detail($id, $lang)
	{
		if($id) {
			$lang = $this->_data['lang'];

			// News
			$news_all_lang					= $this->settings_model->news_detail($id)->result_array();
			$this->_data['news'] 			= $this->settings_model->news_detail($id, $lang)->row_array();
			$this->_data['news']['image'] 	= $this->settings_model->news_img($this->_data['news']['main_id'])->result_array();

			$this->_data['news_relate']		= $this->settings_model->news_relate($id, $lang)->result_array();
			if(isset($this->_data['news_relate'])) {
				foreach ($this->_data['news_relate'] as $key => $value) {
					
					$this->_data['news_relate'][$key] = $value;
					$this->_data['news_relate'][$key]['image'] = $this->settings_model->news_img($value['main_id'], 'highlight')
																							->row_array();
				}
			}

			//Set Headder
			$this->_data['class'] 			= "newsDetail";
			$this->_data['meta_title'] 		= $this->_data['news']['content_title'];
			$this->_data['keyword'] 		= $this->_data['news']['content_keyword'];
			$this->_data['tagDescription'] 	= $this->_data['news']['content_description'];

			foreach ($news_all_lang as $value) {
				
				$this->_data['lang_'.strtolower($value['lang_id'])]			= $value['content_seo'];
			}

			$this->load->view('include/header',$this->_data);
			$this->load->view('news/detail',$this->_data);
			$this->load->view('include/footer',$this->_data);
		} else {

			redirect('news/'.$lang);
		}
	}

/* End of file compress.php */
/* Location: ./system/application/hooks/compress.php */

}
?>