  <div class="footer">
    <div class="container">
      <div class="footerInner">
      
      <?php if  ($lang == 'TH'){ ?>
      
      <div style="text-align:right; padding-bottom:20px;">
        <a href="/about/TH">โพคารี่สเวท</a> | <a href="/articles/TH">บทความที่น่าสนใจ</a> | <a href="/product/TH">ผลิตภัณฑ์</a> | <a href="/news/TH">ข่าวสารและกิจกรรม</a> | <a href="/faq/TH">คำถามยอดฮิต</a> | <a href="/whereToBuy/TH">ช่องทางจัดจำหน่าย</a> | <a href="/contact/TH">ติดต่อเรา</a>
</div>
    <?php } ?>
        <div class="footerSocial">
          <div class="titleText">GET INSPIRED BY POCARI SWEAT</div>
          <div class="pcr_items">
            <div class="pcr_item">
              <a href="<?php echo $address['content_facebook']; ?>" target="_blank"><span><img src="img/footer-social-icon-fb.png"></span></a>
            </div>
            <div class="pcr_item">
              <a href="<?php echo $address['content_instagram']; ?>" target="_blank"><span><img src="img/footer-social-icon-ig.png"></span></a>
            </div>
            <div class="pcr_item">
              <a href="<?php echo $address['content_line']; ?>" target="_blank"><span><img src="img/footer-social-icon-line.png"></span></a>
            </div>
            <div class="pcr_item">
              <a href="<?php echo $address['content_youtube']; ?>" target="_blank"><span><img src="img/footer-social-icon-yt.png"></span></a>
            </div>
          </div>
        </div>
        <div class="footerCopyTermLink">
          <div class="footerLinks">
            <a href="javascript:;">TERMS &AMP; CONDITIONS</a>
            <a href="javascript:;">PRIVACY POLICY</a>
          </div>
          <div class="footerCopyright">
            Copy Right 2018 by Otsuka Nutraceutical (Thailand) Co., Ltd | All Rights Reserved.
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- globalScriptJS   -->
  <script src="js/all-vendor.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
  <script>
    // define page by classname
    // document.querySelector('html').classList.add('homePage');
  </script>

  <!-- slick slider -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
  <script>

    <?php if(@$this->youtube_id) { ?>
    // youtube api
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      var heroPlayer;
      function onYouTubeIframeAPIReady() {
        heroPlayer = new YT.Player('heroPlayer', {
          height: '390',
          width: '640',
          videoId: '<?php echo $this->youtube_id; ?>',
          events: {
            'onReady': onPlayerReady
            // ,'onStateChange': onStateChange
          }
          // ,playerVars: {rel: 0}
        });
      }
      // function onPlayerReady(event) {
      //   event.target.playVideo();
      // }
      // function onStateChange(event) {
      //   if (event.data === 0) {
      //     $('.heroBanner .pcr_items').slick('slickGoTo', 1);
      //   }
      // }

      function onPlayerReady(event) {
        var pauseButtonRight = document.getElementById("pause");
        pauseButtonRight.addEventListener("click", function() {
          heroPlayer.pauseVideo();
        });
        var pauseButtonLeft = document.getElementById("play");
        pauseButtonLeft.addEventListener("click", function() {
          heroPlayer.playVideo();
        });
      }

      // var done = false;
      // function onPlayerStateChange(event) {
      //   if (event.data == YT.PlayerState.PLAYING && !done) {
      //     setTimeout(stopVideo, 1000);
      //     done = true;
      //   }
      // }
      // function stopVideo() {
      //   console.log(heroPlayer);
      //   if(heroPlayer) {
      //     heroPlayer.pauseVideo();
      //   }
      // }

    <?php } ?>

    // slick slider
    $('.heroBanner .pcr_items').slick({
      arrows: false,
      dots: true,
      adaptiveHeight: true
    })    
    $('.heroBanner .pcr_items').on('beforeChange', function(event, slick, currentSlide, nextSlide){

      var youtube_slide = "<?php echo (@$this->youtube_slide ? @$this->youtube_slide : 0 ); ?>";
      <?php if(@$this->youtube_id) { ?>
        if (nextSlide == youtube_slide) {

          // heroPlayer.playVideo();
            $("#play").trigger('click');
        } else {

          // heroPlayer.pauseVideo();
          $("#pause").trigger('click');
        }
      <?php } ?>

    });    

    $('.productHighlightNav .image').on('click mouseenter',function() {
      var index = $(this).data('product');
      $('.productHighlight').attr('data-show', index);
    });

  </script>

  <?php
  if ($this->uri->segment(1) === 'about') :
  ?>
    <script>
      $(document).ready(function () {
        $('.aboutInThailand .row > *').matchHeight();
        $('.aboutGlobal .row > *').matchHeight();
        $('.aboutWhyPocari .row > *').matchHeight();
      });
    </script>
  <?php endif; ?>

  <?php
  if ($this->uri->segment(1) === 'news') :
  ?>
    <script>
      detectOrientation('.thumbnailTheme .image img');
      $(document).ready(function () {
        $('.newsThumbnailItems .thumbnailTheme .caption').matchHeight();
        $('.newsThumbnailItems .thumbnailTheme .imageWrap').matchHeight();
        $('.newsThumbnailHighlight .thumbnailTheme > *').matchHeight();
      });
    </script>
  <?php
  endif;
  ?>

  <?php
  if ($this->uri->segment(1) === 'articles') :
  ?>
  <script>
    detectOrientation('.thumbnailTheme .image img');
    $(document).ready(function () {
      $('.thumbnailItems .thumbnailTheme .caption').matchHeight();
      $('.thumbnailItems .thumbnailTheme .imageWrap').matchHeight();
    });
  </script>
  <?php
  endif;
  ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120400574-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-120400574-1');
	</script>

</body>

</html>