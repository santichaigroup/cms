<!DOCTYPE html>
<html class="<?php echo (!$this->uri->segment(1)?'index': $this->uri->segment(1) ); ?>Page lang<?php echo $lang; ?>">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $meta_title; ?> | POCARI SWEAT</title>
  <base href="<?php echo base_url('public'); ?>/" />
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="favicon/manifest.json">
  <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#ffffff">
  <link rel="shortcut icon" href="favicon/favicon.ico">
  <meta name="msapplication-config" content="favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

  <!-- build:css -->
  <link rel="stylesheet" href="css/main.css"/>
  <link rel="stylesheet" href="css/style.css"/>
  <!-- endbuild -->
</head>
<body>
<!--[if lt IE 8]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="headerWrap">
  <div class="container">
    <div class="header">
      <div class="headerInner">
        <div class="headerBrand">
          <a href="<?php echo site_url(''); ?>" class="headerBrandInner"><span><img src="img/brand<?php echo ($lang!="TH" ? "-".strtolower($lang) : ''); ?>.png"></span></a>
          <button class="btn btnHamburger" data-toggle="mainNavigation">
            <span class="btnHamburgerInner">
              <span>button </span>
              <span>toggle </span>
              <span>header </span>
            </span>
          </button>           
        </div>
        <nav class="mainNavigation">
          <ul class="navItems">
            <?php 
            $navigationItems = array(
              text_lang('POCARI SWEAT', $lang),
              text_lang('FACT', $lang),
              text_lang('PRODUCT', $lang),
              text_lang('NEWS & ACTIVITY', $lang),
              text_lang('FAQS', $lang),
              text_lang('WHERE TO BUT', $lang),
              text_lang('CONTACT US', $lang)
            );          
            $activePage = array(
              'about',
              'fact',
              'product',
              'news',
              'faq',
              'whereToBuy',
              'contact');
            $navigationLinks = array(
              site_url('about/'.$lang),
              site_url('articles/'.$lang),
              site_url('product/'.$lang),
              site_url('news/'.$lang),
              site_url('faq/'.$lang),
              site_url('whereToBuy/'.$lang),
              site_url('contact/'.$lang)
            );
            $navigationHasSubMenu = array(
              true,
              false,
              false,
              false,
              false,
              false,
              false);
            for ($i=0; $i < count($navigationItems) ; $i++) : ?>
            <li role="presentation" class="navItem <?php echo($this->uri->segment(1) === $activePage[$i] ? 'active':''); ?> <?php echo($navigationHasSubMenu[$i]? 'hasSubMenu':''); ?>">
              <a href="<?php echo($navigationLinks[$i]); ?>"><span><?php echo($navigationItems[$i]); ?></span></a>
              <?php if ($i === 0 && $this->uri->segment(1) === 'about') : ?>
              <ul class="navSubItems">
                <li class="navSubItem"><a href="#aboutCover" data-toggle="navSmoothScroll"><span><?php echo text_lang('about_history', $lang); ?></span></a></li>
                <li class="navSubItem"><a href="#aboutInThailand" data-toggle="navSmoothScroll"><span><?php echo text_lang('about_in_thai', $lang); ?></span></a></li>
                <li class="navSubItem"><a href="#aboutGlobal" data-toggle="navSmoothScroll"><span><?php echo text_lang('about_in_world', $lang); ?></span></a></li>
                <li class="navSubItem"><a href="#aboutWhyPocari" data-toggle="navSmoothScroll"><span><?php echo text_lang('about_why', $lang); ?></span></a></li>
                <li class="navSubItem"><a href="#aboutEveryWhere" data-toggle="navSmoothScroll"><span><?php echo text_lang('about_everyday', $lang); ?></span></a></li>
              </ul>
              <?php endif; ?>
            </li>      
            <?php endfor; ?>
            <li role="presentation" class="navItem isSwitchLang">
              <?php
              if($lang=="EN") {

                if($lang_th) {
              ?>
                <a href="<?php echo site_url($lang_th); ?>">
                  <span>ภาษาไทย</span>
                </a>
              <?php
                }
              } else {
                if(isset($lang_en)) {
              ?>
                <a href="<?php echo site_url($lang_en); ?>">
                  <span>English</span>
                </a>
              <?php
                }
              }
              ?>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>