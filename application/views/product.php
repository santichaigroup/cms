

   <?php if  ($lang == 'TH'){ ?>
  <div style="background:#bfdcff; padding-top:90px; ">
  
      
     <div style="max-width:1100px; margin:auto;  padding-bottom:20px; font-size:14px; padding-left:16px; padding-right:16px;">
     
       <div>
        <a href="/">หน้าหลัก</a> > ผลิตภัณฑ์
    </div>
     
     <p style="padding-top:20px;">เครื่องดื่มคุณภาพญี่ปุ่น เพื่อคนรักสุขภาพ ประกอบไปด้วยแร่ธาตุที่มีสัดส่วนใกล้เคียงกับของเหลวในร่างกาย เช่น โซเดียม แมกนีเซียม โพแทสเซียม และแคลเซียม จึงสามารถดูดซึมเข้าสู่ร่างกายได้ทันทีและคงอยู่ในร่างกายได้นานกว่าน้ำ สามารถดื่มได้ทุกเวลาที่ต้องการ ไม่ว่าจะเป็นการดื่มเพื่อเติมความสดชื่นระหว่างทำกิจกรรมต่างๆ หรือดื่มเพื่อเติมน้ำและแร่ธาตุที่สำคัญให้กับร่างกาย หลังเสียเหงื่อจากกิจกรรมต่างๆระหว่างวัน หรือดื่มหลังจากขาดน้ำในขณะที่อากาศร้อนจัด</p>
  
  </div>
 </div>
<?php } ?>

  <div class="productCover lvlBlock">
   
    <style scoped="">
    @media (min-width:768px) {
      .productCover {
        background-image:url(img/product-detail-cover<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png);
      }
    }
    </style>
    <div class="container">
      <div class="productCoverInner">
        <figure class="image">
          <img src="img/product-detail-cover-mobile<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="product350 lvlBlock">
    <style scoped="">
    @media (min-width:768px) {
      .product350 {
        background-image:url(img/product-detail-350<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png);
      }
    }
    </style>
    <div class="container">
      <div class="product350Inner">
        <figure class="image">
          <img src="img/product-detail-350-mobile<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png">
        </figure>
      </div>
    </div>
  </div>
  <div class="product500 lvlBlock">
    <style scoped="">
    @media (min-width:768px) {
      .product500 {
        background-image:url(img/product-detail-500<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png);
      }
    }
    </style>
    <div class="container">
      <div class="product500Inner">
        <figure class="image">
          <img src="img/product-detail-500-mobile<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png">
        </figure>
      </div>
    </div>
  </div>
