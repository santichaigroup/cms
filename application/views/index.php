  <div class="heroBanner lvlBlock">
    <div class="container-fluid">
      <div class="heroBannerInner">
        <div class="pcr_items">

        <?php foreach ($banner as $key => $value) { ?>
	        <?php
	        if($value['BType']==2) {

			    $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

			    if (preg_match($longUrlRegex, $value['content_detail'], $matches)) {
			        $this->youtube_id = $matches[count($matches) - 1];
              $this->youtube_slide = $key;
			    }
	        ?>
	          	<div class="pcr_item isVideo">
		            <!-- 16:9 aspect ratio -->
		            <div class="embed-responsive embed-responsive-16by9">
		              <div id="heroPlayer" class="embed-responsive-item"></div>
                  <input id="pause" type="submit" value="pause" style="display: none;" />
                  <input id="play" type="submit" value="play" style="display: none;" />
		            </div>
	          	</div>
	        <?php
	    	} else {

	    		$query = $this->db->select('banner_attachment.*')->from('banner_attachment')->where('default_main_id', $value['main_id'])->where('attachment_status', 'active')->get()->result_array();

          if ($lang=="TH") {
            $url = ( $value['content_url_th'] ? $value['content_url_th'] : "javascript:;" );
          } else {
            $url = ( $value['content_url_en'] ? $value['content_url_en'] : "javascript:;" );
          }
	        ?>
	           <div class="pcr_item" style="background-color:#6fc9ee;">
		            <figure class="image">
		              <a href="<?php echo $url; ?>" title="<?php echo $value['content_title']; ?>">
		            <?php
						foreach ($query as $q) {
		            ?>
		                <img src="<?php echo base_url('public/uploads/banner/images/'.$q['attachment_name']); ?>" alt="<?php echo $q['attachment_alt']; ?>" title="<?php echo $q['attachment_title']; ?>">
		            <?php
		            	}
		            ?>
		              </a>
		            </figure>
	          	</div>
	        <?php
	        }
	        ?>
	    <?php } ?>

        </div>
      </div>
    </div>
  </div>

  <div class="productHighlight lvlBlock" data-show="2">
    <div class="container">
        <?php if  ($lang == 'TH'){ ?>
       <div style="padding-bottom:50px; max-width:900px; text-align:left;">
       <h1 style="color:#007dc6;">เครื่องดื่มโพคารี่สเวท (POCARI SWEAT)</h1>  
       <p style="font-size:14px;">โพคารี่สเวท (POCARI SWEAT) เครื่องดื่มคุณภาพญี่ปุ่น เพื่อคนรักสุขภาพ ประกอบไปด้วยแร่ธาตุที่มีสัดส่วนใกล้เคียงกับของเหลวในร่างกาย จึงสามารถดูดซึมเข้าสู่ร่างกายได้ทันทีและคงอยู่ในร่างกายได้นานกว่าน้ำ สามารถดื่มได้ทุกเวลาที่ต้องการ ไม่ว่าจะเป็นการดื่มเพื่อเติมความสดชื่นระหว่างทำกิจกรรมต่างๆ หรือดื่มเพื่อเติมน้ำและแร่ธาตุที่สำคัญให้กับร่างกาย หลังเสียเหงื่อจากกิจกรรมต่างๆระหว่างวัน หรือดื่มหลังจากขาดน้ำในขณะที่อากาศร้อนจัด</p></div>
       <?php } ?>
    
      <div class="productHighlightInner">
        <div class="productHighlightNav">
          <div class="row">
            <div class="col-xs-6">
              <figure class="image" data-product="1"><img src="img/thumbnail-product-500.png" <?php if  ($lang == 'TH'){ ?> alt="POCARI SWEAT 500ml" <?php } ?>></figure>
            </div>
            <div class="col-xs-6">
              <figure class="image" data-product="2"><img src="img/thumbnail-product-350.png"  <?php if  ($lang == 'TH'){ ?> alt="POCARI SWEAT 350ml" <?php } ?> ></figure>
            </div>
          </div>
        </div>
        <div class="productHighlightDisplay">
          <div class="productHighlightDisplayInner">
            <img src="img/thumbnail-product-bg<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png" class="isBg">
            <div class="isText">
              <div><?php echo text_lang('ABOUT', $lang); ?></div>
              <div><?php echo text_lang('POCARI', $lang); ?></div>
              <div><?php echo text_lang('SWEAT', $lang); ?></div>
            </div>
            <div class="isButton">
              <button class="btn"><?php echo text_lang('READ MORE', $lang); ?></button>
            </div>
            <div class="productHighlightDisplayProducts">
              <a href="<?php echo site_url('product/'.$lang); ?>"><img src="img/thumbnail-product-show-500.png" <?php if  ($lang == 'TH'){ ?> alt="เกี่ยวกับเครื่องดื่มโพคารี่สเวท" <?php } ?>></a>
              <a href="<?php echo site_url('product/'.$lang); ?>"><img src="img/thumbnail-product-show-350.png" <?php if  ($lang == 'TH'){ ?> alt="เกี่ยวกับเครื่องดื่มโพคารี่สเวท" <?php } ?>></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="videoAndNews lvlBlock" >
    <div class="container">

      <div class="videoAndNewsInner">
      
        <div class="row">
          <div class="col-sm-8 isVideo">
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/NIoAcHoh3ng" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>            
          </div>
          <div class="col-sm-4">
            <figure class="image isNewsWithText">
              <a href="<?php echo site_url('whereToBuy/'.$lang); ?>">
                <img src="img/thumbnail-news<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png"   <?php if  ($lang == 'TH'){ ?> alt="ช่องทางจัดจำหน่าย" <?php } ?>>
                <span class="newsText">
                  <span><?php echo text_lang('Where To', $lang); ?></span>
                  <span><?php echo text_lang('BUY', $lang); ?></span>
                </span>
              </a>
              </figure>
          </div>
        </div>
      </div>
    </div>
  </div>