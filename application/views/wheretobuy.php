<div class="whereToBuyCover lvlBlock">
  <style scoped>
    .whereToBuyCover {
      background-image:url(img/whereToBuy-cover.png);
    }
  </style>
  <div class="whereToBuyCoverInner">
    <h1 class="lvlHeaderPage"><?php if  ($lang == 'TH'){ echo 'ช่องทางจัดจำหน่ายเครื่องดื่มโพคารี่สเวท'; } else{   echo text_lang('WHERE TO BUT', $lang); }?></h1>
  </div>
</div>
<div class="whereToBuyLists lvlBlock">
  <div class="container">
      <?php if  ($lang == 'TH'){ ?>
      
      <p style="font-size:14px; padding-bottom:50px;">สามารถหาซื้อเครื่องดื่มโพคารี่สเวท (POCARI SWEAT) ได้ที่ร้านสะดวกซื้อและซูปเปอร์มาร์เก็ตชั้นนำดังต่อไปนี้</p>
      <?php } ?>

    <div class="whereToBuyListsInner">
      <div class="row">
        <?php 
        foreach ($wheretobuy as $key => $value) {

          $query = $this->db->select('wheretobuy_attachment.*')->from('wheretobuy_attachment')->where('default_main_id', $value['main_id'])->where('attachment_status', 'active')->get()->result_array();

          if($value['content_detail']) {
            $content_detail = $value['content_detail'];
          } else {
            $content_detail = "javascript:;";
          }
        ?>
        <div class="col-lg-3 col-sm-4 col-xs-6">
          <figure class="image">
            <a href="<?php echo $content_detail; ?>"><span><img src="<?php echo base_url('public/uploads/wheretobuy/images/'.@$query[0]['attachment_name']); ?>"></span></a>
          </figure>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>