<div class="span9"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-edit"></i> เพิ่ม<?php echo $_menu_name; ?> (<?php echo $this->admin_library->getLanguagename($lang_id); ?>)</h4> 
      <span class="tools">
      
      </span>
      </div>
    <div class="widget-body form">
    <form method="post" name="optionform" id="optionform" enctype="multipart/form-data" >
        <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />
        <input type="hidden" name="main_date" id="main_date" value="<?php echo set_value("main_date",date("d-m-Y")); ?>" />
        <input type="hidden" name="main_status" id="main_status" value="<?php echo set_value("main_status"); ?>" />
        <input type="hidden" name="content_keyword" id="content_keyword" value="<?php echo set_value("content_keyword"); ?>" />
    <?php echo @$validation_errors; ?>
    <?php if(@$error_message!=NULL){ ?>
    	<div class="alert alert-error">
        	<button class="close" data-dismiss="alert">×</button>
            <strong>Error !</strong> <?php echo $error_message; ?>
        </div>
    <?php }?>
    		<div class="control-group">
                 <label class="control-label" >Image Thubnail : รูปขนาดไม่เกิน 1 M </label>
                 <div class="controls">
                    <input type="file" name="image_thumb" id="image_thumb" accept="image/*" />
                 </div>
            </div>
            
			<div class="control-group">
                <label class="control-label" for="input1">ห้วข้อ : &nbsp;<span style="color:#F00;">*</span></label>
                <div class="controls">
                    <input type="text" class="span10" id="content_subject" name="content_subject" value="<?php echo set_value("content_subject"); ?>">
                </div>
            </div>
         
            <div class="control-group">
                <label class="control-label" for="input1">ที่อยู่ : </label>
                <div class="controls">
                    <textarea class="span6" name="content_address" id="content_address" rows="3" ><?php echo set_value("content_address"); ?></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Phone : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_phone" name="content_phone" value="<?php echo set_value("content_phone"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Fax : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_fax" name="content_fax" value="<?php echo set_value("content_fax"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Email : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_email" name="content_email" value="<?php echo set_value("content_email"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Google Map : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_gmap" name="content_gmap" value="<?php echo set_value("content_gmap"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Facebook : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_fb" name="content_fb" value="<?php echo set_value("content_fb"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Twitter : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_twitter" name="content_twitter" value="<?php echo set_value("content_twitter"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Google+ : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_gplus" name="content_gplus" value="<?php echo set_value("content_gplus"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Instagram : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_ig" name="content_ig" value="<?php echo set_value("content_ig"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Pinterest : </label>
                <div class="controls">
                    <input type="text" class="span10" id="content_pinterest" name="content_pinterest" value="<?php echo set_value("content_pinterest"); ?>">
                </div>
            </div>
           
    </form>
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 
<div class="span3"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-check"></i> เครื่องมือเพิ่มเติม</h4> 
      <span class="tools">
            
      </span>
      </div>
    <div class="widget-body form">
       
        <div class="control-group">
            <label class="control-label">การแสดงผล :</label>
            <div class="controls">
                <select class="chosen" data-placeholder="สถานะ" name="main_status_select" id="main_status_select">
                   <option value="active" <?php if(set_value("main_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงผลเนื้อหานี้</option>
                   <option value="pending" <?php if(set_value("main_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงผล/แบบร่าง</option>
                </select>
            </div>
        </div>
		<div class="control-group">
            <label class="control-label">ภาษา :</label>
            <div class="controls btn-group">
                    <button class="btn dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/img/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
					<?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                     <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                    <?php foreach($this->admin_library->getLanguageList() as $lang){
							if($lang_id <> $lang['lang_id']){
							 ?>
						<li>
						<a href="<?php echo admin_url("contact/add/".$lang['lang_id']); ?>">
						<img src="assets/img/flags/<?php echo $lang['lang_flag']; ?>">
						&nbsp;<?php echo $lang['lang_name']; ?>
						</a></li>
                    <?php }} ?>
                    </ul>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">วันที่ :</label>
            <div class="controls">
                <div class="input-append date date-picker" data-date="<?php echo set_value("main_date",date("d-m-Y")); ?>" data-date-format="dd-mm-yyyy">
                   <input class="input-small date-picker" size="16" type="text" value="<?php echo set_value("main_date",date("d-m-Y")); ?>"  readonly="readonly" name="main_date_edit" id="main_date_edit" /><span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="control-group">
             <label class="control-label" for="content_keyword">Keyword </label>
             <div class="controls">
                <input type="text" class="span12" id="content_keyword_edit" name="content_keyword_edit" multiple value="<?php echo set_value("content_keyword"); ?>" />
                <span class="help-inline">ใช้ , ขั้นระหว่างคำ</span>
             </div>
        </div>
        <div class="form-actions">
        	<button type="submit" class="btn btn-success" onclick="save_form();">บันทึกการแก้ไข</button>
            <button type="button" class="btn btn-danger"  onclick="close_form();">ยกเลิก</button>		
        </div>
        
        
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 
<script type="text/javascript">
function save_form()
{
	$("#main_status").val($("#main_status_select").val());
	$("#main_date").val($("#main_date_edit").val());
	$("#content_keyword").val($("#content_keyword_edit").val());
	$("form#optionform").submit();	
}
function close_form()
{
	var x = confirm("Are you sure ?");
	if(x){
		$.getLocation(admin_url + "contact/index");	
	}
}
</script>