<div class="row">
  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo set_value("lang_id", $row['lang_id']); ?>">

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">

            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Form Box</h3>

        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
                </div>
            <?php }?>
            <!--  Error Alert  -->
              
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="content_subject" class="control-label">หัวข้อ: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value("content_subject", $row['content_subject']); ?>">
                </div>

                <!--<div class="form-group">
                  <label for="content_short" class="control-label">รายละเอียดย่อ: </label>
                    <textarea class="form-control" name="content_short" id="content_short" rows="6" cols="80"><?php echo set_value('content_short', $row['content_short']); ?></textarea>
                </div>-->

                <div class="form-group">
                  <label for="content_detail" class="control-label">รายละเอียดบนซ้าย: </label>
                    <textarea class="ckeditor" name="content_detail" id="content_detail" rows="10" cols="80">
                        <?php echo set_value("content_detail", $row['content_detail']); ?>
                    </textarea>
                </div>

                <div class="form-group">
                  <label for="content_detail_2" class="control-label">รายละเอียดขวาล่าง: </label>
                    <textarea class="ckeditor" name="content_detail_2" id="content_detail_2" rows="10" cols="80">
                        <?php echo set_value("content_detail_2", $row['content_detail_2']); ?>
                    </textarea>
                </div>

                <div class="form-group">
                  <label for="content_detail_3" class="control-label">รายละเอียดเพิ่มเติม (ล่างเต็มจอ): </label>
                    <textarea class="ckeditor" name="content_detail_3" id="content_detail_3" rows="10" cols="80">
                        <?php echo set_value('content_detail_3', $row['content_detail_3']); ?>
                    </textarea>
                </div>

                <?php
                if($rs_highlight) {
                ?>
                <div class="form-group">
                  <label for="content_subject" class="control-label">รูปภาพ Highlight (ปก) หน้า Articles list: </label>
                  <div class="row">
                    <div class="col-md-6 text-center">
                      <?php
                      if($rs_highlight['attachment_name']) {
                      ?>
                      <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$rs_highlight['attachment_name']); ?>">
                        <img src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$rs_highlight['attachment_name']); ?>" data-holder-rendered="true">
                      </a>

                      <div class="row" style="text-align: center;margin-bottom: 20px;">
                        <div class="col-md-12">
                          <div class="form-group text-left">
                            <label for="attachment_alt" class="control-label text-left">Alternate Text: </label>
                            <input type="text" name="attachment_alt[<?php echo $rs_highlight['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_alt[".$rs_highlight['attachment_id']."]", $rs_highlight['attachment_alt']); ?>">
                          </div>
                          <div class="form-group text-left">
                            <label for="attachment_title" class="control-label text-left">Title: </label>
                            <input type="text" name="attachment_title[<?php echo $rs_highlight['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_title[".$rs_highlight['attachment_id']."]", $rs_highlight['attachment_title']); ?>">
                          </div>

                          <a href="javascript:;" onclick="delete_img('<?php echo $rs_highlight['attachment_id']; ?>','<?php echo $rs_highlight['default_main_id']; ?>','<?php echo $rs_highlight['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                          </a>
                        </div>
                      </div>
                      <input type="hidden" name="attachment_id[]" value="<?php echo $rs_highlight['attachment_id']; ?>"> 
                      <?php
                      } else {
                      ?>
                      <a href="javascript:;" class="thumbnail">
                        <img src="<?php echo base_url("public/images/thumbnail-default.jpg"); ?>" data-holder-rendered="true">
                      </a>
                      <?php
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <?php
                } else {
                ?>
                  <div class="form-group highlight">
                    <label for="content_subject" class="control-label">อัพโหลดรูปภาพ Highlight (ปก) หน้า list: </label>
                      <input type="file" name="file_highlight[]" id="file_highlight" accept="image/*"/>
                      <p class="help-block">ขนาดรูป 556x313 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                  </div>content_detail_2
                <?php
                }
                ?>

                <?php
                if(count($rs_file)>1) {
                  $j=1;
                ?>
                <div class="form-group">
                  <label for="content_subject" class="control-label">รูปภาพ หน้า Articles detail บนซ้าย: </label>
                  <div class="row">
                    <div class="col-md-6 text-center">
                      <?php
                      if($rs_file[0]['attachment_name']) {
                      ?>
                      <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$rs_file[0]['attachment_name']); ?>">
                        <img src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$rs_file[0]['attachment_name']); ?>" data-holder-rendered="true">
                      </a>

                      <div class="row" style="text-align: center;margin-bottom: 20px;">
                        <div class="col-md-12">
                          <div class="form-group text-left">
                            <label for="attachment_alt_dt" class="control-label text-left">Alternate Text: </label>
                            <input type="text" name="attachment_alt_dt[<?php echo $rs_file[0]['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_alt_dt[".$rs_file[0]['attachment_id']."]", $rs_file[0]['attachment_alt']); ?>">
                          </div>
                          <div class="form-group text-left">
                            <label for="attachment_title_dt" class="control-label text-left">Title: </label>
                            <input type="text" name="attachment_title_dt[<?php echo $rs_file[0]['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_title_dt[".$rs_file[0]['attachment_id']."]", $rs_file[0]['attachment_title']); ?>">
                          </div>

                          <a href="javascript:;" onclick="delete_img('<?php echo $rs_file[0]['attachment_id']; ?>','<?php echo $rs_file[0]['default_main_id']; ?>','<?php echo $rs_file[0]['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                          </a>
                        </div>
                      </div>
                      <input type="hidden" name="attachment_id_dt[]" value="<?php echo $rs_file[0]['attachment_id']; ?>"> 
                      <?php
                      } else {
                      ?>
                      <a href="javascript:;" class="thumbnail">
                        <img src="<?php echo base_url("public/images/thumbnail-default.jpg"); ?>" data-holder-rendered="true">
                      </a>
                      <?php
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <?php
                } else {
                  $j=0;
                ?>
                  <div class="form-group">
                    <label for="content_subject" class="control-label">อัพโหลดรูปภาพ หน้า Articles detail บนซ้าย: &nbsp;<span style="color:#F00;">*</span></label>
                      <input type="file" name="file_thumb[]" accept="image/*"/>
                      <p class="help-block">ขนาดรูป 555x361 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                  </div>
                <?php
                }
                ?>

                <?php
                if(count(@$rs_file)) {
                ?>
                <div class="form-group">
                  <label for="content_subject" class="control-label">รูปภาพ หน้า Articles detail ขวาล่าง: </label>
                  <div class="row">
                    <div class="col-md-6 text-center">
                      <?php
                      if($rs_file[$j]['attachment_name']) {
                      ?>
                      <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$rs_file[$j]['attachment_name']); ?>">
                        <img src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$rs_file[$j]['attachment_name']); ?>" data-holder-rendered="true">
                      </a>

                      <div class="row" style="text-align: center;margin-bottom: 20px;">
                        <div class="col-md-12">
                          <div class="form-group text-left">
                            <label for="attachment_alt_dt" class="control-label text-left">Alternate Text: </label>
                            <input type="text" name="attachment_alt_dt[<?php echo $rs_file[$j]['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_alt_dt[".$rs_file[$j]['attachment_id']."]", $rs_file[$j]['attachment_alt']); ?>">
                          </div>
                          <div class="form-group text-left">
                            <label for="attachment_title_dt" class="control-label text-left">Title: </label>
                            <input type="text" name="attachment_title_dt[<?php echo $rs_file[$j]['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_title_dt[".$rs_file[$j]['attachment_id']."]", $rs_file[$j]['attachment_title']); ?>">
                          </div>

                          <a href="javascript:;" onclick="delete_img('<?php echo $rs_file[$j]['attachment_id']; ?>','<?php echo $rs_file[$j]['default_main_id']; ?>','<?php echo $rs_file[$j]['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                          </a>
                        </div>
                      </div>
                      <input type="hidden" name="attachment_id_dt[]" value="<?php echo $rs_file[$j]['attachment_id']; ?>"> 
                      <?php
                      } else {
                      ?>
                      <a href="javascript:;" class="thumbnail">
                        <img src="<?php echo base_url("public/images/thumbnail-default.jpg"); ?>" data-holder-rendered="true">
                      </a>
                      <?php
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <?php
                } else {
                ?>
                  <div class="form-group">
                    <label for="content_subject" class="control-label">อัพโหลดรูปภาพ หน้า Articles detail ขวาล่าง: &nbsp;<span style="color:#F00;">*</span></label>
                      <input type="file" name="file_thumb[]" accept="image/*"/>
                      <p class="help-block">ขนาดรูป 556x360 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                  </div>
                <?php
                }
                ?>
            </div>

              <div class="col-md-3">
                <div class="form-group">
                    <label for="menu_status" class="control-label">เปลี่ยนภาษา: </label><br>
                    <div class="controls btn-group">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">
                            <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                              <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                             <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <?php foreach($this->admin_library->getLanguageList() as $lang){
                          if($row['lang_id'] <> $lang['lang_id']){
                          ?>
                            <li>
                              <a href="<?php echo admin_url($this->menu['menu_link']."/edit/".$row['main_id']."/".$lang['lang_id']); ?>">
                                <img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?>
                              </a>
                            </li>
                          <?php }} ?>
                        </ul>
                    </div>
                </div>

                <!-- <div class="form-group">
                    <label for="create_by" class="control-label">เขียนโดย: </label>
                    <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                </div> -->

                <div class="form-group">
                    <label for="post_date" class="control-label">เขียนเมื่อ: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",$row['post_date']); ?>"  readonly="readonly">
                    </div>
                </div>
                <?php
                if($row['update_by']) {
                ?>
                <div class="form-group">
                    <label for="upadte_by" class="control-label">แก้ไขโดย: </label>
                    <input type="text" class="form-control" name="upadte_by" id="upadte_by" value="<?php $update_name = $this->admin_library->getuserinfo($row['update_by']); echo $update_name['user_fullname']; ?>" disabled>
                </div>
                <?php
                }
                ?>
                <?php
                if($row['update_date']) {
                ?>
                <div class="form-group">
                    <label for="update_date" class="control-label">แก้ไขเมื่อ: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right active date-picker" name="update_date" id="update_date" value="<?php echo set_value("update_date",$row['update_date']); ?>" readonly="readonly">
                    </div>
                </div>
                <?php
                }
                ?>
                <div class="form-group">
                    <label for="content_status" class="control-label">การแสดงผล: </label>

                    <select name="content_status" id="content_status" class="form-control">
                      <option value="active" <?php if(set_value("content_status", $row['content_status'])=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                      <option value="pending" <?php if(set_value("content_status", $row['content_status'])=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                    </select>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
                    <a href="<?php echo admin_url($_menu_link."/index/".$lang_id); ?>" class="btn btn-danger">ย้อนกลับ</a>
                </div>
              </div>
            </div>
            
        </div>
      </div>
    </div>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">

            <div class="form-group">
              <label for="content_seo" class="control-label">Url: </label>
              <input type="text" name="content_seo" class="form-control" id="content_seo" placeholder="" value="<?php echo set_value("content_seo", $row['content_seo']); ?>">
            </div>

            <div class="form-group">
              <label for="content_title" class="control-label">Title: </label>
              <input type="text" name="content_title" class="form-control" id="content_title" placeholder="" value="<?php echo set_value("content_title", $row['content_title']); ?>">
            </div>

            <div class="form-group">
              <label for="content_description" class="control-label">Meta Description: </label>
              <input type="text" name="content_description" class="form-control" id="content_description" placeholder="" value="<?php echo set_value("content_description", $row['content_description']); ?>">
            </div>

            <div class="form-group">
              <label for="content_keyword" class="control-label">Meta Keywords: </label>
              <input type="text" name="content_keyword" class="form-control" id="content_keyword" placeholder="" value="<?php echo set_value("content_keyword", $row['content_keyword']); ?>">
              <p class="help-block">ใช้ , คั่นระหว่างคำ</p>
            </div>
              
        </div>
      </div>
    </div>

  </form>
</div>

<script type="text/javascript">

  function save_form()
  {
    $("form#optionform").submit();
  }

  function delete_img(attachment_id,default_main_id,attachment_name,lang_id,type_name)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#optionform").attr("action",admin_url+"<?php echo $this->menu['menu_link']; ?>/delete_img/"+attachment_id+"/"+default_main_id+"/"+attachment_name+"/"+lang_id+"/"+type_name);
    $("#optionform").submit();
    }
  }

  function set_default_img(default_main_id,content_id,attachment_name,lang_id)
  {
    if(confirm("Set image default. Are you sure ?")){
    $("#optionform").attr("action",admin_url+"<?php echo $this->menu['menu_link']; ?>/set_default_img/"+default_main_id+"/"+content_id+"/"+attachment_name+"/"+lang_id);
    $("#optionform").submit();
    }
  }
</script>