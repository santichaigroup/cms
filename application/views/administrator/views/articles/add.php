<div class="row">
  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Form Box</h3>
        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
                </div>
            <?php }?>
            <!--  Error Alert  -->
            
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="content_subject" class="control-label">หัวข้อ: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value('content_subject'); ?>">
                </div>

                <!--<div class="form-group">
                  <label for="content_short" class="control-label">รายละเอียดย่อ: </label>
                    <textarea class="form-control" name="content_short" id="content_short" rows="6" cols="80"><?php echo set_value('content_short'); ?></textarea>
                </div>-->

                <div class="form-group">
                  <label for="content_detail" class="control-label">รายละเอียดบนซ้าย: </label>
                    <textarea class="ckeditor" name="content_detail" id="content_detail" rows="10" cols="80">
                        <?php echo set_value('content_detail'); ?>
                    </textarea>
                </div>

                <div class="form-group">
                  <label for="content_detail_2" class="control-label">รายละเอียดขวาล่าง: </label>
                    <textarea class="ckeditor" name="content_detail_2" id="content_detail_2" rows="10" cols="80">
                        <?php echo set_value('content_detail_2'); ?>
                    </textarea>
                </div>

                <div class="form-group">
                  <label for="content_detail_3" class="control-label">รายละเอียดเพิ่มเติม (ล่างเต็มจอ): </label>
                    <textarea class="ckeditor" name="content_detail_3" id="content_detail_3" rows="10" cols="80">
                        <?php echo set_value('content_detail_3'); ?>
                    </textarea>
                </div>

                <!--<div class="form-group">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ: </label>
                    <input type="file" name="image_thumb[]" id="image_thumb" accept="image/*" multiple/>
                    <p class="help-block">ขนาดรูป 640x360 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 6 รูป</p>
                </div>-->

                <!--<div class="form-group">
                  <label for="content_subject" class="control-label">อัพโหลดไฟล์: </label>
                    <input type="file" name="file_thumb[]" id="file_thumb" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf"/>
                    <p class="help-block">ไฟล์ขนาดไม่เกิน 20 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .pdf, doc, docx, rar, zip จำนวน 1 ไฟล์</p>
                </div>-->
                
                <div class="form-group highlight">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ Articles (ปก) หน้า list: </label>
                    <input type="file" name="file_highlight[]" id="file_highlight" accept="image/*"/>
                    <p class="help-block">ขนาดรูป 556x313 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                </div>

                <div class="form-group">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ หน้า Articles detail บนซ้าย: &nbsp;<span style="color:#F00;">*</span></label>
                    <input type="file" name="file_thumb[]" id="file_thumb" accept="image/*"/>
                    <p class="help-block">ขนาดรูป 555x361 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                </div>

                <div class="form-group">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ หน้า Articles detail ขวาล่าง: &nbsp;<span style="color:#F00;">*</span></label>
                    <input type="file" name="file_thumb[]" id="file_thumb" accept="image/*"/>
                    <p class="help-block">ขนาดรูป 556x360 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                </div>
              </div>

              <div class="col-md-3">
                  <div class="form-group">
                    <label for="menu_status" class="control-label">เปลี่ยนภาษา: </label><br>
                    <div class="controls btn-group">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">
                        <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                          <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                         <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <?php foreach($this->admin_library->getLanguageList() as $lang){
                          if($lang_id <> $lang['lang_id']){
                        ?>
                          <li>
                            <a href="<?php echo admin_url($this->menu['menu_link'].$this->submenu['menu_link']."/add/".$lang['lang_id']); ?>"><img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?></a>
                          </li>
                        <?php }} ?>
                      </ul>
                    </div>
                  </div>

                  <!-- <div class="form-group">
                      <label for="create_by" class="control-label">เขียนโดย: </label>
                      <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                  </div> -->

                  <div class="form-group">
                    <label for="post_date" class="control-label">เขียนเมื่อ: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",date("d-m-Y")); ?>"  readonly="readonly">
                    </div>
                  </div>

                  <!-- <div class="form-group">
                      <label for="upadte_by" class="control-label">แก้ไขโดย: </label>
                      <input type="text" class="form-control" name="upadte_by" id="upadte_by" disabled>
                  </div>

                  <div class="form-group">
                      <label for="update_date" class="control-label">แก้ไขเมื่อ: </label>
                      <input type="text" class="form-control" name="update_date" id="update_date">
                  </div> -->

                  <div class="form-group">
                    <label for="content_status" class="control-label">การแสดงผล: </label>

                    <select name="content_status" id="content_status" class="form-control">
                      <option value="active" <?php if(set_value("content_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                      <option value="pending" <?php if(set_value("content_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                      <option value="deleted">ลบข้อมูล</option>
                    </select>
                  </div>

                  <div class="box-footer">
                      <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
                      <a href="<?php echo admin_url($_menu_link); ?>" class="btn btn-danger">ย้อนกลับ</a>
                  </div>
              </div>
            </div>

        </div>
      </div>
    </div>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">

            <div class="form-group">
              <label for="content_title" class="control-label">Title: </label>
              <input type="text" name="content_title" class="form-control" id="content_title" placeholder="หัวข้อเว็บไซต์" value="<?php echo set_value('content_title'); ?>">
            </div>

            <div class="form-group">
              <label for="content_description" class="control-label">Meta Description: </label>
              <input type="text" name="content_description" class="form-control" id="content_description" placeholder="รายละเอียดเว็บไซต์" value="<?php echo set_value('content_description'); ?>">
            </div>

            <div class="form-group">
              <label for="content_keyword" class="control-label">Meta Keywords: </label>
              <input type="text" name="content_keyword" class="form-control" id="content_keyword" placeholder="คำค้นหา สำหรับ SEO" value="<?php echo set_value('content_keyword'); ?>">
              <p class="help-block">ใช้ , คั่นระหว่างคำ</p>
            </div>
              
        </div>
      </div>
    </div>

  </form>
</div>

<script type="text/javascript">
  function save_form()
  {
    $("form#optionform").submit();
  }
</script>