<!-- TOP SKU Report -->
<div class="row">
  <div class="col-md-12">

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Top Seller SKU Report (Current Month)</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-6">
          <h2 class="page-header">Home Cook</h2>
          <div class="col-md-12">
            <canvas id="topSkuHomeCook" style="height: 247px; width: 495px;" width="495" height="247"></canvas>
          </div>
          <div class="col-md-12">
            <ul class="nav nav-stacked">
              <?php foreach ($sku_report['home_cook']['data'] as $key => $value): ?>
              <li>(<?php echo $value['sku_code']; ?>) <?php echo $value['sku_name']; ?><span class="pull-right badge <?php echo $value['css']; ?>"><?php echo $value['total_qty']; ?></span></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="col-md-6">
          <h2 class="page-header">Food Service</h2>
          <div class="col-md-12">
            <canvas id="topSkuFoodService" style="height: 247px; width: 495px;" width="495" height="247"></canvas>
          </div>
          <div class="col-md-12">
            <ul class="nav nav-stacked">
              <?php foreach ($sku_report['food_service']['data'] as $key => $value): ?>
              <li>(<?php echo $value['sku_code']; ?>) <?php echo $value['sku_name']; ?><span class="pull-right badge <?php echo $value['css']; ?>"><?php echo $value['total_qty']; ?></span></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div><!-- /.box-body -->
    </div>
  </div>
</div>
<!-- /TOP SKU Report -->

<!-- Order Report -->
<div class="row">
  <div class="col-md-12">

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Order Report</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-6">
            <h2 class="page-header">Home Cook</h2>
            <div class="col-md-12">
              <canvas id="orderStatusHomeCook" style="height: 247px; width: 495px;" width="495" height="247"></canvas>
            </div>
            <div class="col-md-12">
              <ul class="nav nav-stacked">
                <?php foreach ($order_report['home_cook']['by_status'] as $key => $value): ?>
                <li><?php echo $key; ?><span class="pull-right badge <?php echo $value['css']; ?>"><?php echo $value['number']; ?></span></li>
                <?php endforeach; ?>
              </ul>
            </div>
        </div>
        <div class="col-md-6">
            <h2 class="page-header">Food Service</h2>
            <div class="col-md-12">
              <canvas id="orderStatusFoodService" style="height: 247px; width: 495px;" width="495" height="247"></canvas>
            </div>
            <div class="col-md-12">
              <ul class="nav nav-stacked">
                <?php foreach ($order_report['food_service']['by_status'] as $key => $value): ?>
                <li><?php echo $key; ?><span class="pull-right badge <?php echo $value['css']; ?>"><?php echo $value['number']; ?></span></li>
                <?php endforeach; ?>
              </ul>
            </div>
        </div>
        
      </div><!-- /.box-body -->
    </div>
  </div>
</div>
<!-- /Order Report -->

<!-- Member Report -->
<div class="row">
  <div class="col-md-12">

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Member Report</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-6">
          <h2 class="page-header">By Gender</h2>
          <div class="col-md-12">
            <canvas id="memberGender" style="height: 247px; width: 495px;" width="495" height="247"></canvas>
          </div>
          <div class="col-md-12">
            <ul class="nav nav-stacked">
              <?php foreach ($user_report['member_gender'] as $key => $value): ?>
              <li><?php echo $key; ?><span class="pull-right badge <?php echo $value['css']; ?>"><?php echo $value['number']; ?></span></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="col-md-6">
          <h2 class="page-header">By Status</h2>
          <div class="col-md-12">
            <canvas id="memberStatus" style="height: 247px; width: 495px;" width="495" height="247"></canvas>
          </div>
          <div class="col-md-12">
            <ul class="nav nav-stacked">
              <?php foreach ($user_report['by_status'] as $key => $value): ?>
              <li><?php echo $key; ?><span class="pull-right badge <?php echo $value['css']; ?>"><?php echo $value['number']; ?></span></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div><!-- /.box-body -->
    </div>
  </div>
</div>
<!-- /Member Report -->
<script type="text/javascript">
function generate_member_gender_pie_chart() {
  //---------------------------
  //- PIE CHART Member Gender -
  //---------------------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#memberGender").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $user_report['gender_chart_data']; ?>;
  var pieOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 50, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
}

function generate_member_status_pie_chart() {
  //---------------------------
  //- PIE CHART Member Gender -
  //---------------------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#memberStatus").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $user_report['status_chart_data']; ?>;
  var pieOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 50, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
}

function generate_order_status_home_cook_pie_chart() {
  //------------------------------------
  //- PIE CHART ORDER STATUS HOME COOK -
  //------------------------------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#orderStatusHomeCook").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $order_report['home_cook']['order_chart_data']; ?>;
  var pieOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 50, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
}

function generate_order_status_food_service_pie_chart() {
  //------------------------------------
  //- PIE CHART ORDER STATUS HOME COOK -
  //------------------------------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#orderStatusFoodService").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $order_report['food_service']['order_chart_data']; ?>;
  var pieOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 50, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
}

function generate_top_sku_home_cook_pie_chart() {
  //---------------------------
  //- PIE CHART Member Gender -
  //---------------------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#topSkuHomeCook").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $sku_report['home_cook']['sku_chart_data']; ?>;
  var pieOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 50, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
}

function generate_top_sku_food_service_pie_chart() {
  //---------------------------
  //- PIE CHART Member Gender -
  //---------------------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#topSkuFoodService").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = <?php echo $sku_report['food_service']['sku_chart_data']; ?>;
  var pieOptions = {
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 50, // This is 0 for Pie charts
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
}

generate_member_gender_pie_chart();
generate_member_status_pie_chart();
generate_order_status_home_cook_pie_chart();
generate_order_status_food_service_pie_chart();
generate_top_sku_home_cook_pie_chart();
generate_top_sku_food_service_pie_chart();
</script>