<div class="row">
  <div class="col-md-7">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Google Analytics</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
          <?php }?>
          <!--  Error Alert  -->

          <form name="user_listform" id="user_listform" method="post" enctype="multipart/form-data" role="form">
              <input type="hidden" name="user_status" id="user_status" value="<?php echo set_value("user_status"); ?>" />
            
              <div class="form-group">
                <label for="group_name" class="control-label">Tracking ID: &nbsp;</label>
                <input type="text" class="form-control" id="analytic_tracking_id" name="analytic_tracking_id" placeholder="Google Analytics Tracking ID. Ex. UA-12345678-1" value="<?php echo set_value("analytic_tracking_id", $analytic_tracking_id); ?>">
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Tracking All Event: &nbsp;</label>

                <select class="form-control" name="analytic_tracking_event" id="analytic_tracking_event">
                  <option value="yes">Yes</option>
                    <option value="no" <?php if(set_value("analytic_tracking_event", $analytic_tracking_event)=="no"){ ?> selected <?php } ?>>No</option>
                </select>
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Additional Tracking Code: &nbsp;</label>

                <textarea class="form-control" name="analytic_tracking_add_code" id="analytic_tracking_add_code" rows="10" ><?php echo set_value("analytic_tracking_add_code", $analytic_tracking_add_code); ?></textarea>
              </div>
          </form>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-offset-1 col-md-4">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>

      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/userlist" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>

</div>

<script type="text/javascript">
  function save_form()
  {
    $("#user_status").val($("#user_status_select").val());
    $("#main_date").val($("#post_date").val());
    $("form#user_listform").submit();
  }
</script>