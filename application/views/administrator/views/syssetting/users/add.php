<div class="row">
  <div class="col-md-8">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
          <?php }?>
          <!--  Error Alert  -->

          <form name="user_listform" id="user_listform" method="post" enctype="multipart/form-data" role="form">
              <input type="hidden" name="main_date" id="main_date" value="<?php echo set_value("main_date",date("d-m-Y")); ?>" />
              <input type="hidden" name="user_status" id="user_status" value="<?php echo set_value("user_status"); ?>" />
            
              <div class="form-group">
                <label for="user_fullname" class="control-label">Full Name: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="user_fullname" class="form-control" id="user_fullname" placeholder="กรุณากรอกชื่อ-นามสกุล" value="<?php echo set_value('user_fullname'); ?>">
              </div>

              <div class="form-group">
                <label for="user_email" class="control-label">Email: &nbsp;<span style="color:#F00;">*</span></label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="glyphicon glyphicon-envelope"></i>
                    </div>
                    <input type="text" name="user_email" class="form-control" id="user_email" placeholder="กรุณากรอกอีเมล" value="<?php echo set_value('user_email'); ?>">
                </div>
              </div>

              <div class="form-group">
                <label for="user_mobileno" class="control-label">Mobile Phone: </label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="glyphicon glyphicon-phone"></i>
                    </div>
                    <input type="number" name="user_mobileno" class="form-control" id="user_mobileno" placeholder="กรุณากรอกเบอร์โทรศัพท์" value="<?php echo set_value('user_mobileno'); ?>">
                </div>
              </div>

              <div class="form-group">
                   <label class="control-label" for="user_group">User Group: &nbsp;<span style="color:#F00;">*</span></label>
                   <div class="controls">
                      <select name="user_group" class="form-control">
                         <option value="">กรุณาเลือก User Group</option>
                         <?php foreach($this->admin_library->getAllGroup()->result_array() as $row){ ?>
                         <option value="<?php echo $row['group_id']; ?>" <?php if(set_value("user_group",@$rs['user_group'])==$row['group_id']){ ?> selected="selected" <?php } ?>><?php echo $row['group_name']; ?></option>
                         <?php } ?>
                      </select>
                   </div>
              </div>

              <div style="clear:both"></div>

              <div class="form-group col-md-6">
                <label for="username" class="control-label">Username: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="username" class="form-control" id="username" placeholder="กรุณากรอก Username *เฉพาะภาษาอังกฤษ" value="<?php echo set_value('username'); ?>">
              </div>

              <div class="form-group col-md-6">
                <label class="control-label"></label>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" checked="checked" name="check_send_mail"> Send Username & Password to Email.
                  </label>
                </div>
              </div>

              <div style="clear:both"></div>

              <div class="form-group col-md-6">
                <label for="password" class="control-label">Password: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Password 8 - 12 ตัวอักษร" value="<?php echo set_value('password'); ?>">
              </div>

              <div class="form-group col-md-2">
                <label class="control-label">&nbsp;</label>
                <a class="btn btn-primary form-control" onclick="generatePassword();"><b>สุ่มรหัสผ่าน</b></a>
              </div>

              <div class="form-group col-md-4">
                <label class="control-label">&nbsp;</label>
                <input type="text" name="random_code" class="form-control" id="random_code" placeholder="" value="<?php echo set_value('random_code'); ?>">
              </div>

              <div style="clear:both"></div>

              <div class="form-group col-md-6">
                <label for="confirm_password" class="control-label">Confirm Password: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="" value="<?php echo set_value('confirm_password'); ?>">
              </div>

              <div style="clear:both"></div>

              <div class="form-group">

                <label for="image_thumb" class="control-label">อัพโหลดรูปประจำตัว: </label>
                  <input type="file" name="image_thumb[]" id="image_thumb" accept="image/*"/>
                  <p class="help-block">ขนาดรูป 640x360 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif จำนวน 1 รูป</p>
              </div>

          </form>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-4">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-body">

          <div class="form-group">
              <label for="post_date" class="control-label">Join Date: </label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="glyphicon glyphicon-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",date("d-m-Y")); ?>"  readonly="readonly">
              </div>

          </div>

          <div class="form-group">
              <label for="user_status_select" class="control-label">User Status: </label>

              <select name="user_status_select" id="user_status_select" class="form-control">
                <option value="active" <?php if(set_value("user_status")=="active"){ ?>selected="selected" <?php } ?>>อนุมัติใช้งาน</option>
                <option value="pending" <?php if(set_value("user_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่อนุมัติใช้งาน</option>
                <option value="deleted">ลบข้อมูล</option>
              </select>

          </div>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
          <a href="<?php echo admin_url($_menu_link."/userlist"); ?>" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>

</div>

<script src="plugins/validate/jquery.validate.min.js"></script>

<script type="text/javascript">

  $(function () {

    $("#user_listform").validate({
        rules: {
          content_subject: "required",
          user_email: {
            required: true,
            email: true,
            remote:
            {
                url   : admin_url+"/syssetting/check_email_user",
                type  : "post"
            }
          },
          user_group : "required",
          username: {
            required: true,
            minlength: 5,
            remote:
            {
                url   : admin_url+"/syssetting/check_username_user",
                type  : "post"
            }
          },
          password: {
            required: true,
            minlength: 5
          },
          confirm_password: {
            required: true,
            minlength: 5,
            equalTo: "#password"
          }
        },

        messages: {
          content_subject: "กรุณากรอกชื่อ-นามสกุล",
          user_email: {
              required: "กรุณากรอกอีเมล",
              email: "รูปแบบอีกเมลไม่ถูกต้อง ลองใหม่อีกครั้ง",
              remote: "อีเมลนี้มีอยู่ในระบบ ไม่สามารถใช้ได้"
          },
          user_group: "กรุณาเลือก User group.",
          username: {
            required: "กรุณากรอก Username",
            minlength: "กรุณากรอก Username อย่างน้อย 5 ตัวอักษร",
            remote: "Username นี้มีอยู่ในระบบ ไม่สามารถใช้ได้"
          },
          password: {
            required: "กรุณากรอก Password",
            minlength: "กรุณากรอก Password อย่างน้อย 5 ตัวอักษร"
          },
          confirm_password: {
            required: "กรุณากรอก Confirm Password",
            minlength: "กรุณากรอก Confirm Password อย่างน้อย 5 ตัวอักษร",
            equalTo: "กรุณากรอก Confirm Password ให้ตรงกับ Password"
          }
        }
      });

  });

  function generatePassword() {

      var length = 8,
          charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
          retVal = "";
      for (var i = 0, n = charset.length; i < length; ++i) {
          retVal += charset.charAt(Math.floor(Math.random() * n));
      }

      $("#random_code").val(retVal);
      $("#password").val(retVal);
      $("#confirm_password").val(retVal);
  }

  function save_form()
  {
    $("#user_status").val($("#user_status_select").val());
    $("#main_date").val($("#post_date").val());
    $("form#user_listform").submit();
  }
</script>