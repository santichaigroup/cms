<div class="row">
  <div class="col-md-7">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
          <?php }?>
          <!--  Error Alert  -->

          <form name="user_listform" id="user_listform" method="post" enctype="multipart/form-data" role="form">
              <div class="form-group">
                <label for="group_name" class="control-label">Website Title TH: &nbsp;</label>
                <input type="text" class="form-control" id="web_title" name="web_title" value="<?php echo set_value("web_title",$this->settings_model->item('web_title')); ?>">
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Website Keyword TH: &nbsp;</label>
                <input type="text" class="form-control" id="web_keyword" name="web_keyword" value="<?php echo set_value("web_keyword",$this->settings_model->item('web_keyword')); ?>">
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Description TH: &nbsp;</label>
                <textarea class="form-control" name="web_description" id="web_description" rows="3"><?php echo set_value("web_description",$this->settings_model->item('web_description')); ?></textarea>
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Website Title EN: &nbsp;</label>
                <input type="text" class="form-control" id="web_title_en" name="web_title_en" value="<?php echo set_value("web_title_en",$this->settings_model->item('web_title_en')); ?>">
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Website Keyword EN: &nbsp;</label>
                <input type="text" class="form-control" id="web_keyword_en" name="web_keyword_en" value="<?php echo set_value("web_keyword_en",$this->settings_model->item('web_keyword_en')); ?>">
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Description EN: &nbsp;</label>
                <textarea class="form-control" name="web_description_en" id="web_description_en" rows="3"><?php echo set_value("web_description_en",$this->settings_model->item('web_description_en')); ?></textarea>
              </div>

              <div class="form-group">
                <label for="group_name" class="control-label">Additional &lt;head&gt; code: &nbsp;</label>
                <textarea class="form-control" name="web_head_add_code" id="web_head_add_code" rows="10"><?php echo set_value("web_head_add_code",$this->settings_model->item('web_head_add_code')); ?></textarea>
              </div>
          </form>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-offset-1 col-md-4">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/userlist" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>

</div>

<script type="text/javascript">
  function save_form()
  {
    $("#user_status").val($("#user_status_select").val());
    $("#main_date").val($("#post_date").val());
    $("form#user_listform").submit();
  }
</script>