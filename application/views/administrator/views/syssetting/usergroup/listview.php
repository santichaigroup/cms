<style type="text/css">
  


</style>

<div class="box">
  <div class="box-header">

      <!--  Error Alert  -->
      <h4></h4>
      <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>

      <div class="btn-group">
         
         <button type="button" class="btn btn-default" data-toggle="dropdown"><i class="icon-user"></i> เครื่องมือ (<span class="select_count">0</span>) <span class="icon-caret-down"></span></button>
         <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>

         <ul class="dropdown-menu">
            <li><a href="javascript:;" onclick="selectgroup_delete();">ลบข้อมูล</a></li>
            <li><a href="javascript:;" onclick="selectgroup_unsuspend();">แสดงข้อมูล</a></li>
            <li><a href="javascript:;" onclick="selectgroup_suspend();">ไม่แสดงข้อมูล</a></li>
         </ul>
      </div>

      <a href="<?php echo admin_url($_menu_link."/usergroupadd"); ?>" class="btn btn-success" style="width: 150px;"><i class="icon-plus-sign"></i> เพิ่มผู้ใช้</a> 

  </div>
  <div class="box-body">

    <form method="post" name="usergroup_listform" id="usergroup_listform" enctype="multipart/form-data">
      <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>">
      <table id="datatable_list" class="table table-bordered table-striped" cellspacing="0">

        <thead>
          <tr class="info">
            <th align="center" width="5"><input type="checkbox" class="select_all_item" style="margin-left:20px;" /></th>
            <th><i class="glyphicon glyphicon-list"></i> No.</th>
            <th><i class="glyphicon glyphicon-list-alt"></i> Title name</th>
            <th><i class="glyphicon glyphicon-list-alt"></i> Title name</th>
            <th><i class="glyphicon glyphicon-check"></i> Status</th>
            <!-- <th class="dt-body-center"><i class="glyphicon glyphicon-wrench"></i> Actions</th> -->
          </tr>
        </thead>

        <tbody></tbody>

      </table>
    </form>

  </div>
</div>

<script>
  $(function () {

    var table = $('#datatable_list').DataTable({

          "bProcessing": true,
          "sAjaxSource"      : "<?php echo admin_url($_menu_link."/load_datatable_group"); ?>",
          "sAjaxDataProp"    : "aData",
          "language"  : {
            "sProcessing":   "กำลังดำเนินการ...",
            "sLengthMenu":   "แสดง _MENU_ แถว",
            "sZeroRecords":  "ไม่พบข้อมูล",
            "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix":  "",
            "sSearch":       "ค้นหา: ",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "เิริ่มต้น",
                "sPrevious": "ก่อนหน้า ",
                "sNext":     " ถัดไป",
                "sLast":     "สุดท้าย"
            }
          },
          //  Set title Table.
          "aoColumns":[
                    {"mDataProp": "group_id" },
                    {"mDataProp": "group_name",  "sTitle": "Group Name"},
                    {"mDataProp": "group_superadmin", "sTitle": "Superadmin Group"},
                    {"mDataProp": "group_status",   "sTitle": "สถานะ"},
                    {"mDataProp": "group_id", "sTitle":"เครื่องมือ" }
          ],
          'columnDefs': [{
               'targets': 0,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<input type="checkbox" name="group_id[]" value="' 
                      + $('<div/>').text(data).html() + '">';
               }
            },
            {
                'targets': 3,
                'className':'dt-body-center',
                'render': function (data, type, full, meta) {

                  switch(data) {
                      case "active" :
                      color = "green";
                      data = "แสดงข้อมูล";
                      break;
                      case "pending" :
                      color = "orange";
                      data = "ไม่แสดงข้อมูล";
                      break;
                      case "deleted" :
                      color = "red";
                      data = "ลบข้อมูล";
                      break;
                  }

                  return "<font color='"+color+"'>"+data+"</font>";
               }
            },
            {
               'targets': 4,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<a href="<?php echo admin_url($_menu_link."/usergroupedit/"); ?>' 
                      + $('<div/>').text(data).html() + '" class="btn btn-warning">แก้ไข</a>'
                      + ' <a onclick="delete_group('+ $('<div/>').text(data).html() +');" class="btn btn-danger">ลบ</a>';
            }
          }],
          'order': [1, 'asc']
       });

       $('.select_all_item').on('click', function() {

          var rows = table.rows({ 'search': 'applied' }).nodes();
          $('input[type="checkbox"]', rows).prop('checked', this.checked);

          var select_length = $('#datatable_list tbody input[type="checkbox"]:checked').length;
          $(".select_count").text(select_length);          

       });

       $('#datatable_list tbody').on('change', 'input[type="checkbox"]', function() {

          var select_length = $('#datatable_list tbody input[type="checkbox"]:checked').length;
          $(".select_count").text(select_length);

          if(!this.checked){

             var el = $('.select_all_item').get(0);
             if(el && el.checked && ('indeterminate' in el)) {

                el.indeterminate = true;
             }
          }
       });

  });

function delete_group(group_id)
{
  if(confirm("Delete User Group !. Are you sure ?")){
    $("#usergroup_listform").attr("action",admin_url+"syssetting/usergroupdelete_user/"+group_id);
    $("#usergroup_listform").submit();
  }
}
function selectgroup_delete()
{
  if(confirm("Delete User Group !. Are you sure ?")){
    $("#usergroup_listform").attr("action",admin_url+"syssetting/usergrouphandle_delete");
    $("#usergroup_listform").submit();
  }
}
function selectgroup_suspend()
{
  $("#usergroup_listform").attr("action",admin_url+"syssetting/usergrouphandle_suspend");
  $("#usergroup_listform").submit();
}
function selectgroup_unsuspend()
{
  $("#usergroup_listform").attr("action",admin_url+"syssetting/usergrouphandle_unsuspend");
  $("#usergroup_listform").submit();
}
</script>