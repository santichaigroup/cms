<div class="row">
  <div class="col-md-9">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">
            
          <div class="form-group">
				<table class="table table-striped table-bordered table-advance table-hover">
		            <tbody>

						<tr>
							<td class="highlight" width="150">
								<div class="success"></div>
								<span class="text-list">วันที่ส่ง :</span>
							</td>
							<td><?php echo $row['create_date'];?></td>
						</tr>

						<tr>
							<td class="highlight" width="150">
								<div class="success"></div>
								<span class="text-list">หัวข้อ :</span>
							</td>
							<td><?php echo $row['contactus_title'];?></td>
						</tr>

						<tr>
							<td class="highlight">
								<div class="success"></div>
								<span class="text-list">ชื่อ-นามสกุล :</span>
							</td>
							<td><?php echo $row['contactus_fullname'];?></td>
						</tr>
<!-- 						<tr>
							<td class="highlight" width="150">
								<div class="success"></div>
								<span class="text-list">ที่อยู่ :</span>
							</td>
							<td><?php echo $row['contactus_address'];?></td>
						</tr> -->
		                <tr>
							<td class="highlight" width="150">
								<div class="success"></div>
								<span class="text-list">อีเมล์ :</span>
							</td>
							<td><?php echo $row['contactus_email'];?></td>
						</tr>
		                <!-- <tr>
							<td class="highlight" width="150">
								<div class="success"></div>
								<span class="text-list">เบอร์โทรศัพท์ :</span>
							</td>
							<td><?php echo $row['contactus_phone'];?></td>
						</tr> -->

		                <tr>
							<td class="highlight" width="150">
								<div class="success"></div>
								<span class="text-list">รายละเอียด :</span>
							</td>
							<td><?php echo $row['contactus_detail'];?></td>
						</tr>

					</tbody>
				</table>
          </div>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-3">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>

      <div class="box-footer">
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/contactus" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>

</div>

<script type="text/javascript">
  function save_form()
  {
    $("#main_status").val($("#main_status_select").val());
    $("#main_date").val($("#post_date").val());
    $("#content_title").val($("#content_title_select").val());
    $("#content_description").val($("#content_description_select").val());
    $("#content_keyword").val($("#content_keyword_select").val());
    $("form#optionform").submit();
  }
</script>