class <?php echo ucfirst($class_name); ?>_model extends CI_Model {

	public function dataTable($lang_id)
	{
		$this->db->select('*'); 
		$this->db->from('<?php echo strtolower($class_name); ?>_id', '<?php echo strtolower($class_name); ?>_content');
		
		$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id", "left");

		$this->db->where("<?php echo strtolower($class_name); ?>_content.lang_id", $lang_id);
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>", "deleted");
		$this->db->where("<?php echo strtolower($class_name); ?>_content.content_status <>", "deleted");
		$this->db->order_by("<?php echo strtolower($class_name); ?>_id.main_id", "ASC");
		$this->db->limit(1000);
		return $this->db->get();
	}

	public function getAllContent()
	{
		$this->db->select('main_id');
		$this->db->where('main_status <>','deleted');
		$this->db->order_by('main_id','DESC');
		return $this->db->get('<?php echo strtolower($class_name); ?>_id');
	}

	public function getDetail($<?php echo strtolower($class_name); ?>_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('<?php echo strtolower($class_name); ?>_id', '<?php echo strtolower($class_name); ?>_content');
		
		if($lang_id){
			$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id AND <?php echo strtolower($class_name); ?>_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id AND <?php echo strtolower($class_name); ?>_content.content_id = <?php echo strtolower($class_name); ?>_id.default_main_id");
		}
		
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_id",$<?php echo strtolower($class_name); ?>_id);
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>",'deleted');
		$this->db->order_by("<?php echo strtolower($class_name); ?>_id.main_id","desc");
		$this->db->limit(1000);
		return  $this->db->get()->row_array();
	}

	public function addData()
	{
		$this->db->select("count(*) AS countSequence");
		$this->db->where("main_status <>","deleted");
		$countRow = $this->db->get("<?php echo strtolower($class_name); ?>_id")->row_array();

		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->set("sequence", ($countRow['countSequence']+1) );
		$this->db->insert("<?php echo strtolower($class_name); ?>_id");

		$<?php echo strtolower($class_name); ?>_id = $this->db->insert_id();
		if(!$<?php echo strtolower($class_name); ?>_id) {
			show_error("Cannot create <?php echo strtolower($class_name); ?> id");	
		}

		return $<?php echo strtolower($class_name); ?>_id;
	}

	public function setDate($<?php echo strtolower($class_name); ?>_id)
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$<?php echo strtolower($class_name); ?>_id);

		return $this->db->update("<?php echo strtolower($class_name); ?>_id");	
	}

	public function setStatus($main_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("content_id",$main_id);
		$this->db->where("lang_id",$lang_id);

		return $this->db->update("<?php echo strtolower($class_name); ?>_content");	
	}

	public function setDefaultContent($<?php echo strtolower($class_name); ?>_id,$lang_id,$content_id)
	{
		$this->db->set("default_main_id",$content_id);
		$this->db->where("main_id",$<?php echo strtolower($class_name); ?>_id);

		return $this->db->update("<?php echo strtolower($class_name); ?>_id");
	}

	public function addLanguage($<?php echo strtolower($class_name); ?>_id,$lang_id)
	{
		$this->db->where("main_id",$<?php echo strtolower($class_name); ?>_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("<?php echo strtolower($class_name); ?>_content");
		
		if($has->num_rows() == 0) {
			$this->db->set("main_id",$<?php echo strtolower($class_name); ?>_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("<?php echo strtolower($class_name); ?>_content");
			return $this->db->insert_id();
		} else {
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
	}

	function updateContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('main_id',$data['main_id']);
		$this->db->where('lang_id',$data['lang_id']);

		$update_content = $this->db->update('<?php echo strtolower($class_name); ?>_content', $data);

		return $update_content;
	}

	function deleteContent(array $data = array())
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("main_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("main_id",$data['main_id']);

		return $this->db->update("<?php echo strtolower($class_name); ?>_id");
	}

	public function getDetail_img($main_id, $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg');

		$this->db->select('*');
		$this->db->from('<?php echo strtolower($class_name); ?>_id');

		$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id AND <?php echo strtolower($class_name); ?>_content.content_id = <?php echo strtolower($class_name); ?>_id.default_main_id");
		$this->db->join("<?php echo strtolower($class_name); ?>_attachment","<?php echo strtolower($class_name); ?>_attachment.default_main_id = <?php echo strtolower($class_name); ?>_id.main_id ","right");
			
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_id",$main_id);
		$this->db->where_in("<?php echo strtolower($class_name); ?>_attachment.attachment_type", $type_file);
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("<?php echo strtolower($class_name); ?>_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("<?php echo strtolower($class_name); ?>_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}

	public function getDetail_file($main_id,$lang_id=NULL)
	{
		$type_file = array('pdf','doc','docx','rar','zip');

		$this->db->select('*');
		$this->db->from('<?php echo strtolower($class_name); ?>_id');

		$this->db->join("<?php echo strtolower($class_name); ?>_content","<?php echo strtolower($class_name); ?>_content.main_id = <?php echo strtolower($class_name); ?>_id.main_id AND <?php echo strtolower($class_name); ?>_content.content_id = <?php echo strtolower($class_name); ?>_id.default_main_id");
		$this->db->join("<?php echo strtolower($class_name); ?>_attachment","<?php echo strtolower($class_name); ?>_attachment.default_main_id = <?php echo strtolower($class_name); ?>_id.main_id ","right");

		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_id",$main_id);
		$this->db->where_in("<?php echo strtolower($class_name); ?>_attachment.attachment_type", $type_file);
		$this->db->where("<?php echo strtolower($class_name); ?>_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("<?php echo strtolower($class_name); ?>_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("<?php echo strtolower($class_name); ?>_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}
	
	function insertContent($<?php echo strtolower($class_name); ?>_id,$attachment_name,$lang_id=NULL)
	{
		if($attachment_name) {
			$attachment_type = str_replace('.','',strstr($attachment_name,'.'));

			$this->db->set("attachment_name",$attachment_name);
			$this->db->set("attachment_type",$attachment_type);
		}
		$this->db->set("default_main_id",$<?php echo strtolower($class_name); ?>_id);
		$this->db->set("lang_id",$lang_id);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		
		return $this->db->insert("<?php echo strtolower($class_name); ?>_attachment");
	}
	function deleteImage($img_id)
	{
		$this->db->where("attachment_id",$img_id);
		return $this->db->delete("<?php echo strtolower($class_name); ?>_attachment");
	}
	
	function setDefaultImg($default_main_id,$attachment_name,$lang_id)
	{
		$this->db->set("content_thumbnail",$attachment_name);

		$this->db->where("content_id",$default_main_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("<?php echo strtolower($class_name); ?>_content");
	}

	function setSequenceAttachment($id ,$i)
	{
		$this->db->set("sequence",$i);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("attachment_id",$id);
		
		return $this->db->update("<?php echo strtolower($class_name); ?>_attachment");
	}

	function setSequence($main_id,$new_sequence)
	{	
		$this->db->set("sequence",$new_sequence);
		$this->db->where("main_id",$main_id);
		return $this->db->update("<?php echo strtolower($class_name); ?>_id");
	}

}