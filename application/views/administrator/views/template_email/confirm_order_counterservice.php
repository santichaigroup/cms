<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo $email_title_confirm_order_conterservice; ?></title>
  <style>
   body {
    padding: 0;
    margin: 0;
   }
  </style>
</head>
<body>
  <table border="0" style="width: 660px; margin: 0 auto; font-family:Tahoma; font-size:11px;">
    <tr>
      <td><h1 style="margin:0;"><img src="<?php echo base_url().$image_logo; ?>" height="90" width="660" alt="DD4U"></h1></td>
    </tr>
    <tr>
      <td style="font-size:12px; padding-top:55px;">
        <div style="margin-bottom: 30px;"><img src="<?php echo base_url(); ?>public/images/email/email_titleTextOrderConfirm.jpg" height="20" width="205" alt="ORDER CONFIRMATION"></div>
        <div style="margin-bottom: 30px;">
          <table border="0" style="width: 100%;">
            <tr>
              <td>
                <table border="0" style="width: 100%;">
                  <tr>
                    <td width="140"><strong>ผู้สั่งซื้อ  : </strong></td>
                    <td width="370">
                      <?php
                        echo ( $customer_name ? $customer_name : "xxxxxxxxxxxxxx" );
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td><strong>ที่อยู่จัดส่ง  :</strong></td>
                    <td>
                      <?php
                        echo ( $shipping_address ? $shipping_address : "xxxxxxxxxxxxxxx <br> xxxxxxxxxxxxxxxx" );
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td><strong>เบอร์โทรศัพท์ติดต่อ :</strong></td>
                    <td>
                      <?php
                        echo ( $customer_telephone ? $customer_telephone : "089-000-0000" );
                      ?>
                    </td>
                  </tr>
                </table>
              </td>
              <td>
                <table border="0" style="width: 100%;">
                  <tr>
                    <td width="92"><strong>วันที่สั่งซื้อ  : </strong></td>
                    <td>
                      <?php
                        echo ( $order_date ? $order_date : "01/01/58" );
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td><strong>เลขที่สั่งซื้อ  : </strong> <br> (เลขที่อ้างอิง) </td>
                    <td>
                      <?php
                        echo ( $order_ref_id ? $order_ref_id : "0000000001" );
                      ?>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>
        <div style="margin-bottom: 30px;">

          <table border="0" style="width: 100%; text-align:center;border-collapse: collapse;">
            <tr>
              <th style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold;">ลำดับ<br>Item</th>
              <th style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold;">รายการ<br>Description</th>
              <th style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold;">จำนวน<br>Quantity</th>
              <th style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold;">ราคาต่อหน่อย<br>Unit Price</th>
              <th style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold;">จำนวนเงิน(บาท)<br>Amount(Baht)</th>
            </tr>
            <?php
            $i=1;
            foreach($result_order_item AS $row) {

              $no = $i;
              $item_name        = $row['item_name'];
              $order_quantity   = $row['order_quantity'];
              $order_unit       = $row['order_price'] / $order_quantity;
              $order_price      = $row['order_price'];

            ?>
                <tr>
                  <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"><?php echo $no; ?></td>
                  <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none; text-align:left;"><b><?php echo $item_name; ?></b></td>
                  <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"><?php echo $order_quantity; ?></td>
                  <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"><?php echo $order_unit; ?></td>
                  <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"><?php echo $order_price; ?></td>
                </tr>
            <?php
              $i++;
            }
            ?>
            <tr>
              <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"></td>
              <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"></td>
              <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"></td>
              <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"></td>
              <td style="padding: 5px; border: 1px solid #bfbfbf; border-top:none;border-bottom:none"></td>
            </tr>

            <tr>
              <td colspan="4" style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold; text-align:right;">รวมเป็นเงิน(บาท) / Invoice Amount(Baht)</td>
              <td style="padding: 5px; border: 1px solid #bfbfbf;"><?php echo $order_subtotal; ?></td>
            </tr>
            <tr>
              <td colspan="2" rowspan="2" style="padding: 5px; border: 1px solid #bfbfbf;">จำนวนเงินรวมทั้งสิ้น / Received (Amount in words)</td>
              <td colspan="2" style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold;">ค่าจัดส่ง / Delivery Cost</td>
              <td style="padding: 5px; border: 1px solid #bfbfbf;"><?php echo $shipping_cost; ?></td>
            </tr>
            <tr>
              <td colspan="2" style="background-color: #e7e7e7; padding: 5px; border: 1px solid #bfbfbf; color: #434343; font-weight: bold;">จำนวนเงินรวมทั้งสิ้น / Total</td>
              <td style="padding: 5px; border: 1px solid #bfbfbf;"><?php echo $order_grand_total; ?></td>
            </tr>
          </table>       
        </div>
        <div style="margin-bottom: 10px;"><strong>สถานะชำระเงิน :</strong> รอชำระเงิน</div>
        <div style="margin-bottom: 10px;"><strong>ช่องทางชำระเงิน :</strong> </div>
        <div style="margin-bottom: 10px;"><strong>เครดิตการ์ด</strong> <img style="vertical-align:middle;" src="<?php echo base_url(); ?>public/images/email/email_orderconfirm_credit.jpg" height="17" width="77" alt=""></div>
        <div style="margin-bottom: 5px;"><strong>เคาน์เตอร์เซอร์วิส</strong> <img style="vertical-align:middle;" src="<?php echo base_url(); ?>public/images/email/email_orderconfirm_conterService.jpg" height="13" width="111" alt=""></div>
        <div style="margin-bottom: 30px; color:#343434; font-size:10px;">
สามารถชำระสินค้าโดยภาพ ดาวน์โหลดและพิมพ์แบบฟอร์มชำระเงิน ตามที่ปรากฎในหน้า รายการสั่งซื้อหรืออีเมล์ของคุณ <br>
แล้วนำไปชำระผ่านเคาท์เตอร์เซอร์วิสทุกสาขา ทั่วประเทศ โดยมีการเรียกเก็บ ค่าธรรมเนียม. <br>
การชำระ* จากคุณ อัตราค่าธรรมเนียม 15 บาท        
        </div>

      </td>
    </tr>
    <tr>
      <td style="border-top:1px solid #cccccc; padding-top:10px;">
        <table border="0" style="width: 100%;">
          <tr>
            <td><?php echo $address; ?></td>
            <td style="text-align: right;">© 2016 DD4U ALL RIGHTS RESERVED</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>