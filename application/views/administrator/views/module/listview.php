<div class="row">
  <div class="col-md-12">

    <div class="box">
      <div class="box-header">
          
          <!--  Error Alert  -->
          <h4></h4>
          <?php if(@$success_message!=NULL){ ?>
          <div class="alert alert-success"> 
            <button class="close" data-dismiss="alert">×</button>
            <strong>Success !</strong> <?php echo $success_message; ?>
          </div>
          <?php } ?>

          <div class="btn-group">
             
             <button type="button" class="btn btn-default" data-toggle="dropdown"><i class="icon-user"></i> การแสดงผล (<span class="select_count">0</span>) <span class="icon-caret-down"></span></button>
             <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>

             <ul class="dropdown-menu">
                <li><a href="javascript:;" onclick="selectdata_delete();">ลบข้อมูล</a></li>
                <li><a href="javascript:;" onclick="selectdata_unsuspend();">แสดงข้อมูล</a></li>
                <li><a href="javascript:;" onclick="selectdata_suspend();">ไม่แสดงข้อมูล</a></li>
             </ul>
          </div>

          <a href="<?php echo admin_url($_menu_link."/add_menu/TH"); ?>" class="btn btn-success" style="width: 150px;"><i class="icon-plus-sign"></i> เพิ่มเมนูหลัก</a> 

      </div>
      <div class="box-body">

        <form method="post" name="usergroup_listform" id="usergroup_listform" enctype="multipart/form-data">
          <table id="datatable_list" class="table table-bordered table-striped" cellspacing="0">

            <thead>
              <tr class="info">
                <th align="center" width="5"><input type="checkbox" class="select_all_item" style="margin-left:20px;" /></th>
                <th><i class="glyphicon glyphicon-list-alt"></i> Menu Label</th>
                <th><i class="glyphicon glyphicon-calendar"></i> Menu Link</th>
                <th><i class="glyphicon glyphicon-calendar"></i> Menu Title</th>
                <th><i class="glyphicon glyphicon-check"></i> Menu Sequent</th>
                <th class="dt-body-center"><i class="glyphicon glyphicon-wrench"></i> Actions</th>
              </tr>
            </thead>

            <tbody>
    <?php
          foreach($result AS $menu) {

            $menu_id      = $menu['id'];
            $menu_label   = $menu['label'];
            $menu_link    = $menu['link'];
            $menu_title   = $menu['title'];
            $menu_sequent   = $menu['sequent'];
    ?>
              <tr>
                <td class="dt-body-center"><input type="checkbox" name="item_id[]" value="<?php echo $menu_id; ?>"></td>
                <td class="td-body-middle"><?php echo $menu_label; ?></td>
                <td class="td-body-middle"><?php echo $menu_link; ?></td>
                <td class="td-body-middle"><?php echo $menu_title; ?></td>
                <td class="dt-body-center"><?php echo $menu_sequent; ?></td>
                <td class="dt-body-center">
                  <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/add_sub_menu/<?php echo $menu_id; ?>" class="btn btn-sm btn-success">เพิ่มเมนูย่อย</a>
                  <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/edit_menu/<?php echo $menu_id; ?>" class="btn btn-sm btn-warning">แก้ไข</a>
                  <a href="javascript:;" onclick="delete_menu(<?php echo $menu_id; ?>);" class="btn btn-sm btn-danger">ลบ</a>
                </td>
              </tr>
    <?php
              if(count($menu['submenu_entry'])!=0) {

                  foreach($menu['submenu_entry'] AS $submenu) {

                    $submenu_id      = $submenu['id'];
                    $submenu_label   = $submenu['label'];
                    $submenu_link    = $submenu['link'];
                    $submenu_title   = $submenu['title'];
                    $submenu_sequent   = $submenu['sequent'];
    ?>
                    <tr class="">
                      <td></td>
                      <td class="td-body-middle"><i class="glyphicon glyphicon-menu-right"></i> <?php echo $submenu_label; ?></td>
                      <td class="td-body-middle"><?php echo $submenu_link; ?></td>
                      <td class="td-body-middle"><?php echo $submenu_title; ?></td>
                      <td class="dt-body-center"><?php echo $submenu_sequent; ?></td>
                      <td class="dt-body-center">
                        <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/edit_sub_menu/<?php echo $menu_id; ?>/<?php echo $submenu_id; ?>" class="btn btn-sm btn-warning">แก้ไข</a>
                        <a href="javascript:;" onclick="delete_sub_menu(<?php echo $menu_id; ?>,<?php echo $submenu_id; ?>);" class="btn btn-sm btn-danger">ลบ</a>
                      </td>
                    </tr>
    <?php
                  }
              }
          }
    ?>
            </tbody>

          </table>
        </form>

      </div>
    </div>

  </div>
</div>
<script>

  $(function () {

    var table = $('#datatable_list').DataTable({

          "language"  : {
            "sProcessing":   "กำลังดำเนินการ...",
            "sLengthMenu":   "แสดง _MENU_ แถว",
            "sZeroRecords":  "ไม่พบข้อมูล",
            "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix":  "",
            "sSearch":       "ค้นหา: ",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "เิริ่มต้น",
                "sPrevious": "ก่อนหน้า ",
                "sNext":     " ถัดไป",
                "sLast":     "สุดท้าย"
            }
          },
          "bSort" : false

       });

       $('.select_all_item').on('click', function() {

          var rows = table.rows({ 'search': 'applied' }).nodes();
          $('input[type="checkbox"]', rows).prop('checked', this.checked);
       });

       $('#datatable_list tbody').on('change', 'input[type="checkbox"]', function() {

          if(!this.checked){
             var el = $('.select_all_item').get(0);
             if(el && el.checked && ('indeterminate' in el)) {

                el.indeterminate = true;
             }
          }
       });

  });

  function delete_data(news_media_id)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url($_menu_link."/delete/"); ?>"+news_media_id);
    $("#usergroup_listform").submit();
    }
  }

</script>