<style type="text/css">
  .dt-body-center {
    text-align: center;
  }
</style>

<div class="box">
  <div class="box-header">
      
      <!--  Error Alert  -->
      <h4></h4>
      <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>

      <div class="btn-group">
         
         <button type="button" class="btn btn-default" data-toggle="dropdown"><i class="icon-user"></i> การแสดงผล (<span class="select_count">0</span>) <span class="icon-caret-down"></span></button>
         <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>

         <ul class="dropdown-menu">
            <li><a href="javascript:;" onclick="selectdata_delete();">ลบข้อมูล</a></li>
            <li><a href="javascript:;" onclick="selectdata_unsuspend();">แสดงข้อมูล</a></li>
            <li><a href="javascript:;" onclick="selectdata_suspend();">ไม่แสดงข้อมูล</a></li>
         </ul>
      </div>

      <!-- <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/add/TH" class="btn btn-success" style="width: 150px;"><i class="icon-plus-sign"></i> เพิ่มข้อมูล</a>  -->

  </div>
  <div class="box-body">

    <form method="post" name="usergroup_listform" id="usergroup_listform" enctype="multipart/form-data">
      <table id="datatable_list" class="table table-bordered table-striped" cellspacing="0">

        <thead>
          <tr class="info">
            <th align="center" width="5"><input type="checkbox" class="select_all_item" style="margin-left:20px;" /></th>
            <th><i class="glyphicon glyphicon-list-alt"></i> Menu Label</th>
            <th><i class="glyphicon glyphicon-calendar"></i> Menu Link</th>
            <th><i class="glyphicon glyphicon-calendar"></i> Menu Title</th>
            <!-- <th width="90"><i class="glyphicon glyphicon-check"></i> Menu Sequent</th> -->
            <th><i class="glyphicon glyphicon-wrench"></i> Actions</th>
          </tr>
        </thead>

        <tbody></tbody>

      </table>
    </form>

  </div>
</div>

<script>
  $(function () {

    var table = $('#datatable_list').DataTable({

          "bProcessing": true,
          "sAjaxSource"      : "<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/load_layout",
          "sAjaxDataProp"    : "aData",
          "language"  : {
            "sProcessing":   "กำลังดำเนินการ...",
            "sLengthMenu":   "แสดง _MENU_ แถว",
            "sZeroRecords":  "ไม่พบข้อมูล",
            "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix":  "",
            "sSearch":       "ค้นหา: ",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "เิริ่มต้น",
                "sPrevious": "ก่อนหน้า",
                "sNext":     "ถัดไป",
                "sLast":     "สุดท้าย"
            }
          },
          "aoColumns":[
                    {"mDataProp": "layout_id" },
                    {"mDataProp": "layout_label", "sTitle": "Layout Label"},
                    {"mDataProp": "layout_type", "sTitle": "Layout Type"},
                    {"mDataProp": "layout_thumbnail", "sTitle": "Layout Thumbnail"},
                    // {"mDataProp": "layout_status", "sTitle": "Layout Status"},
                    {"mDataProp": "layout_id" }
          ],
          'columnDefs': [{
               'targets': 0,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<input type="checkbox" name="item_id[]" value="' 
                      + $('<div/>').text(data).html() + '">';
               }
            },
            {
                'targets': 3,
                'className':'dt-body-center',
                'render': function (data, type, full, meta) {

                  return "<img data-src='holder.js/100%x180' src='<?php echo $asset_url; ?>uploads/layout/"+data+"'>";
               }
            },
            // {
            //     'targets': 4,
            //     'className':'dt-body-center',
            //     'render': function (data, type, full, meta) {

            //       switch(data) {
            //           case "active" :
            //           color = "green";
            //           break;
            //           case "pending" :
            //           color = "orange";
            //           break;
            //           case "deleted" :
            //           color = "red";
            //           break;
            //       }

            //       return "<font color='"+color+"'>"+data+"</font>";
            //    }
            // },
            {
               'targets': 4,
               'searchable':false,
               'orderable':false,
               'className':'dt-body-center',
               'render': function (data, type, full, meta) {
                   return '<a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/edit/' 
                      + $('<div/>').text(data).html() + '" class="btn btn-warning">แก้ไข</a>'
                      + ' <a onclick="delete_data('+ $('<div/>').text(data).html() +');" class="btn btn-danger">ลบ</a>';
            }
          }],
          'order': [1, 'asc']
       });

       $('.select_all_item').on('click', function() {

          var rows = table.rows({ 'search': 'applied' }).nodes();
          $('input[type="checkbox"]', rows).prop('checked', this.checked);
       });

       $('#datatable_list tbody').on('change', 'input[type="checkbox"]', function() {

          if(!this.checked){
             var el = $('.select_all_item').get(0);
             if(el && el.checked && ('indeterminate' in el)) {

                el.indeterminate = true;
             }
          }
       });

  });

  function delete_data(news_media_id)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#usergroup_listform").attr("action","<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>'/delete/"+news_media_id);
    $("#usergroup_listform").submit();
    }
  }

</script>