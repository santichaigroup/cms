<div class="row">
  <div class="col-md-9">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
          <?php }?>
          <!--  Error Alert  -->

          <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
              <input type="hidden" name="content_id" id="content_id" value="<?php echo $row['content_id']; ?>" />
            
              <div class="form-group">
                <label for="content_detail" class="control-label">Name: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_detail" class="form-control" id="content_detail" placeholder="" value="<?php echo set_value("content_detail", $row['content_detail']); ?>">
              </div>

              <div class="form-group">
                <label for="content_email" class="control-label">E-mail: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_email" class="form-control" id="content_email" placeholder="" value="<?php echo set_value("content_email", $row['content_email']); ?>">
              </div>

              <!--
              <div class="form-group">
                <label for="content_cc" class="control-label">E-mail Admin CC : (email_1@xxx.ocm , email_2@xxx.com)&nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="content_cc" class="form-control" id="content_cc" placeholder="" value="<?php echo set_value("content_cc", $row['content_cc']); ?>">
              </div>
              -->

          </form>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-3">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-body">

          <div class="form-group">
              <label for="content_status_select" class="control-label">การแสดงผล: </label>

              <select name="content_status_select" id="content_status_select" class="form-control">
                <option value="active" <?php if(set_value("content_status", $row['content_status'])=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                <option value="pending" <?php if(set_value("content_status", $row['content_status'])=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
              </select>

          </div>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
      </div>
    </div>

  </div>

<?php
/************************************************** SEO Box **************************************************/
?>

  <div class="col-md-12">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">ตัวอย่างการแสดงผล</h3>

      </div>
      <div class="box-body">

        <!-- ####################################################################################################### -->

        <?php 

          $_data = array();

          $this->_data['customer_name']     = "xxxxxxxxxxxxx";
          $this->_data['customer_email']    = "xxxxxx@xxxx.com";
          $this->_data['shipping_address']  = "xxxxxxxxxxxx xxxxxxxxx <br> xxxxxxxxxxxx";
          $this->_data['customer_telephone']    = "08xxxxxxxx";
          $this->_data['order_date']        = "2016-01-01";
          $this->_data['order_ref_id']      = "0000000001";
          $this->_data['order_subtotal']    = "100";
          $this->_data['shipping_cost']     = "0";
          $this->_data['order_grand_total'] = "100";

          // ช่องทางชำระ
          $this->_data['payment_method']    = "Counter Service";
          // Product detail
          $load_order[0]                       = array(
                                                "item_name"       =>  "xxxxxxxx",
                                                "order_quantity"  =>  "1",
                                                "order_price"     =>  "100"
                                              );

          $this->_data['result_order_item'] = $load_order;

          $this->_data['email_title_payment_counterservice']              = $text_lang['email_title_payment_counterservice'];
          $this->_data['image_logo']                                      = $text_lang['email_image_logo'];
          $this->_data['address']                                         = $text_lang['email_address'];

          echo $this->admin_library->view("template_email/payment_counterservice", $this->_data, TRUE);

        ?>
          
        <!-- ####################################################################################################### -->

    </div>

  </div>

</div>

<script type="text/javascript">
  function save_form()
  {
    $("#main_status").val($("#main_status_select").val());
    $("form#optionform").submit();  
  }
</script>