<div class="row">

  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
  <div class="col-md-9">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
          <?php }?>
          <!--  Error Alert  -->

              <input type="hidden" name="id" id="id" value="<?php echo set_value("id"); ?>">
            
              <div class="form-group">
                <label for="class_name" class="control-label">หัวข้อ: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="class_name" class="form-control" id="class_name" placeholder="" value="<?php echo set_value("class_name"); ?>">
              </div>

              <div class="form-group">
                <label for="class_detail" class="control-label">รายละเอียด: </label>
                  <textarea class="ckeditor" name="class_detail" id="class_detail" rows="10" cols="80">
                      <?php echo set_value("class_detail"); ?>
                  </textarea>
              </div>

              <div class="form-group">
                <label for="member_class_adjust_value" class="control-label">ยอดสั่งซื้อรวมขั้นต่ำ: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="member_class_adjust_value" class="form-control" id="member_class_adjust_value" placeholder="" value="<?php echo set_value("member_class_adjust_value"); ?>">
              </div>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-3">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-body">

          <div class="form-group">
              <label for="class_status" class="control-label">การแสดงผล: </label>

              <select name="class_status" id="class_status" class="form-control">
                <option value="active" <?php if(set_value("class_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                <option value="pending" <?php if(set_value("class_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
              </select>

          </div>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/list_class/<?php echo $lang_id; ?>" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>
  </form>

</div>

<script type="text/javascript">
  function save_form()
  {
    $("form#optionform").submit();
  }
</script>