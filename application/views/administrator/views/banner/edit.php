<div class="row">
  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo set_value("lang_id", $row['lang_id']); ?>">

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">

            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Form Box</h3>

        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
                </div>
            <?php }?>
            <!--  Error Alert  -->
              
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="content_subject" class="control-label">หัวข้อ: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value("content_subject", $row['content_subject']); ?>">
                </div>

                <!-- <div class="form-group">
                  <label for="content_subject" class="control-label">รูปประจำหัวข้อ: </label>
                </div>

                <div class="row">
                  <div class="col-md-3">
                    <?php
                    if($row['content_thumbnail']) {
                    ?>
                    <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row['content_thumbnail']); ?>">
                      <img src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row['content_thumbnail']); ?>" data-holder-rendered="true">
                    </a>
                    <?php
                    } else {
                    ?>
                    <a href="javascript:;" class="thumbnail">
                      <img src="<?php echo base_url("public/images/thumbnail-default.jpg"); ?>" data-holder-rendered="true">
                    </a>
                    <?php
                    }
                    ?>
                  </div>
                </div> -->

                <div class="form-group">
                  <div class="radio">
                    <label>
                      <input type="radio" name="type" value="1" <?php echo ($row['type']==1?"checked":"disabled"); ?>>
                      สไลต์รูปภาพ
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="type" value="2" <?php echo ($row['type']==2?"checked":"disabled"); ?>>
                      สไลต์วิดีโอ
                    </label>
                  </div>
                </div>

                <div class="form-group images">
                  <label for="content_url_th" class="control-label">Url เว็บไซต์ ภาษาไทย: </label>
                  <input type="text" name="content_url_th" class="form-control" id="content_url_th" placeholder="" value="<?php echo set_value('content_url_th', $row['content_url_th']); ?>">
                </div>

                <div class="form-group images">
                  <label for="content_url_en" class="control-label">Url เว็บไซต์ English: </label>
                  <input type="text" name="content_url_en" class="form-control" id="content_url_en" placeholder="" value="<?php echo set_value('content_url_en', $row['content_url_en']); ?>">
                </div>

                <div class="form-group images">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ PC: </label>
                  <?php
                  if(!$rs_img_pc->result_array()) {
                  ?>
                    <input type="file" name="image_thumb[]" id="image_thumb" accept="image/*" multiple/>
                    <p class="help-block">ขนาดรูป 1920x790 พิกเซล(Pixel) ไม่เกิน 10 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                  <?php
                  }
                  ?>
                </div>

                <div style="clear:both"></div>
                  <div class="row images" id="sortable">
                      <?php foreach($rs_img_pc->result_array() AS $row_img) { ?>
                      <?php if(!empty($row_img['attachment_name'])) { ?>
                      <div class="col-md-4 ui-state-default">
                          <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row_img['attachment_name']); ?>">
                            <img class="img-responsive" data-src="holder.js/100%x180" data-holder-rendered="true" src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row_img['attachment_name']); ?>" alt="" />
                          </a>
                          <div class="row" style="text-align: center;margin-bottom: 20px;">
                            <div class="col-md-12">
                              <!-- <a href="javascript:;" onclick="set_default_img('<?php echo $row_img['default_main_id']; ?>','<?php echo $row['content_id']; ?>','<?php echo $row_img['attachment_name'];?>','<?php echo $row['lang_id']; ?>');" class="btn btn-success">
                              ตั้งเป็นรูปประจำหัวข้อ
                              </a> -->
                              <div class="form-group text-left">
                                <label for="attachment_alt" class="control-label text-left">Alternate Text: </label>
                                <input type="text" name="attachment_alt[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_alt[".$row_img['attachment_id']."]", $row_img['attachment_alt']); ?>">
                              </div>
                              <div class="form-group text-left">
                                <label for="attachment_title" class="control-label text-left">Title: </label>
                                <input type="text" name="attachment_title[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_title[".$row_img['attachment_id']."]", $row_img['attachment_title']); ?>">
                              </div>

                              <a href="javascript:;" onclick="delete_img('<?php echo $row_img['attachment_id']; ?>','<?php echo $row_img['default_main_id']; ?>','<?php echo $row_img['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                              </a>
                            </div>
                          </div>
                          <input type="hidden" name="attachment_id[]" value="<?php echo $row_img['attachment_id']; ?>"> 
                      </div>
                      <?php } } ?>
                  </div>
                <div style="clear:both"></div>

                <div class="form-group images">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ Mobile: </label>
                  <?php
                  if(!$rs_img_mb->result_array()) {
                  ?>
                    <input type="file" name="image_thumb_mobile[]" id="image_thumb_mobile" accept="image/*" multiple/>
                    <p class="help-block">ขนาดรูป 1080x1680 พิกเซล(Pixel) ไม่เกิน 10 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                  <?php
                  }
                  ?>
                </div>

                <div style="clear:both"></div>
                  <div class="row images" id="sortable">
                      <?php foreach($rs_img_mb->result_array() AS $row_img) { ?>
                      <?php if(!empty($row_img['attachment_name'])) { ?>
                      <div class="col-md-4 ui-state-default">
                          <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row_img['attachment_name']); ?>">
                            <img class="img-responsive" data-src="holder.js/100%x180" data-holder-rendered="true" src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row_img['attachment_name']); ?>" alt="" />
                          </a>
                          <?php /*
                          <p style="text-align: center;">
                              <!-- <a href="javascript:;" onclick="set_default_img('<?php echo $row_img['default_main_id']; ?>','<?php echo $row['content_id']; ?>','<?php echo $row_img['attachment_name'];?>','<?php echo $row['lang_id']; ?>');" class="btn btn-success">
                              ตั้งเป็นรูปประจำหัวข้อ
                              </a> -->
                              <a href="javascript:;" onclick="delete_img('<?php echo $row_img['attachment_id']; ?>','<?php echo $row_img['default_main_id']; ?>','<?php echo $row_img['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                              </a>
                          </p>
                          */ ?>
                          <div class="row" style="text-align: center;margin-bottom: 20px;">
                            <div class="col-md-12">
                              <!-- <a href="javascript:;" onclick="set_default_img('<?php echo $row_img['default_main_id']; ?>','<?php echo $row['content_id']; ?>','<?php echo $row_img['attachment_name'];?>','<?php echo $row['lang_id']; ?>');" class="btn btn-success">
                              ตั้งเป็นรูปประจำหัวข้อ
                              </a> -->
                              <div class="form-group text-left">
                                <label for="attachment_alt_mb" class="control-label text-left">Alternate Text: </label>
                                <input type="text" name="attachment_alt_mb[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_alt_mb[".$row_img['attachment_id']."]", $row_img['attachment_alt']); ?>">
                              </div>
                              <div class="form-group text-left">
                                <label for="attachment_title_mb" class="control-label text-left">Title: </label>
                                <input type="text" name="attachment_title_mb[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_title_mb[".$row_img['attachment_id']."]", $row_img['attachment_title']); ?>">
                              </div>

                              <a href="javascript:;" onclick="delete_img('<?php echo $row_img['attachment_id']; ?>','<?php echo $row_img['default_main_id']; ?>','<?php echo $row_img['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                              </a>
                            </div>
                          </div>
                          <input type="hidden" name="attachment_id_mb[]" value="<?php echo $row_img['attachment_id']; ?>"> 
                      </div>
                      <?php } } ?>
                  </div>
                <div style="clear:both"></div>

                <div class="form-group videoes">
                    <label for="content_detail" class="control-label">ระบุวิดีโอ: </label>
                  <input type="text" name="content_detail" class="form-control" id="content_detail" placeholder="" value="<?php echo set_value('content_detail', $row['content_detail']); ?>">
                  <p class="help-block">รองรับเฉพาะลิงก์จาก Youtube ตัวอย่าง https://www.youtube.com/watch?v=4DSmnam1YHk</p>
                </div>

                <?php if($row['content_detail']) { echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"100%\" height=\"480\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$row['content_detail']); } ?>

                <!-- <div class="form-group">
                  <label for="content_subject" class="control-label">อัพโหลดไฟล์: </label>
                    <input type="file" name="file_thumb[]" id="file_thumb" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf"/>
                    <p class="help-block">ไฟล์ขนาดไม่เกิน 20 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .pdf, doc, docx, rar, zip จำนวน 1 ไฟล์</p>
                </div> -->

                <!-- <div style="clear:both"></div>
                  <div class="row">
                      <?php foreach($rs_file->result_array() AS $row_file) { ?>
                      <?php if(!empty($row_file['attachment_name'])) { ?>
                      <?php
                        switch ($row_file['attachment_type']) {
                          case 'pdf':
                            $file_type = "pdf.png";
                            break;
                          case 'doc':
                            $file_type = "word.png";
                            break;
                          case 'docx':
                            $file_type = "word.png";
                            break;
                          case 'xls':
                            $file_type = "excel.png";
                            break;
                          case 'rar':
                            $file_type = "rar.png";
                            break;
                          case 'zip':
                            $file_type = "zip.png";
                            break;
                          
                        }
                      ?>

                          <div class="col-md-2">
                              <a class="thumbnail" target="_blank" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/files/".$row_file['attachment_name']); ?>">
                                <img class="img-responsive" src="<?php echo base_url("public/images/icons/".$file_type); ?>" alt="" />
                              </a>
                              <p style="text-align: center;">
                                  <a href="javascript:;" onclick="delete_img(<?php echo $row_file['attachment_id']; ?>,<?php echo $row_file['default_main_id']; ?>,'<?php echo $row_file['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                              </p>
                          </div>

                      <?php } } ?>
                  </div>
                <div style="clear:both"></div> -->
              </div>

              <div class="col-md-3">

                <!-- <div class="form-group">
                    <label for="menu_status" class="control-label">เปลี่ยนภาษา: </label><br>
                    <div class="controls btn-group">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">
                            <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                              <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                             <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <?php foreach($this->admin_library->getLanguageList() as $lang){
                          if($row['lang_id'] <> $lang['lang_id']){
                          ?>
                            <li>
                              <a href="<?php echo admin_url($this->menu['menu_link']."/edit/".$row['main_id']."/".$lang['lang_id']); ?>">
                                <img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?>
                              </a>
                            </li>
                          <?php }} ?>
                        </ul>
                    </div>
                </div> -->

                <!-- <div class="form-group">
                    <label for="create_by" class="control-label">เขียนโดย: </label>
                    <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                </div> -->

                <div class="form-group">
                    <label for="post_date" class="control-label">เขียนเมื่อ: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",$row['post_date']); ?>"  readonly="readonly">
                    </div>
                </div>
                <?php
                if($row['update_by']) {
                ?>
                <div class="form-group">
                    <label for="upadte_by" class="control-label">แก้ไขโดย: </label>
                    <input type="text" class="form-control" name="upadte_by" id="upadte_by" value="<?php $update_name = $this->admin_library->getuserinfo($row['update_by']); echo $update_name['user_fullname']; ?>" disabled>
                </div>
                <?php
                }
                ?>
                <?php
                if($row['update_date']) {
                ?>
                <div class="form-group">
                    <label for="update_date" class="control-label">แก้ไขเมื่อ: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right active date-picker" name="update_date" id="update_date" value="<?php echo set_value("update_date",$row['update_date']); ?>" readonly="readonly">
                    </div>
                </div>
                <?php
                }
                ?>
                <div class="form-group">
                    <label for="content_status" class="control-label">การแสดงผล: </label>

                    <select name="content_status" id="content_status" class="form-control">
                      <option value="active" <?php if(set_value("content_status", $row['content_status'])=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                      <option value="pending" <?php if(set_value("content_status", $row['content_status'])=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                    </select>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
                    <a href="<?php echo admin_url($_menu_link."/index/".$lang_id); ?>" class="btn btn-danger">ย้อนกลับ</a>
                </div>
              </div>
            </div>
            
        </div>
      </div>
    </div>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <div class="col-md-12 images">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">

            <!-- <div class="form-group">
              <label for="content_seo" class="control-label">Url: </label>
              <input type="text" name="content_seo" class="form-control" id="content_seo" placeholder="" value="<?php echo set_value("content_seo", $row['content_seo']); ?>">
            </div> -->

            <div class="form-group">
              <label for="content_title" class="control-label">Tag A Title: </label>
              <input type="text" name="content_title" class="form-control" id="content_title" placeholder="ชื่อ Url ที่ต้องการบอกรายละเอียดแบบย่อ" value="<?php echo set_value("content_title", $row['content_title']); ?>">
            </div>

            <div class="form-group">
              <label for="content_description" class="control-label">Image Alt: </label>
              <input type="text" name="content_description" class="form-control" id="content_description" placeholder="รายละเอียดรูปภาพ" value="<?php echo set_value("content_description", $row['content_description']); ?>">
            </div>

            <!-- <div class="form-group">
              <label for="content_keyword" class="control-label">Meta Keywords: </label>
              <input type="text" name="content_keyword" class="form-control" id="content_keyword" placeholder="" value="<?php echo set_value("content_keyword", $row['content_keyword']); ?>">
              <p class="help-block">ใช้ , คั่นระหว่างคำ</p>
            </div> -->
              
        </div>
      </div>
    </div>

  </form>
</div>

<script type="text/javascript">
  $(function() {

    <?php
    if($row['type']==1) {
    ?>
    $('.images').show();
    $('.videoes').hide();
    <?php
    } else if($row['type']==2) {
    ?>
    $('.images').hide();
    $('.videoes').show();
    <?php
    } else {
    ?>
    $('.images').hide();
    $('.videoes').hide();
    <?php
    }
    ?>

    $('input:radio').change(function() {
      if($("input:radio:checked").val()==1) {

        $('.images').show();
        $('.videoes').hide();
      } else {

        $('.images').hide();
        $('.videoes').show();
      }
    });

  });

  function save_form()
  {
    $("form#optionform").submit();
  }

  function delete_img(attachment_id,default_main_id,attachment_name,lang_id,type_name)
  {
    if(confirm("Delete Data !. Are you sure ?")){
    $("#optionform").attr("action",admin_url+"<?php echo $this->menu['menu_link']; ?>/delete_img/"+attachment_id+"/"+default_main_id+"/"+attachment_name+"/"+lang_id+"/"+type_name);
    $("#optionform").submit();
    }
  }

  function set_default_img(default_main_id,content_id,attachment_name,lang_id)
  {
    if(confirm("Set image default. Are you sure ?")){
    $("#optionform").attr("action",admin_url+"<?php echo $this->menu['menu_link']; ?>/set_default_img/"+default_main_id+"/"+content_id+"/"+attachment_name+"/"+lang_id);
    $("#optionform").submit();
    }
  }
</script>