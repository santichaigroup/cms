<div class="row">
  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form">
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Form Box</h3>
        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
                </div>
            <?php }?>
            <!--  Error Alert  -->
            
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="content_subject" class="control-label">หัวข้อ: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value('content_subject'); ?>">
                </div>

                <div class="form-group">
                  <div class="radio">
                    <label>
                      <input type="radio" name="type" value="1">
                      สไลต์รูปภาพ
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="type" value="2">
                      สไลต์วิดีโอ
                    </label>
                  </div>
                </div>

                <div class="form-group images">
                  <label for="content_url_th" class="control-label">Url เว็บไซต์ ภาษาไทย: </label>
                  <input type="text" name="content_url_th" class="form-control" id="content_url_th" placeholder="" value="<?php echo set_value('content_url_th'); ?>">
                </div>

                <div class="form-group images">
                  <label for="content_url_en" class="control-label">Url เว็บไซต์ English: </label>
                  <input type="text" name="content_url_en" class="form-control" id="content_url_en" placeholder="" value="<?php echo set_value('content_url_en'); ?>">
                </div>

                <div class="form-group images">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ PC: </label>
                    <input type="file" name="image_thumb[]" id="image_thumb" accept="image/*" multiple/>
                    <p class="help-block">ขนาดรูป 1920x790 พิกเซล(Pixel) ไม่เกิน 10 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                </div>

                <div class="form-group images">
                  <label for="content_subject" class="control-label">อัพโหลดรูปภาพ Mobile: </label>
                    <input type="file" name="image_thumb_mobile[]" id="image_thumb_mobile" accept="image/*" multiple/>
                    <p class="help-block">ขนาดรูป 1080x1680 พิกเซล(Pixel) ไม่เกิน 10 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 1 รูป</p>
                </div>

                <div class="form-group videoes">
                    <label for="content_detail" class="control-label">ระบุวิดีโอ: </label>
                  <input type="text" name="content_detail" class="form-control" id="content_detail" placeholder="" value="<?php echo set_value('content_detail'); ?>">
                  <p class="help-block">รองรับเฉพาะลิงก์จาก Youtube *หากข้อมูลไม่ถูกต้อง วิดีโอจะไม่แสดงผล ตัวอย่าง https://www.youtube.com/watch?v=4DSmnam1YHk</p>
                </div>

                <!-- <div class="form-group">
                  <label for="content_subject" class="control-label">อัพโหลดไฟล์: </label>
                    <input type="file" name="file_thumb[]" id="file_thumb" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf"/>
                    <p class="help-block">ไฟล์ขนาดไม่เกิน 20 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .pdf, doc, docx, rar, zip จำนวน 1 ไฟล์</p>
                </div> -->
              </div>

              <div class="col-md-3">

                  <!-- <div class="form-group">
                    <label for="menu_status" class="control-label">เปลี่ยนภาษา: </label><br>
                    <div class="controls btn-group">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">
                        <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                          <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                         <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <?php foreach($this->admin_library->getLanguageList() as $lang){
                          if($lang_id <> $lang['lang_id']){
                        ?>
                          <li>
                            <a href="<?php echo admin_url($this->menu['menu_link'].$this->submenu['menu_link']."/add/".$lang['lang_id']); ?>"><img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?></a>
                          </li>
                        <?php }} ?>
                      </ul>
                    </div>
                  </div> -->

                  <!-- <div class="form-group">
                      <label for="create_by" class="control-label">เขียนโดย: </label>
                      <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                  </div> -->

                  <div class="form-group">
                    <label for="post_date" class="control-label">เขียนเมื่อ: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",date("d-m-Y")); ?>"  readonly="readonly">
                    </div>
                  </div>

                  <!-- <div class="form-group">
                      <label for="upadte_by" class="control-label">แก้ไขโดย: </label>
                      <input type="text" class="form-control" name="upadte_by" id="upadte_by" disabled>
                  </div>

                  <div class="form-group">
                      <label for="update_date" class="control-label">แก้ไขเมื่อ: </label>
                      <input type="text" class="form-control" name="update_date" id="update_date">
                  </div> -->

                  <div class="form-group">
                    <label for="content_status" class="control-label">การแสดงผล: </label>

                    <select name="content_status" id="content_status" class="form-control">
                      <option value="active" <?php if(set_value("content_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                      <option value="pending" <?php if(set_value("content_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                      <option value="deleted">ลบข้อมูล</option>
                    </select>
                  </div>

                  <div class="box-footer">
                      <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
                      <a href="<?php echo admin_url($_menu_link); ?>" class="btn btn-danger">ย้อนกลับ</a>
                  </div>
              </div>
            </div>

        </div>
      </div>
    </div>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <div class="col-md-12 images">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">

            <div class="form-group">
              <label for="content_title" class="control-label">Tag A Title: </label>
              <input type="text" name="content_title" class="form-control" id="content_title" placeholder="ชื่อ Url ที่ต้องการบอกรายละเอียดแบบย่อ" value="<?php echo set_value('content_title'); ?>">
            </div>

            <div class="form-group">
              <label for="content_description" class="control-label">Image Alt: </label>
              <input type="text" name="content_description" class="form-control" id="content_description" placeholder="รายละเอียดรูปภาพ" value="<?php echo set_value('content_description'); ?>">
            </div>

            <!-- <div class="form-group">
              <label for="content_keyword" class="control-label">Meta Keywords: </label>
              <input type="text" name="content_keyword" class="form-control" id="content_keyword" placeholder="คำค้นหา สำหรับ SEO" value="<?php echo set_value('content_keyword'); ?>">
              <p class="help-block">ใช้ , คั่นระหว่างคำ</p>
            </div> -->
              
        </div>
      </div>
    </div>

  </form>
</div>

<script type="text/javascript">

  $(function() {

    $('.images').hide();
    $('.videoes').hide();

    $('input:radio').change(function() {
      if($("input:radio:checked").val()==1) {

        $('.images').show();
        $('.videoes').hide();
      } else {

        $('.images').hide();
        $('.videoes').show();
      }
    });

  });

  function save_form()
  {
    $("form#optionform").submit();
  }
</script>