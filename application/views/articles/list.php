  <div class="lvlBlock factCover">
    <style scoped>
      @media (min-width:768px) {
        .factCover {
          background-image:url(img/fact-cover.png);
        }
      }
    </style>
    <div class="container">
      <div class="lvlBlockInner">
        <h1 class="titleText"><?php echo text_lang('FACT', $lang); ?></h1>
        <figure class="image visible-xs"><img src="img/fact-cover-mobile.png"></figure>
      </div>
    </div>
  </div>


  <div class="lvlBlock isFact">
    <div class="container">
      <div class="lvlBlockInner">
        <div class="thumbnailItems">
          <div class="row">
            <?php
            if(count($articles)) {

              for ($i=0; $i<count($articles); $i++) {

                $content_subject  = $articles[$i]['content_subject'];
                $content_desc     = ( $articles[$i]['content_short'] ? substr_utf8($articles[$i]['content_short'], 0, 170) : '' );
                $content_url      = ( $articles[$i]['content_seo'] ? site_url($articles[$i]['content_seo']) : 'javascript:;' );
                $content_alt      = ( $articles[$i]['content_description'] ? $articles[$i]['content_description'] : '' );

                  if($articles[$i]['image']) {

                    $highlight = ( $articles[$i]['image']['attachment_name'] ? '<img src="'.base_url('public/uploads/articles/images/'.$articles[$i]['image']['attachment_name']).'" alt="'.$articles[$i]['image']['attachment_alt'].'" title="'.$articles[$i]['image']['attachment_title'].'">' : '' );
                  } else {

                    $highlight = null;
                  }

                  $content_highlight = '<figure class="image"><a href="'.$content_url.'">'.$highlight.'</a></figure>';
              ?>

                <div class="col-sm-4">
                  <div class="thumbnailTheme noCaption isMD">
                    <div class="imageWrap">
                      <?php echo $content_highlight; ?>
                    </div>
                    <div class="captionWrap">
                      <div class="caption">
                        <h3 class="titleText">
                          <span><?php echo $content_subject; ?></span>
                          <?php /* if($thumbnailSubTitle):
                          <small>consectetur adipiscing</small>
                          <?php endif; */ ?>
                        </h3>
                        <?php if($content_desc) { ?>
                        <div class="desc">
                            <p><?php echo $content_desc; ?></p>
                        </div>
                        <?php } ?>
                      </div>
                      <div class="button"><a href="<?php echo $content_url; ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                    </div>
                  </div>
                </div>

            <?php } } ?>

                <div class="col-sm-4">
                  <div class="thumbnailTheme noCaption isMD">
                    <div class="imageWrap">
                      <figure class="image"><a href="<?php echo site_url($lang=="TH"?"ภาวะการขาดน้ำแบบแฝง​/TH":"voluntary-dehydration/EN"); ?>"><img src="https://pocarisweat.co.th/public/uploads/articles/images/4de90f6827a342b3bffb4b21fa42cd85.png" title="" alt="" class="detectOrientation isLandscape detectOrientationDone"></a></figure>                    </div>
                    <div class="captionWrap">
                      <div class="caption">
                        <h3 class="titleText">
                          <span><?php echo text_lang('fact_head_5', $lang); ?></span>
                                                  </h3>
                                              </div>
                      <div class="button"><a href="<?php echo site_url($lang=="TH"?"ภาวะการขาดน้ำแบบแฝง​/TH":"voluntary-dehydration/EN"); ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="thumbnailTheme noCaption isMD">
                    <div class="imageWrap">
                      <figure class="image"><a href="<?php echo site_url($lang=="TH"?"ความกระหายคืออะไร/TH":"thirst-mechanism/EN"); ?>"><img src="https://pocarisweat.co.th/public/uploads/articles/images/367370901c57fe39624b88f36155c839.png" title="" alt="" class="detectOrientation isLandscape detectOrientationDone"></a></figure>                    </div>
                    <div class="captionWrap">
                      <div class="caption">
                        <h3 class="titleText">
                          <span><?php echo text_lang('fact_head_4', $lang); ?></span>
                                                  </h3>
                                              </div>
                      <div class="button"><a href="<?php echo site_url($lang=="TH"?"ความกระหายคืออะไร/TH":"thirst-mechanism/EN"); ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="thumbnailTheme noCaption isMD">
                    <div class="imageWrap">
                      <figure class="image"><a href="<?php echo site_url($lang=="TH"?"อย่าให้ร่างกายขาดน้ำ/TH":"don’t-get-dehydration/EN"); ?>"><img src="https://pocarisweat.co.th/public/uploads/articles/images/b4676734605ea0bd24fd8091a86d0368.png" title="" alt="" class="detectOrientation isLandscape detectOrientationDone"></a></figure>                    </div>
                    <div class="captionWrap">
                      <div class="caption">
                        <h3 class="titleText">
                          <span><?php echo text_lang('fact_head_3', $lang); ?></span>
                                                  </h3>
                                              </div>
                      <div class="button"><a href="<?php echo site_url($lang=="TH"?"อย่าให้ร่างกายขาดน้ำ/TH":"don’t-get-dehydration/EN"); ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="thumbnailTheme noCaption isMD">
                    <div class="imageWrap">
                      <figure class="image"><a href="<?php echo site_url($lang=="TH"?"หน้าที่ของของเหลวในร่างกาย/TH":"roles-of-body-fluids/EN"); ?>"><img src="https://pocarisweat.co.th/public/uploads/articles/images/51c2f3634d761f2c67e25b1658cc262c.png" title="" alt="" class="detectOrientation isLandscape detectOrientationDone"></a></figure>                    </div>
                    <div class="captionWrap">
                      <div class="caption">
                        <h3 class="titleText">
                          <span><?php echo text_lang('fact_head_2', $lang); ?></span>
                                                  </h3>
                                              </div>
                      <div class="button"><a href="<?php echo site_url($lang=="TH"?"หน้าที่ของของเหลวในร่างกาย/TH":"roles-of-body-fluids/EN"); ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="thumbnailTheme noCaption isMD">
                    <div class="imageWrap">
                      <figure class="image"><a href="<?php echo site_url($lang=="TH"?"น้ำเป็นองค์ประกอบหลักในร่างกายของเรา/TH":"water-is-important/EN"); ?>"><img src="https://pocarisweat.co.th/public/uploads/articles/images/f4d80a0810264bb880d951681584e8de.png" title="" alt="" class="detectOrientation isLandscape detectOrientationDone"></a></figure>                    </div>
                    <div class="captionWrap">
                      <div class="caption">
                        <h3 class="titleText">
                          <span><?php echo text_lang('fact_head_1', $lang); ?></span>
                                                  </h3>
                                              </div>
                      <div class="button"><a href="<?php echo site_url($lang=="TH"?"น้ำเป็นองค์ประกอบหลักในร่างกายของเรา/TH":"water-is-important/EN"); ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                    </div>
                  </div>
                </div>

          </div>
        </div>  
      </div>
    </div>
  </div>