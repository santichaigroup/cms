  <div class="lvlBlock factCover">
    <style scoped>
      @media (min-width:768px) {
        .factCover {
          background-image:url(img/fact-cover.png);
        }
      }
    </style>
    <div class="container">
      <div class="lvlBlockInner">
        <h1 class="titleText"><?php echo text_lang('FACT', $lang); ?></h1>
        <figure class="image visible-xs"><img src="img/fact-cover-mobile.png"></figure>
      </div>
    </div>
  </div>

  <div class="lvlBlock isFactDetail">
    <div class="container">
      <div class="lvlBlockInner">
        <div class="articleTheme">
          <article>
            <div class="row">
              <div class="col-md-6">
                <section>
                  <h1 class="articleThemeHeader"><?php echo $articles['content_subject']; ?></h1>
                  <p>
                    <?php echo $articles['content_detail']; ?>
                  </p>
                </section>
              </div>
              <div class="col-md-6">
                <?php
                if(@$articles['image'][0]) {
                ?>
                  <figure>
                    <img src="<?php echo base_url('public/uploads/articles/images/'.@$articles['image'][0]['attachment_name']); ?>" alt="<?php echo $articles['image'][0]['attachment_alt']; ?>" title="<?php echo $articles['image'][0]['attachment_title']; ?>">
                  </figure>
                <?php
                }
                ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <?php
                if(@$articles['image'][1]) {
                ?>
                  <figure>
                    <img src="<?php echo base_url('public/uploads/articles/images/'.@$articles['image'][1]['attachment_name']); ?>" alt="<?php echo $articles['image'][1]['attachment_alt']; ?>" title="<?php echo $articles['image'][1]['attachment_title']; ?>">
                  </figure>
                <?php
                }
                ?>
              </div>
              <div class="col-md-6">
                <section>
                  <?php echo $articles['content_detail_2']; ?>
                </section>
              </div>
              <?php
              if($articles['content_detail_3']) {
              ?>
                <div class="col-md-12">
                  <section>
                    <?php echo $articles['content_detail_3']; ?>
                  </section>
                </div>
              <?php
              }
              ?>
            </div>
          </article>
        </div>
        <div class="button">
          <a href="<?php echo site_url('articles/'.$lang); ?>" class="btn btnTheme"><?php echo text_lang('fact_back', $lang); ?></a>
        </div>
      </div>
    </div>
  </div>

  <?php
  if(isset($articles_relate)) {
  ?>
  <div class="lvlBlock isRelatedContent">
    <div class="container">
      <div class="lvlBlockInner">
        <h3 class="lvlBlockHeader h2"><b><?php echo text_lang('RELATED CONTENT', $lang); ?></b></h3>
        <div class="newsThumbnailItems">
          <div class="row">
            <?php 
              for ($i=0; $i < count($articles_relate) ; $i++) :

              $content_subject  = $articles_relate[$i]['content_subject'];
              $content_desc     = ( $articles_relate[$i]['content_detail'] ? substr_utf8($articles_relate[$i]['content_detail'], 0, 170) : '' );
              $content_url      = ( $articles_relate[$i]['content_seo'] ? site_url($articles_relate[$i]['content_seo']) : 'javascript:;' );
              $content_alt      = ( $articles_relate[$i]['content_description'] ? $articles_relate[$i]['content_description'] : '' );

                if($articles_relate[$i]['image']) {

                  $highlight = ( @$articles_relate[$i]['image']['attachment_name'] ? '<img src="'.base_url('public/uploads/articles/images/'.$articles_relate[$i]['image']['attachment_name']).'" alt="'.$articles_relate[$i]['image']['attachment_alt'].'" title="'.$articles_relate[$i]['image']['attachment_title'].'">' : '' );

                } else {

                  $highlight = null;
                }

              $content_highlight = '<figure class="image"><a href="'.$content_url.'">'.$highlight.'</a></figure>';
            ?>
              <div class="col-sm-4">
                <div class="thumbnailTheme isMD">
                  <div class="imageWrap">
                    <?php echo $content_highlight; ?>
                  </div>
                  <div class="captionWrap">
                    <div class="caption">
                      <h3 class="titleText">
                        <a href="<?php echo $content_url; ?>"><span><?php echo $content_subject; ?></span></a>
                      </h3>
                      <div class="desc">
                        <p><?php echo $content_desc; ?></p>
                      </div>
                    </div>
                    <div class="button"><a href="<?php echo $content_url; ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                  </div>
                </div>
              </div>
            <?php endfor; ?>
          </div>
        </div>        
      </div>
    </div>
  </div>
  <?php
  }
  ?>