<div class="lvlBlock aboutCover" id="aboutCover">
  <style scoped>
    @media (min-width:768px) {
      .aboutCover {
        background-image:url(img/about-cover.png);
      }
    }
  </style>
  <div class="container">
    <div class="lvlBlockInner">
      <h1 class="titleText">
        <div class="isHL1">
          <?php echo text_lang('about_history_1', $lang); ?> 
        </div>
        <div class="isHL2">
           <?php echo text_lang('about_history_2', $lang); ?> 
        </div>
      </h1>
      <figure class="image visible-xs"><img src="img/about-cover-mobile.png"></figure>
    </div>
  </div>
</div>
<div class="lvlBlock aboutSimpleText">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="row">
        <div class="col-sm-6">
          <div class="aboutSimpleTextItem">
            <p><?php echo text_lang('about_history_desc_1', $lang); ?></p>
            <p><?php echo text_lang('about_history_desc_2', $lang); ?></p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="aboutSimpleTextItem">
            <p><?php echo text_lang('about_history_desc_3', $lang); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="lvlBlock aboutInThailand" id="aboutInThailand">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="row">
        <div class="col-sm-7 col-sm-push-5 col-md-6 col-md-push-6">
          <div class="caption">
            <h2 class="titleText">
              <div class="isHL1"><?php echo text_lang('about_in_thai_1', $lang); ?></div>
              <div class="isHL2"><?php echo text_lang('about_in_thai_2', $lang); ?></div>
            </h2>
            <div class="desc">
              <p><?php echo text_lang('about_in_thai_desc_1', $lang); ?></p>
              <p><?php echo text_lang('about_in_thai_desc_2', $lang); ?></p>
            </div>
          </div>
        </div>
        <div class="col-sm-5 col-sm-pull-7 col-md-6 col-md-pull-6">
          <figure class="image"><img src="img/about-ThaiMap.png"></figure>
        </div>          
      </div>
    </div>
  </div>
</div>
<div class="lvlBlock aboutGlobal" id="aboutGlobal">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="row">
        <div class="col-sm-4">
          <div class="caption">
            <h2 class="titleText">
              <span class="isHL1"><?php echo text_lang('about_in_world_1', $lang); ?></span>
              <span class="isHL2"><?php echo text_lang('about_in_world_2', $lang); ?></span>
            </h2>
            <table>
              <?php
            if($lang=="EN") {
              ?>
              <tr>
                <td>1980</td>
                <td>Japan</td>
              </tr>
              <tr>
                <td>1982</td>
                <td>Taiwan</td>
              </tr>
              <tr>
                <td>1982</td>
                <td>Hong kong</td>
              </tr>
              <tr>
                <td>1983</td>
                <td>Singapore</td>
              </tr>
              <tr>
                <td>1983</td>
                <td>Middle East</td>
              </tr>
              <tr>
                <td>1987</td>
                <td>Korea</td>
              </tr>
              <tr>
                <td>1989</td>
                <td>Indonesia</td>
              </tr>
              <tr>
                <td>1998</td>
                <td>Thailand</td>
              </tr>
              <tr>
                <td>1999</td>
                <td>Malaysia</td>
              </tr>
              <tr>
                <td>2003</td>
                <td>China</td>
              </tr>
              <tr>
                <td>2007</td>
                <td>Philippine</td>
              </tr>
              <tr>
                <td>2008</td>
                <td>Egypt</td>
              </tr>
              <tr>
                <td>2012</td>
                <td>Vietnam</td>
              </tr>
              <tr>
                <td>2015</td>
                <td>Myanmar</td>
              </tr>
              <tr>
                <td>2015</td>
                <td>Timor-Leste</td>
              </tr>
              <tr>
                <td>2015</td>
                <td>Cambodia</td>
              </tr>
              <?php
            } else {
              ?>
              <tr>
                <td>1980</td>
                <td>ญี่ปุ่น</td>
              </tr>
              <tr>
                <td>1982</td>
                <td>ไต้หวัน</td>
              </tr>
              <tr>
                <td>1982</td>
                <td>ฮ่องกง</td>
              </tr>
              <tr>
                <td>1983</td>
                <td>สิงคโปร์</td>
              </tr>
              <tr>
                <td>1983</td>
                <td>ตะวันออกกลาง</td>
              </tr>
              <tr>
                <td>1987</td>
                <td>เกาหลี</td>
              </tr>
              <tr>
                <td>1989</td>
                <td>อินโดนีเซีย</td>
              </tr>
              <tr>
                <td>1998</td>
                <td>ไทย</td>
              </tr>
              <tr>
                <td>1999</td>
                <td>มาเลเซีย</td>
              </tr>
              <tr>
                <td>2003</td>
                <td>จีน</td>
              </tr>
              <tr>
                <td>2007</td>
                <td>ฟิลิปปินส์</td>
              </tr>
              <tr>
                <td>2008</td>
                <td>อิยิปต์</td>
              </tr>
              <tr>
                <td>2012</td>
                <td>เวียดนาม</td>
              </tr>
              <tr>
                <td>2015</td>
                <td>พม่า</td>
              </tr>
              <tr>
                <td>2015</td>
                <td>ติมอร์ – เลสเต</td>
              </tr>
              <tr>
                <td>2015</td>
                <td>เขมร</td>
              </tr>
              <?php
            }
              ?>
            </table>
          </div>
        </div>
        <div class="col-sm-8">
          <figure class="image"><img src="img/about-World.png"></figure>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="lvlBlock aboutWhyPocari" id="aboutWhyPocari">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="row">
        <div class="col-sm-6">
          <div class="caption">
            <h2 class="titleText">
              <div class="isHL1"><?php echo text_lang('about_why_1', $lang); ?></div>
              <div class="isHL2"><?php echo text_lang('about_why_2', $lang); ?></div>
            </h2>
            <div class="desc">
              <?php echo text_lang('about_why_desc_1', $lang); ?>
            </div>
            <h3 class="titleText">
              <span class="isHL3"><?php echo text_lang('about_why_desc_2_1', $lang); ?></span>
              <span class="isHL4"><?php echo text_lang('about_why_desc_2_2', $lang); ?></span>
            </h3>
            <div class="desc">
              <?php echo text_lang('about_why_desc_2', $lang); ?>
              <?php echo text_lang('about_why_desc_3', $lang); ?>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <figure class="image"><img src="img/about-why0<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png" alt=""></figure>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-sm-push-6">
          <div class="caption">
            <h3 class="titleText">
              <span class="isHL3"><?php echo text_lang('about_can_faster_1_1', $lang); ?></span>
              <span class="isHL4"><?php echo text_lang('about_can_faster_1_2', $lang); ?></span>                
            </h3>
            <div class="desc">
              <?php echo text_lang('about_can_faster_1', $lang); ?>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-pull-6">
          <figure class="image"><img src="img/about-why1.png" alt=""></figure>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="lvlBlock aboutEveryWhere" id="aboutEveryWhere">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="row">
        <div class="col-sm-6">
          <div class="caption">
            <h2 class="titleText">
              <div class="isHL1"><?php echo text_lang('about_everyday_1_1', $lang); ?></div>
              <div class="isHL2"><?php echo text_lang('about_everyday_1_2', $lang); ?></div>
            </h2>
            <div class="desc">
              <?php echo text_lang('about_everyday_1', $lang); ?>
              <ul class="listTheme">
              <?php echo text_lang('about_everyday_2', $lang); ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <figure class="image"><img src="img/about-everyWhere.png"></figure>
        </div>
      </div>
      <figure class="image isIcons"><img src="img/about-icons.png"></figure>
    </div>
  </div>
</div>