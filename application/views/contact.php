  <div class="contact lvlBlock">
    <div class="container">
      <div class="contactInner">
        <h1 class="lvlHeaderPage"><?php echo text_lang('CONTACT US', $lang); ?></h1>
        <address>
          <div>
            <?php echo $address['content_address']; ?>
          </div>
        </address>
        <div class="contactLists">
          <ul class="list-inline">
            <li>
              <a href="<?php echo $address['content_facebook']; ?>" target="_blank">
                <img src="img/contact-social-icon-fb.png" alt="">
                <span>Pocari Sweat Thailand</span>
              </a>
            </li>
            <li>
              <a href="tel:<?php echo $address['content_phone']; ?>">
                <img src="img/contact-social-icon-tel.png" alt="">
                <span><?php echo $address['content_phone']; ?></span>
              </a>
            </li>
            <li>
              <a href="tel:<?php echo $address['content_fax']; ?>">
                <img src="img/contact-social-icon-fax.png" alt="">
                <span><?php echo $address['content_fax']; ?></span>
              </a>
            </li>
          </ul>
        </div>
        <div class="googleMap">
          <div id="map"></div>
        </div>
        <script>
          var map;
          var latLng = {lat: 13.7270401, lng: 100.531447}
          function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
              center: latLng,
              zoom: 17
            });

            createMarker(latLng, 'Thai Otsuka Pharmaceutical' , 'A' );

            function createMarker(latLng, text, label) {
              var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                label: {text: label, color: "white"}
              });
              google.maps.event.addListener(marker, "mouseover", function(evt) {
                var label = this.getLabel();
                label.color="black";
                this.setLabel(label);
              });
                google.maps.event.addListener(marker, "mouseout", function(evt) {
                var label = this.getLabel();
                label.color="white";
                this.setLabel(label);
              });
              return marker;
            }                 
          }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDY7AE3e-2KArsOjo_GgBRI5nRAr1D_uew&callback=initMap"
        async defer></script>        
      </div>
    </div>
  </div>