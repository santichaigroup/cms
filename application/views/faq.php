<div class="lvlBlock isFAQ">
  <div class="container">
    <div class="lvlBlockInner">
      <h1 class="lvlBlockHeader"><?php echo text_lang('FAQS', $lang); ?></h1>
      <div class="lvlBlock isFAQProduct">
        <div class="container">
          <figure>
            <img src="img/faqProduct.png">
          </figure>
        </div>
      </div>
      <div class="faqItems">
        <?php 
        $faqAsk = array(
          text_lang('faq_ques_1', $lang),
          text_lang('faq_ques_2', $lang),
          text_lang('faq_ques_3', $lang),
          text_lang('faq_ques_4', $lang),
          text_lang('faq_ques_5', $lang),
          text_lang('faq_ques_6', $lang),
          text_lang('faq_ques_7', $lang),
          text_lang('faq_ques_8', $lang),
          text_lang('faq_ques_9', $lang)
        );
        $faqAns = array(
          text_lang('faq_anw_1', $lang),
          text_lang('faq_anw_2', $lang),
          text_lang('faq_anw_3', $lang),
          text_lang('faq_anw_4', $lang),
          text_lang('faq_anw_5', $lang),
          text_lang('faq_anw_6', $lang),
          text_lang('faq_anw_7', $lang),
          text_lang('faq_anw_8', $lang),
          text_lang('faq_anw_9', $lang)
        );
        for ($i=0, $j=1; $i < count($faqAsk) ; $i++) : ?>
        <?php
          if($faqAsk[$i]) {
        ?>
          <div class="faqItem <?php echo($i === 0 ? 'isHighlight':''); ?>">
            <div class="faqMessage isAsk">
              <div class="avatar"><div class="avatarInner"><?php echo($j); ?></div></div>
              <div class="text"><div class="textInner"><?php echo($faqAsk[$i]); ?></div></div>
            </div>
            <div class="faqMessage isAnswer">
              <div class="avatar"><div class="avatarInner"><img src="img/faqIconBrand.png"></div></div>
              <div class="text"><div class="textInner"><?php echo($faqAns[$i]); ?></div></div>
            </div>
          </div>
        <?php
            $j++;
          }
        ?>
        <?php endfor; ?>
      </div>
    </div>
  </div>
</div>