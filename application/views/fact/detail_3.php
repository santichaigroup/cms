<div class="lvlBlock factCover">
  <style scoped>
    @media (min-width:768px) {
      .factCover {
        background-image:url(img/fact-cover.png);
      }
    }
  </style>
  <div class="container">
    <div class="lvlBlockInner">
      <h1 class="titleText"><?php echo text_lang('fact_cover_h1', $lang); ?></h1>
      <figure class="image visible-xs"><img src="img/fact-cover-mobile.png"></figure>
    </div>
  </div>
</div>

<div class="lvlBlock isFactDetail">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="articleTheme">
        <article>
          <div class="row">
            <div class="col-md-6">
              <section>
                <h1 class="articleThemeHeader"><?php echo text_lang('fact_detail_3_1', $lang); ?></h1>
                <p><?php echo text_lang('fact_detail_3_2', $lang); ?></p>
              </section>
            </div>
            <div class="col-md-6">
              <figure><img src="img/factDetail-thumbnail-3-0.png" alt=""></figure>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <section class="isHL1 mb-xs-0">
                <h2 class="h3 mt-0"><?php echo text_lang('fact_detail_3_3', $lang); ?></h2>
                <table class="mb-xs-0">
                  <tr>
                    <td><b>6:00</b></td>
                    <td>
                      <?php echo text_lang('fact_detail_3_5', $lang); ?>
                    </td>
                  </tr>
                  <tr>
                    <td><b>12:00</b></td>
                    <td>
                      <?php echo text_lang('fact_detail_3_6', $lang); ?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <div class="row isSmallGap-xs visible-xs">
                        <div class="col-xs-6">
                          <figure class="mb-0"><img src="img/factDetail-thumbnail-3-1.png" alt=""></figure>
                        </div>
                        <div class="col-xs-6">
                          <figure class="mb-0"><img src="img/factDetail-thumbnail-3-2.png" alt=""></figure>
                        </div>
                      </div>  
                    </td>
                  </tr>
                  <tr>
                    <td><b>15:00</b></td>
                    <td>
                      <?php echo text_lang('fact_detail_3_8', $lang); ?>
                    </td>
                  </tr>
                  <tr class="visible-xs">
                    <td><b>18:00</b></td>
                    <td>
                      <?php echo text_lang('fact_detail_3_10', $lang); ?>
                    </td>
                  </tr>
                  <tr class="visible-xs">
                    <td><b>00:00</b></td>
                    <td>
                      <?php echo text_lang('fact_detail_3_12', $lang); ?>
                    </td>
                  </tr>                    
                  <tr class="visible-xs">
                    <td colspan="2">
                      <div class="row isSmallGap-xs visible-xs">
                        <div class="col-xs-6">
                          <figure class="mb-xs-10"><img src="img/factDetail-thumbnail-3-3.png" alt=""></figure>
                        </div>
                        <div class="col-xs-6">
                          <figure class="mb-xs-10"><img src="img/factDetail-thumbnail-3-4.png" alt=""></figure>
                        </div>              
                        <div class="col-xs-6">
                          <figure class="mb-xs-0"><img src="img/factDetail-thumbnail-3-5.png" alt=""></figure>
                        </div>              
                      </div>                      
                    </td>
                  </tr>
                </table>
              </section>  
            </div>
            <div class="col-md-6 hidden-xs">
              <div class="row">
                <div class="col-xs-6">
                  <figure><img src="img/factDetail-thumbnail-3-1.png" alt=""></figure>
                </div>
                <div class="col-xs-6">
                  <figure><img src="img/factDetail-thumbnail-3-2.png" alt=""></figure>
                </div>
              </div>              
            </div>
          </div>
          <div class="row hidden-xs">
            <div class="col-md-6">
              <div class="row">
                <div class="col-xs-6">
                  <figure><img src="img/factDetail-thumbnail-3-3.png" alt=""></figure>
                </div>
                <div class="col-xs-6">
                  <figure><img src="img/factDetail-thumbnail-3-4.png" alt=""></figure>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <figure><img src="img/factDetail-thumbnail-3-5.png" alt=""></figure>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <table>
                    <tr>
                      <td><b>18:00</b></td>
                      <td>
                        <?php echo text_lang('fact_detail_3_10', $lang); ?>
                      </td>
                    </tr>
                    <tr>
                      <td><b>00:00</b></td>
                      <td>
                        <?php echo text_lang('fact_detail_3_12', $lang); ?>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
      <div class="button">
        <a href="<?php echo site_url('articles/'.$lang); ?>" class="btn btnTheme"><?php echo text_lang('fact_back', $lang); ?></a>
      </div>
    </div>
  </div>
</div>