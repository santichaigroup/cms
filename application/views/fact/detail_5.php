<div class="lvlBlock factCover">
  <style scoped>
    @media (min-width:768px) {
      .factCover {
        background-image:url(img/fact-cover.png);
      }
    }
  </style>
  <div class="container">
    <div class="lvlBlockInner">
      <h1 class="titleText"><?php echo text_lang('fact_cover_h1', $lang); ?></h1>
      <figure class="image visible-xs"><img src="img/fact-cover-mobile.png"></figure>
    </div>
  </div>
</div>

<div class="lvlBlock isFactDetail">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="articleTheme">
        <article>
          <div class="row">
            <div class="col-md-6">
              <section>
                <h1 class="articleThemeHeader"><?php echo text_lang('fact_detail_5_1', $lang); ?></h1>
                <p><?php echo text_lang('fact_detail_5_2', $lang); ?></p>
                <p class="hidden-xs"><?php echo text_lang('fact_detail_5_3', $lang); ?></p>
                </section>
            </div>
            <div class="col-md-6">
              <figure><img src="img/factDetail-thumbnail-5-0.png" alt=""></figure>
            </div>
          </div>
          <section class="visible-xs">
            <p><?php echo text_lang('fact_detail_5_4', $lang); ?></p>
          </section>
          <div class="row">
            <div class="col-md-9">
              <figure><img src="img/factDetail-thumbnail-5-2<?php echo ($lang!="TH" ? "-".strtolower($lang) : ''); ?>.png" alt=""></figure>
            </div>
            <div class="col-md-3">
              <figure><img src="img/factDetail-thumbnail-5-3.png" alt=""></figure>
            </div>
          </div>
        </article>
      </div>
      <div class="button">
        <a href="<?php echo site_url('articles/'.$lang); ?>" class="btn btnTheme"><?php echo text_lang('fact_back', $lang); ?></a>
      </div>
    </div>
  </div>
</div>