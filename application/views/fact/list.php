<div class="lvlBlock factCover">
  <style scoped>
    @media (min-width:768px) {
      .factCover {
        background-image:url(img/fact-cover.png);
      }
    }
  </style>
  <div class="container">
    <div class="lvlBlockInner">
      <h1 class="titleText"><?php echo text_lang('fact_cover_h1', $lang); ?></h1>
      <figure class="image visible-xs"><img src="img/fact-cover-mobile.png"></figure>
    </div>
  </div>
</div>

<div class="lvlBlock isFact">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="thumbnailItems">
        <div class="row">
          <?php 
          $thumbnailSize = array(
            'col-sm-6', 
            'col-sm-3', 
            'col-sm-3', 
            'col-sm-3', 
            'col-sm-9'
          );
          $thumbnailLocalClassname = array(
            'noCaption isMD',
            'noCaption isSM',
            'noCaption isSM',
            'noCaption isSM',
            'noCaption isLG'
          );
          $thumbnailLocalLink = array(
            ($lang=="TH"?"น้ำเป็นองค์ประกอบหลักในร่างกายของเรา/TH":"water-is-important/EN"),
            ($lang=="TH"?"หน้าที่ของของเหลวในร่างกาย/TH":"roles-of-body-fluids/EN"),
            ($lang=="TH"?"อย่าให้ร่างกายขาดน้ำ/TH":"don’t-get-dehydration/EN"),
            ($lang=="TH"?"ความกระหายคืออะไร/TH":"thirst-mechanism/EN"),
            ($lang=="TH"?"ภาวะการขาดน้ำแบบแฝง​/TH":"voluntary-dehydration/EN")
          );
          $thumbnailLocalTitle = array(
            text_lang('fact_head_1', $lang),
            text_lang('fact_head_2', $lang),
            text_lang('fact_head_3', $lang),
            text_lang('fact_head_4', $lang),
            text_lang('fact_head_5', $lang)
          );
          $thumbnailLocalImage = array(
            'img/fact-thumbnail-0.png',
            'img/fact-thumbnail-1.png',
            'img/fact-thumbnail-2.png',
            'img/fact-thumbnail-3.png',
            'img/fact-thumbnail-4.png'
          );
          for ($i=0; $i < count($thumbnailSize) ; $i++): 
            $thumbnailCaption = false;
            $thumbnailSubTitle = false;
            $thumbnailTitle = $thumbnailLocalTitle[$i];
            $thumbnailLink = $thumbnailLocalLink[$i];
            $thumbnailImage = $thumbnailLocalImage[$i];
            $thumbnailClassname = $thumbnailLocalClassname[$i];  ?>
          <div class="<?php echo($thumbnailSize[$i]); ?>">
            <div class="thumbnailTheme <?php echo($thumbnailClassname); ?> ">
              <div class="imageWrap">
                <figure class="image"><img src="<?php echo($thumbnailImage); ?>"></figure>
              </div>
              <div class="captionWrap">
                <div class="caption">
                  <h3 class="titleText">
                    <span><?php echo($thumbnailTitle); ?></span>
                    <?php if($thumbnailSubTitle): ?>
                    <small>consectetur adipiscing</small>
                    <?php endif; ?>
                  </h3>
                  <?php if($thumbnailCaption): ?>
                  <div class="desc">
                    <!-- <p>Ut diam sapien, eleifend nonorcivel, fringill apellentesque velit. Inlobortis odio eu augue auctor. Ut diam sapien, </p> -->
                  </div>
                  <?php endif; ?>
                </div>
                <div class="button"><a href="<?php echo site_url($thumbnailLink); ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>  
    </div>
  </div>
</div>