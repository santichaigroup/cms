<div class="lvlBlock factCover">
  <style scoped>
    @media (min-width:768px) {
      .factCover {
        background-image:url(img/fact-cover.png);
      }
    }
  </style>
  <div class="container">
    <div class="lvlBlockInner">
      <h1 class="titleText"><?php echo text_lang('fact_cover_h1', $lang); ?></h1>
      <figure class="image visible-xs"><img src="img/fact-cover-mobile.png"></figure>
    </div>
  </div>
</div>

<div class="lvlBlock isFactDetail">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="articleTheme">
        <article>
          <header>
            <h1 class="articleThemeHeader"><?php echo text_lang('fact_detail_4_1', $lang); ?></h1>
          </header>
          <div class="row">
            <div class="col-md-6">
              <figure><img src="img/factDetail-thumbnail-4-0.png" alt=""></figure>
              <figure><img src="img/factDetail-thumbnail-4-1.png" alt=""></figure>
            </div>
            <div class="col-md-6">
              <section>
                <p><?php echo text_lang('fact_detail_4_2', $lang); ?></p>
              </section>
              <figure><img src="img/factDetail-thumbnail-4-3<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png" alt=""></figure>
              <section class="isHL1">
                <h4><?php echo text_lang('fact_detail_4_3', $lang); ?></h4>
                <ol>
                  <?php echo text_lang('fact_detail_4_4', $lang); ?>
                </ol>
              </section>
            </div>
          </div>
        </article>
      </div>
      <div class="button">
        <a href="<?php echo site_url('articles/'.$lang); ?>" class="btn btnTheme"><?php echo text_lang('fact_back', $lang); ?></a>
      </div>
    </div>
  </div>
</div>