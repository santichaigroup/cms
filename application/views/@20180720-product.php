  <div class="productCover lvlBlock">
    <style scoped="">
    @media (min-width:768px) {
      .productCover {
        background-image:url(img/product-detail-cover<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png);
      }
    }
    </style>
    <div class="container">
      <div class="productCoverInner">
        <figure class="image">
          <img src="img/product-detail-cover-mobile<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="product350 lvlBlock">
    <style scoped="">
    @media (min-width:768px) {
      .product350 {
        background-image:url(img/product-detail-350<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png);
      }
    }
    </style>
    <div class="container">
      <div class="product350Inner">
        <figure class="image">
          <img src="img/product-detail-350-mobile<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png">
        </figure>
      </div>
    </div>
  </div>
  <div class="product500 lvlBlock">
    <style scoped="">
    @media (min-width:768px) {
      .product500 {
        background-image:url(img/product-detail-500<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png);
      }
    }
    </style>
    <div class="container">
      <div class="product500Inner">
        <figure class="image">
          <img src="img/product-detail-500-mobile<?php echo ($lang!="TH"?"-".strtolower($lang):''); ?>.png">
        </figure>
      </div>
    </div>
  </div>