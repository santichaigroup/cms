<div class="lvlBlock isNews">
  <div class="container">
    <div class="lvlBlockInner">
      <div class="lvlBlockHeader hasSearch">
        <div class="row">
          <div class="col-sm-8">
            <h1 class="titleText"><?php echo text_lang('NEWS & ACTIVITY', $lang); ?></h1>
          </div>
          <!-- <div class="col-sm-4">
            <div class="searchBar">
              <form class="formTheme">
                <div class="form-group hasFloatButton">
                  <div class="form-control-floatText">
                    <label>Search</label>
                    <input type="text" class="form-control">
                  </div>
                  <button class="btn"><img src="img/search-icon.png"></button>
                </div>
              </form> 
            </div>
          </div> -->
        </div>
      </div>

    <?php
    if(count($news)>0 && empty($news[0]['content_youtube'])) {

      $j=1;

        $url = ( $news[0]['content_seo'] ? site_url($news[0]['content_seo']) : 'javascript:;' );

        if($news[0]['image']) {

          $highlight = ( $news[0]['image']['attachment_name'] ? base_url('public/uploads/news/images/'.$news[0]['image']['attachment_name']) : '' );
          $alt = ( $news[0]['image']['attachment_alt'] ? $news[0]['image']['attachment_alt'] : '' );
          $title = ( $news[0]['image']['attachment_title'] ? $news[0]['image']['attachment_title'] : '' );
        } else {

          $highlight = null;
          $alt = null;
          $title = null;
        }
    ?>

      <div class="newsThumbnailHighlight">
        <div class="thumbnailTheme isNewsHighlight">
          <div class="imageWrap">
            <figure class="image">
              <a href="<?php echo $url; ?>">
                <?php
                if($highlight) {
                ?>
                  <img src="<?php echo $highlight; ?>" alt="<?php echo $alt; ?>" title="<?php echo $title; ?>">
                <?php
                }
                ?>
              </a>
            </figure>
          </div>
          <div class="captionWrap">
            <div class="caption">
              <h2 class="titleText h3"><?php echo $news[0]['content_subject']; ?></h2>
              <div class="desc">
                <p><?php echo substr_utf8($news[0]['content_short'], 0, 170); ?></p>
              </div>
              <div class="button"><a href="<?php echo $url; ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
            </div>
          </div>
        </div>
      </div>

    <?php
    } else {

      $j=0;
    }
    ?>

      <hr class="lineBetweenBlock">

    <?php
    if(count($news)>1 || $j==0) {
    ?>

      <div class="newsThumbnailItems">
        <div class="row">

          <?php
          for ($i=$j; $i<count($news); $i++) {

            $content_subject  = $news[$i]['content_subject'];
            $content_desc     = ( $news[$i]['content_short'] ? substr_utf8($news[$i]['content_short'], 0, 170) : '' );
            $content_url      = ( $news[$i]['content_seo'] ? site_url($news[$i]['content_seo']) : 'javascript:;' );
            $content_alt      = ( $news[$i]['content_description'] ? $news[$i]['content_description'] : '' );

            if($news[$i]['content_youtube']) {

              $content_show     = "col-sm-6";
              $content_type     = "isEMBED";

              $longUrlRegex     = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

              if (preg_match($longUrlRegex, $news[$i]['content_youtube'], $matches)) {

                $content_highlight = '<div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/'.$matches[count($matches) - 1].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>';
              } else {

                $content_highlight = null;
              }

            } else {

              $content_show = "col-sm-3"; // col-sm-6
              $content_type = "isSM"; // isMD

              if($news[$i]['image']) {

                $highlight = ( $news[$i]['image']['attachment_name'] ? '<img src="'.base_url('public/uploads/news/images/'.$news[$i]['image']['attachment_name']).'" alt="'.$news[$i]['image']['attachment_alt'].'" title="'.$news[$i]['image']['attachment_title'].'">' : '' );
              } else {

                $highlight = null;
              }

              $content_highlight = '<figure class="image"><a href="'.$content_url.'">'.$highlight.'</a></figure>';
            }

          ?>
            <div class="col-sm-6">
              <div class="thumbnailTheme isEMBED">
                <div class="imageWrap">
                  <?php echo $content_highlight; ?>
                </div>
                <div class="captionWrap">
                  <div class="caption">
                    <h3 class="titleText">
                      <?php echo $content_subject; ?>
                    </h3>
                    <div class="desc">
                      <p><?php echo $content_desc; ?></p>
                    </div>
                  </div>
                  <div class="button"><a href="<?php echo $content_url; ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                </div>
              </div>              
            </div>
          <?php
          }
          ?>
        </div>
      </div>

    <?php
    }
    ?>

      <!-- <div class="pagerTheme">
        <ul class="list-inline">
          <li class="isArrow isPrev disabled"><a><span>previous</span></a></li>
          <li><a href=""><span>1</span></a></li>
          <li><a href=""><span>2</span></a></li>
          <li><a ><span>...</span></a></li>
          <li><a href=""><span>99</span></a></li>
          <li class="isArrow isNext"><a href=""><span>next</span></a></li>
        </ul>
      </div> -->

    </div>
  </div>
</div>