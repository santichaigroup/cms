  <div class="lvlBlock isCover">
    <figure class="image">
      <?php
      if(count($news['image'])) {
        foreach ($news['image'] as $key => $value) {
          if($value['type']=="cover") {
      ?>
            <img src="<?php echo base_url('public/uploads/news/images/'.$value['attachment_name']); ?>" alt="<?php echo $value['attachment_alt']; ?>" title="<?php echo $value['attachment_title']; ?>">
      <?php
          }
        }
      }
      ?>
    </figure>
  </div>
  <div class="lvlBlock isArticleDetail">
    <div class="container">
      <div class="lvlBlockInner">
        <article>
          <div class="row">
            <div class="col-lg-10 col-lg-offset-1">

              <?php
              if($news['content_youtube']) {

                $longUrlRegex     = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

                if (preg_match($longUrlRegex, $news['content_youtube'], $matches)) {
              ?>
                <div class="embed-responsive-wrap">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/<?php echo $matches[count($matches) - 1]; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                  </div>
                </div>
              <?php
                }
              }
              ?>

            </div>
          </div>
          <header class="articleHeader">
            <h1><?php echo $news['content_subject']; ?></h1>
            <!-- <p>"Lose to Live" #เติมให้พอไปต่อให้เต็มที่</p> -->
          </header>
          <p>
            <?php echo $news['content_detail']; ?>
          </p>
        </article>
      </div>
    </div>
  </div>

  <?php
  if(isset($news_relate)) {
  ?>
  <div class="lvlBlock isRelatedContent">
    <div class="container">
      <div class="lvlBlockInner">
        <h3 class="lvlBlockHeader h2"><b><?php echo text_lang('RELATED CONTENT', $lang); ?></b></h3>
        <div class="newsThumbnailItems">
          <div class="row">
            <?php 
              for ($i=0; $i < count($news_relate) ; $i++) :

              $content_subject  = $news_relate[$i]['content_subject'];
              $content_desc     = ( $news_relate[$i]['content_short'] ? substr_utf8($news_relate[$i]['content_short'], 0, 170) : '' );
              $content_url      = ( $news_relate[$i]['content_seo'] ? site_url($news_relate[$i]['content_seo']) : 'javascript:;' );
              $content_alt      = ( $news_relate[$i]['content_description'] ? $news_relate[$i]['content_description'] : '' );

                if($news_relate[$i]['content_youtube']) {

                  $longUrlRegex     = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

                  if (preg_match($longUrlRegex, $news_relate[$i]['content_youtube'], $matches)) {

                    $content_highlight = '<div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/'.$matches[count($matches) - 1].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>';
                  } else {

                    $content_highlight = null;
                  }

                } else {

                    if($news_relate[$i]['image']) {

                      $highlight = ( @$news_relate[$i]['image']['attachment_name'] ? '<img src="'.base_url('public/uploads/news/images/'.$news_relate[$i]['image']['attachment_name']).'" alt="'.$news_relate[$i]['image']['attachment_alt'].'" title="'.$news_relate[$i]['image']['attachment_title'].'">' : '' );

                    } else {

                      $highlight = null;
                    }

                    $content_highlight = '<figure class="image"><a href="'.$content_url.'">'.$highlight.'</a></figure>';
                }
            ?>
              <div class="col-sm-6">
                <div class="thumbnailTheme isMD">
                  <div class="imageWrap">
                    <?php echo $content_highlight; ?>
                  </div>
                  <div class="captionWrap">
                    <div class="caption">
                      <h3 class="titleText">
                        <a href="<?php echo $content_url; ?>"><span><?php echo $content_subject; ?></span></a>
                      </h3>
                      <div class="desc">
                        <p><?php echo $content_desc; ?></p>
                      </div>
                    </div>
                    <div class="button"><a href="<?php echo $content_url; ?>" class="btn btnTheme"><?php echo text_lang('news_list_read_more', $lang); ?></a></div>
                  </div>
                </div>
              </div>
            <?php endfor; ?>
          </div>
        </div>        
      </div>
    </div>
  </div>
  <?php
  }
  ?>