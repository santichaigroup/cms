<?php
if(! function_exists('pre')) {
	function pre($a) {
		echo "<pre>";
		var_dump($a);
		echo "</pre>";
		exit();
	}
}

if(! function_exists('zoom_close')) {
	function zoom_close($basepath,$source_file,$output_filename,$width=200,$height=200){
		$CI =& get_instance();
		$CI->load->library('phpthumb/Phpthumb');

		$CI->phpthumb->setSourceFilename($basepath.$source_file);
		$files =  pathinfo($source_file);
		if($files['extension'] == 'png') {
			$CI->phpthumb->setParameter('f', 'png');
		}
		$CI->phpthumb->setParameter('zc', 1);
		$CI->phpthumb->setParameter('w', $width);
		$CI->phpthumb->setParameter('h', $height);
		
		if ($CI->phpthumb->GenerateThumbnail()) {
			$CI->phpthumb->RenderToFile($basepath.$output_filename);
			$CI->phpthumb->purgeTempFiles();
			//reset object if multiple file generate from http://phpthumb.sourceforge.net/demo/docs/phpthumb.faq.txt
			$CI->phpthumb->resetObject();
		}
		return $CI->phpthumb->debugmessages;
	}
}

if(! function_exists('convertnumber_to_text')) {
	function convertnumber_to_text($number){ 
		$txtnum1 = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ'); 
		$txtnum2 = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'); 
		$number = str_replace(",", "", $number); 
		$number = str_replace(" ", "", $number); 
		$number = str_replace("บาท", "", $number); 
		$number = explode(".",$number); 
		if(sizeof($number) > 2){ 
			return 'Too many decimal'; 
			exit; 
		} 

		$strlen = strlen($number[0]); 
		$convert = ''; 
		for($i = 0; $i < $strlen; $i++){ 
			$n = substr($number[0], $i,1); 
			if($n != 0){ 
				if($i == ($strlen - 1) AND $n == 1){
					$convert .= 'เอ็ด';
				} 
				elseif($i == ($strlen - 2) AND $n == 2){
					$convert .= 'ยี่';
				} 
				elseif($i == ($strlen - 2) AND $n == 1){
					$convert .= '';
				} 
				else{
					$convert .= $txtnum1[$n];
				} 
				
				$convert .= $txtnum2[$strlen - $i - 1]; 
			} 
		} 

		$convert .= 'บาท'; 
		if($number[1] == '0' OR $number[1] == '00' OR $number[1] == ''){ 
			$convert .= 'ถ้วน'; 
		}
		else{ 
			$strlen = strlen($number[1]); 
			for($i = 0; $i < $strlen; $i++){ 
				$n = substr($number[1], $i,1); 
				if($n != 0){ 
					if($i == ($strlen - 1) AND $n == 1){
						$convert .= 'เอ็ด';
					} 
					elseif($i == ($strlen - 2) AND $n == 2){
						$convert .= 'ยี่';
					} 
					elseif($i == ($strlen - 2) AND $n == 1){
						$convert .= '';
					} 
					else{ 
						$convert .= $txtnum1[$n];
					} 
					
					$convert .= $txtnum2[$strlen - $i - 1]; 
				} 
			} 
			
			$convert .= 'สตางค์'; 
		} 
		
		return $convert; 
	} 
}

if(! function_exists('setCurrentLanguage')) {
	function setCurrentLanguage($lang) {

		require_once 'language/class/PHPMLC.php';
		$ml = new PHPMLC();

		return $ml->setCurrentLanguage($lang);
	}
}

if(! function_exists('text_lang')) {
	function text_lang($string, $lang) {

		require_once 'language/class/PHPMLC.php';
		$ml = new PHPMLC();

		return htmlspecialchars_decode($ml->getTranslatedString($string, $lang));
	}
}

if(! function_exists('substr_utf8')) {
    function substr_utf8( $str, $start_p , $len_p){
        $ret =  preg_replace(   '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start_p.'}'.
                                '((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len_p.'}).*#s',
                                '$1' , 
                                $str );
        if(strlen($str) > $len_p){
                $ret .= "...";
        }
        return $ret;
    }
}
?>
