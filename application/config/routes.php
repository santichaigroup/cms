<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] 			= "home";
$route['404_override'] 					= "home";

$route['home/TH']							= "home/index/TH";
$route['home/EN']							= "home/index/EN";

$route['about/TH']							= "home/about/TH";
$route['about/EN']							= "home/about/EN";
// $route['articles/TH']						= "fact/index//TH";
// $route['articles/EN']						= "fact/index/EN";
$route['product/TH']						= "home/product/TH";
$route['product/EN']						= "home/product/EN";
$route['faq/TH']							= "home/faq/TH";
$route['faq/EN']							= "home/faq/EN";
$route['whereToBuy/TH']						= "home/whereToBuy/TH";
$route['whereToBuy/EN']						= "home/whereToBuy/EN";
$route['contact/TH']						= "home/contact/TH";
$route['contact/EN']						= "home/contact/EN";

$route['news/TH']							= "news/index/TH";
$route['news/EN']							= "news/index/EN";

$route['articles/TH']						= "articles/index/TH";
$route['articles/EN']						= "articles/index/EN";

/******						Administrator					*******/

$route['src/(:any)'] 					= "sourcefile/index/$1";
$route['admin'] 						= "administrator/admin_route";
$route['administrator'] 				= "administrator/index";
$route['backend'] 						= "administrator/index";

/******						Use Database					*******/

// require_once( BASEPATH .'database/DB'. EXT );
// $db 						=& DB();
// $result 					= $db->get('system_url_rewrite')->result();

// foreach( $result as $row )
// {
// 	$target_main_path 		= $row->target_main_path;
// 	$target_path 			= $row->target_path;
// 	$target_detail_path 	= $row->target_detail_path;

// 	$route[$target_main_path]	= $target_path."/".$target_detail_path;
// }

/******						Use Cache					*******/

require_once APPPATH . 'cache/routes.php';

/******						Use Manual					*******/

$route['น้ำเป็นองค์ประกอบหลักในร่างกายของเรา/TH'] = "fact/fact_detail/1/TH";
$route['water-is-important/EN'] = "fact/fact_detail/1/EN";

$route['หน้าที่ของของเหลวในร่างกาย/TH'] = "fact/fact_detail/2/TH";
$route['roles-of-body-fluids/EN'] = "fact/fact_detail/2/EN";

$route['อย่าให้ร่างกายขาดน้ำ/TH'] = "fact/fact_detail/3/TH";
$route['don’t-get-dehydration/EN'] = "fact/fact_detail/3/EN";

$route['ความกระหายคืออะไร/TH'] = "fact/fact_detail/4/TH";
$route['thirst-mechanism/EN'] = "fact/fact_detail/4/EN";

$route['ภาวะการขาดน้ำแบบแฝง​/TH'] = "fact/fact_detail/5/TH";
$route['voluntary-dehydration/EN'] = "fact/fact_detail/5/EN";

// echo"<pre>";var_dump($route);exit();
/* End of file routes.php */
/* Location: ./application/config/routes.php */