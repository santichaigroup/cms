<?php
class Settings_model extends CI_Model{
	var $_setting = array();
	public function __construct()
	{
		parent::__construct();
		$setting = $this->db->get("system_setting")->result_array();	
		foreach($setting as $row){
			$this->_setting[$row['setting_key']]=$row['setting_value'];	
		}
	}
	public function item($key)
	{
		if(isset($this->_setting[$key])){
			return $this->_setting[$key];
		}else{
			return NULL;
		}
	}
	public function set($key,$value)
	{
		$this->db->set("setting_value",$value);
		if(isset($this->_setting[$key])){
			$this->db->where("setting_key",$key);	
			$this->db->update("system_setting");
		}else{
			$this->db->set("setting_key",$key);	
			$this->db->insert("system_setting");
		}
	}
	public function banner($lang_id="TH", $limit = 1000)
	{
		$this->db->select('banner_content.*, banner_id.*, banner_id.type AS BType'); 
		$this->db->from('banner_id', 'banner_content');
		
		$this->db->join("banner_content","banner_content.main_id = banner_id.main_id", "left");

		$this->db->where("banner_id.main_status", "active");
		$this->db->where("banner_content.content_status", "active");
		$this->db->order_by("banner_id.sequence, banner_id.post_date", "asc");
		$this->db->group_by('banner_id.main_id');
		$this->db->limit($limit);
		return $this->db->get();
	}
	public function insert_subscript($subscritp)
	{
		$this->db->set("faq_email",$subscritp);
		$this->db->set("create_date","NOW()",false);
		$this->db->set("user_ip",$this->input->ip_address());
		$this->db->set("faq_status","new");
		return $this->db->insert("faq");
	}

	public function news($lang_id="TH", $limit = 1000)
	{
		$this->db->select('news_content.*, news_id.*'); 
		$this->db->from('news_id', 'news_content');
		
		$this->db->join("news_content","news_content.main_id = news_id.main_id", "left");

		if($lang_id) {
			$this->db->where("news_content.lang_id", $lang_id);		
		}

		$this->db->where("news_id.main_status", "active");
		$this->db->where("news_content.content_status", "active");
		$this->db->order_by("news_id.sequence, news_id.post_date", "asc");
		$this->db->group_by('news_id.main_id');
		$this->db->limit($limit);
		return $this->db->get();
	}

	public function news_relate($news_id, $lang_id="TH", $limit = 2)
	{
		$this->db->select('news_content.*, news_id.*'); 
		$this->db->from('news_id', 'news_content');
		
		$this->db->join("news_content","news_content.main_id = news_id.main_id", "left");

		if($lang_id) {
			$this->db->where("news_content.lang_id", $lang_id);		
		}

		$this->db->where("news_id.main_id !=", $news_id);
		$this->db->where("news_id.main_status", "active");
		$this->db->where("news_content.content_status", "active");
		$this->db->order_by("news_id.sequence, news_id.post_date", "desc");
		$this->db->group_by('news_id.main_id');
		$this->db->limit($limit);
		return $this->db->get();
	}

	public function news_detail($news_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('news_id', 'news_content');
		
		if($lang_id){
			$this->db->join("news_content","news_content.main_id = news_id.main_id AND news_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("news_content","news_content.main_id = news_id.main_id");
		}
		
		$this->db->where("news_id.main_id",$news_id);
		$this->db->where("news_content.content_status", "active");
		$this->db->where("news_id.main_status",'active');
		$this->db->order_by("news_id.main_id","desc");
		$this->db->limit(1000);
		return  $this->db->get();
	}

	public function news_img($main_id, $type=null, $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg');

		$this->db->select('*');
		$this->db->from('news_id');

		$this->db->join("news_attachment","news_attachment.default_main_id = news_id.main_id ","right");
		
		if($type) {

			$this->db->where("news_attachment.type", $type);
		}

		$this->db->where("news_id.main_id",$main_id);
		$this->db->where_in("news_attachment.attachment_type", $type_file);
		$this->db->where("news_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("news_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("news_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}

	public function wheretobuy($lang_id="TH", $limit = 1000)
	{
		$this->db->select('wheretobuy_content.*, wheretobuy_id.*'); 
		$this->db->from('wheretobuy_id', 'wheretobuy_content');
		
		$this->db->join("wheretobuy_content","wheretobuy_content.main_id = wheretobuy_id.main_id", "left");

		$this->db->where("wheretobuy_id.main_status", "active");
		$this->db->where("wheretobuy_content.content_status", "active");
		$this->db->order_by("wheretobuy_id.sequence, wheretobuy_id.post_date", "asc");
		$this->db->group_by('wheretobuy_id.main_id');
		$this->db->limit($limit);
		return $this->db->get();
	}

	public function articles($lang_id="TH", $limit = 1000)
	{
		$this->db->select('articles_content.*, articles_id.*'); 
		$this->db->from('articles_id', 'articles_content');
		
		$this->db->join("articles_content","articles_content.main_id = articles_id.main_id", "left");

		if($lang_id) {
			$this->db->where("articles_content.lang_id", $lang_id);		
		}

		$this->db->where("articles_id.main_status", "active");
		$this->db->where("articles_content.content_status", "active");
		$this->db->order_by("articles_id.sequence, articles_id.post_date", "desc");
		$this->db->group_by('articles_id.main_id');
		$this->db->limit($limit);
		return $this->db->get();
	}

	public function articles_relate($articles_id, $lang_id="TH", $limit = 3)
	{
		$this->db->select('articles_content.*, articles_id.*'); 
		$this->db->from('articles_id', 'articles_content');
		
		$this->db->join("articles_content","articles_content.main_id = articles_id.main_id", "left");

		if($lang_id) {
			$this->db->where("articles_content.lang_id", $lang_id);		
		}

		$this->db->where("articles_id.main_id !=", $articles_id);
		$this->db->where("articles_id.main_status", "active");
		$this->db->where("articles_content.content_status", "active");
		$this->db->order_by("articles_id.sequence, articles_id.post_date", "desc");
		$this->db->group_by('articles_id.main_id');
		$this->db->limit($limit);
		return $this->db->get();
	}

	public function articles_detail($articles_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('articles_id', 'articles_content');
		
		if($lang_id){
			$this->db->join("articles_content","articles_content.main_id = articles_id.main_id AND articles_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("articles_content","articles_content.main_id = articles_id.main_id");
		}
		
		$this->db->where("articles_id.main_id",$articles_id);
		$this->db->where("articles_id.main_status",'active');
		$this->db->order_by("articles_id.main_id","desc");
		$this->db->limit(1000);
		return  $this->db->get();
	}

	public function articles_img($main_id, $type=null, $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg');

		$this->db->select('*');
		$this->db->from('articles_id');

		$this->db->join("articles_attachment","articles_attachment.default_main_id = articles_id.main_id ","right");
		
		if($type) {

			$this->db->where("articles_attachment.type", $type);
		}

		$this->db->where("articles_id.main_id",$main_id);
		$this->db->where_in("articles_attachment.attachment_type", $type_file);
		$this->db->where("articles_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("articles_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("articles_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}
}