<?php
class Footer_model extends CI_Model
{
	
	public function address($lang="TH"){
		$this->db->select('*'); 
		$this->db->from('address_id');
		
		$this->db->join("address_content","address_content.address_id = address_id.address_id");
		$this->db->where("address_id.address_status","active");	
		$this->db->where("address_content.content_status","active");	
		$this->db->where("address_content.lang_id",$lang);
		$this->db->order_by("address_id.address_id","desc");
		$this->db->limit(2);
		return $this->db->get();
	}
	public function emailAdmin($email_id=NULL){
		$this->db->select('*'); 
		$this->db->from('setting_email_id');
		
		$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id");

		if($email_id) {
			$this->db->where("setting_email_content.content_id", $email_id);	
		}

		$this->db->where("setting_email_id.email_status","active");	
		$this->db->where("setting_email_content.content_status","active");	
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
	public function getTitle($lang)
	{
		if($lang=='EN')
		{
			$this->db->where("setting_key",'web_title_en');
		}else{
			$this->db->where("setting_key",'web_title');
		}

		return $this->db->get('system_setting')->row_array();
	}
	public function getKeyword($lang)
	{
		if($lang=='EN')
		{
			$this->db->where("setting_key",'web_keyword_en');
		}else{
			$this->db->where("setting_key",'web_keyword');	
		}

		return $this->db->get('system_setting')->row_array();
	}
	public function getDescription($lang)
	{
		if($lang=='EN')
		{
			$this->db->where("setting_key",'web_description_en');
		}else{
			$this->db->where("setting_key",'web_description');	
		}

		return $this->db->get('system_setting')->row_array();
	}
	public function getAnalytic()
	{
		$this->db->where("setting_key",'analytic_tracking_add_code');
		return $this->db->get('system_setting')->row_array();
	}
}