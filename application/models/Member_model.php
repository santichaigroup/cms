<?php
class Member_model extends CI_Model
{

	function get_detail_member($member_id, $fb_id=NULL, $guest_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('member');

		if($guest_id) {
			$this->db->where('session_id', $guest_id);
		} else if($fb_id) {
			$this->db->where('fb_id', $fb_id);
		} else {
			$this->db->where('member_id', $member_id);
		}

		$this->db->where('status <>', 'deleted');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	function countDataFb($fb_id)
	{
		$this->db->where('fb_id',$fb_id);
		return $this->db->count_all_results('member');
	}

	function get_address_member($member_id=NULL, $address_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('member_address');

		if($member_id) {
			$this->db->where('member_id', $member_id);
		}

		if($address_id) {
			$this->db->where('id', $address_id);
		}

		$this->db->where('address_status <>', 'deleted');
		$this->db->order_by('id','ASC');
		return $this->db->get();
	}

	function get_detail_member_by_email($member_email)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $member_email);
		$this->db->where('status <>', 'deleted');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	function update_status_member($member_id, $update_by)
	{
		$this->db->set('status', 'active');
		$this->db->set("update_by",$update_by);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where('member_id', $member_id);
		return $this->db->update("member");
	}

	function address_add($member_id,
						$address_no,
						$address_soi,
						$address_road,
						$province_id,
						$district_id,
						$sub_district_id,
						$postal_code,
						$post_by)
	{

		// Insert member address
			$this->db->set("member_id",$member_id);
			$this->db->set("address_no",$address_no);
			$this->db->set("address_soi",$address_soi);
			$this->db->set("address_road",$address_road);
			$this->db->set("sub_district_id",$sub_district_id);
			$this->db->set("district_id",$district_id);
			$this->db->set("province_id",$province_id);
			$this->db->set("postal_code",$postal_code);
			// $this->db->set("address_remark",$address_remark);
			$this->db->set("post_by",$post_by);
			$this->db->set("post_date","NOW()",false);
			$this->db->set("post_ip",$this->input->ip_address());
			$this->db->set("address_status","active");

		$this->db->insert("member_address");
		$member_address = $this->db->insert_id();

		if(!$member_address) {

			show_error("Cannot create address id");
		} else {
			
			$member_address = TRUE;
		}

		return $member_address;
	}

	function address_update($address_id,
						$member_id,
						$address_no,
						$address_soi,
						$address_road,
						$province_id,
						$district_id,
						$sub_district_id,
						$postal_code,
						$update_by)
	{

		// Insert member address
			$this->db->set("member_id",$member_id);
			$this->db->set("address_no",$address_no);
			$this->db->set("address_soi",$address_soi);
			$this->db->set("address_road",$address_road);
			$this->db->set("sub_district_id",$sub_district_id);
			$this->db->set("district_id",$district_id);
			$this->db->set("province_id",$province_id);
			$this->db->set("postal_code",$postal_code);
			// $this->db->set("address_remark",$address_remark);
			$this->db->set("update_by",$update_by);
			$this->db->set("update_date","NOW()",false);
			$this->db->set("update_ip",$this->input->ip_address());
			$this->db->where("address_status","active");
			$this->db->where("member_id", $member_id);
			$this->db->where("id", $address_id);

		$member_address = $this->db->update("member_address");

		if(!$member_address) {

			show_error("Cannot update address id");
		} else {
			
			$member_address = TRUE;
		}

		return $member_address;
	}

	function address_delete($address_id, $member_id, $update_by) {

		// Delete member address
			$this->db->set('address_status', 'deleted');
			$this->db->set("update_by",$update_by);
			$this->db->set("update_date","NOW()",false);
			$this->db->set("update_ip",$this->input->ip_address());
			$this->db->where("member_id", $member_id);
			$this->db->where("id", $address_id);

		$member_address = $this->db->update("member_address");

		if(!$member_address) {

			show_error("Cannot delete address id");
		} else {
			
			$member_address = TRUE;
		}

		return $member_address;
	}

	function register_member(
		// $username,
		$email,
		$password,
		$firstname,
		$lastname,
		// $telephone,
		// $gender,
		$birthdate,
		// $address_no,
		// $address_soi,
		// $address_road,
		// $sub_district_id,
		// $district_id,
		// $province_id,
		// $postal_code,
		// $address_remark,
		// $is_enews,
		$post_by) {

		// Insert member detail

			$this->db->set("member_class", "1");
			$this->db->set("username",$email);
			$this->db->set("email",$email);
			$this->db->set("password",$password);
			$this->db->set("firstname",$firstname);
			$this->db->set("lastname",$lastname);
			// $this->db->set("telephone",$telephone);
			// $this->db->set("gender",$gender);
			$this->db->set("birthdate",$birthdate);
			// $this->db->set("is_enews",$is_enews);

			$this->db->set('member_class_valid_date', date('Y-m-d H:i:s', time()));
			$this->db->set('member_class_expire_date', date('Y-m-d H:i:s', strtotime('+50 year', time())));

			$this->db->set("post_by",$post_by);
			$this->db->set("post_date","NOW()",false);
			$this->db->set("post_ip",$this->input->ip_address());
			$this->db->set("update_by",$post_by);
			$this->db->set("update_date","NOW()",false);
			$this->db->set("update_ip",$this->input->ip_address());
			$this->db->set("status","active");

		$this->db->insert("member");
		$member_id = $this->db->insert_id();

		// Insert member address

		// 	$this->db->set("member_id",$member_id);
		// 	$this->db->set("address_no",$address_no);
		// 	$this->db->set("address_soi",$address_soi);
		// 	$this->db->set("address_road",$address_road);
		// 	$this->db->set("sub_district_id",$sub_district_id);
		// 	$this->db->set("district_id",$district_id);
		// 	$this->db->set("province_id",$province_id);
		// 	$this->db->set("postal_code",$postal_code);
		// 	$this->db->set("address_remark",$address_remark);

		// 	$this->db->set("post_by",$post_by);
		// 	$this->db->set("post_date","NOW()",false);
		// 	$this->db->set("post_ip",$this->input->ip_address());
		// 	$this->db->set("update_by",$post_by);
		// 	$this->db->set("update_date","NOW()",false);
		// 	$this->db->set("update_ip",$this->input->ip_address());
		// 	// $this->db->set("address_status","active");

		// $member_address = $this->db->insert("member_address");


		if(!$member_id) {

			show_error("Cannot create member id");
		} else {
			
			$this->db->set("sequence", $member_id);
			$this->db->where("member_id",$member_id);
			$this->db->update("member");
		}
		return $member_id;
	}

	function register_fb($data, $action=NULL)
	{
		if(empty($action)) {

				$password = $this->randPassword(12);
			// Insert member detail
				$gender = ( $data['fbgender']=="male" ? "M" : "F" );

				$this->db->set("member_class", "1");
				$this->db->set("username",$data['fbemail']);
				$this->db->set("email",$data['fbemail']);

				$this->db->set("password",md5($password));

				$this->db->set("fb_id",$data['fbid']);
				$this->db->set("firstname",$data['fbfname']);
				$this->db->set("lastname",$data['fblname']);
				// $this->db->set("telephone",$telephone);
				$this->db->set("gender",$gender);
				// $this->db->set("birthdate",$birthdate);
				// $this->db->set("is_enews",$is_enews);

				$this->db->set("post_by",'0');
				$this->db->set("post_date",$data['join_date']);
				$this->db->set("post_ip",$data['join_ip']);

				$this->db->set("status","active");
				$this->db->set("accessToken",$data['access_token']);

				$this->db->set('member_class_valid_date', date('Y-m-d H:i:s', time()));
				$this->db->set('member_class_expire_date', date('Y-m-d H:i:s', strtotime('+50 year', time())));

			$this->db->insert("member");
			$member_id = $this->db->insert_id();

			if(!$member_id) {

				show_error("Cannot create member fb_id");
			} else {
				
				$this->db->set("sequence", $member_id);
				$this->db->where("member_id",$member_id);
				$this->db->update("member");
			}

		} else {

				$this->db->set("fb_id",$data['fbid']);
				$this->db->set("accessToken",$data['access_token']);

			$this->db->where('email', $data['fbemail']);
			$this->db->where('username', $data['fbemail']);
			$this->db->update("member");
			
			$this->db->select('member_id');
			$this->db->where('email', $data['fbemail']);
			$this->db->where('username', $data['fbemail']);
			$member_id = $this->db->get('member');
		}

		return $member_id;
	}

	function guest_member(
		$session_id,
		$email,
		$firstname,
		$lastname,
		$telephone,
		$address_no,
		$address_soi,
		$address_road,
		$sub_district_id,
		$district_id,
		$province_id,
		$postal_code,
		// $address_remark,
		// $is_enews,
		$post_by){

		$guest_result = array();

		// Check Guest Member die or not.
		$this->db->select('*');
		$this->db->where('email', $email);
		$check_guest = $this->db->get('member')->row_array();

		if( !$check_guest ) {

			// Insert member detail

			$password = $this->randPassword(8);
			$guest_result['guest_code'] = $password;

				$this->db->set("session_id", $session_id);
				$this->db->set("member_class", "1");
				$this->db->set("username",$email);
				$this->db->set("email", $email);
				$this->db->set("password", md5($password));
				$this->db->set("firstname",$firstname);
				$this->db->set("lastname",$lastname);
				$this->db->set("telephone",$telephone);
				$this->db->set("gender","U");
				// $this->db->set("birthdate",$birthdate);
				// $this->db->set("is_enews",$is_enews);

				$this->db->set("post_by",$post_by);
				$this->db->set("post_date","NOW()",false);
				$this->db->set("post_ip",$this->input->ip_address());
				$this->db->set("update_by",$post_by);
				$this->db->set("update_date","NOW()",false);
				$this->db->set("update_ip",$this->input->ip_address());
				$this->db->set("status","guest");

				$this->db->set('member_class_valid_date', date('Y-m-d H:i:s', time()));
				$this->db->set('member_class_expire_date', date('Y-m-d H:i:s', strtotime('+50 year', time())));

			$this->db->insert("member");
			$guest_result['member_id'] = $this->db->insert_id();

			// Insert member address

				$this->db->set("member_id",$guest_result['member_id']);
				$this->db->set("address_no",$address_no);
				$this->db->set("address_soi",$address_soi);
				$this->db->set("address_road",$address_road);
				$this->db->set("sub_district_id",$sub_district_id);
				$this->db->set("district_id",$district_id);
				$this->db->set("province_id",$province_id);
				$this->db->set("postal_code",$postal_code);
				// $this->db->set("address_remark",$address_remark);

				$this->db->set("post_by",$post_by);
				$this->db->set("post_date","NOW()",false);
				$this->db->set("post_ip",$this->input->ip_address());
				$this->db->set("update_by",$post_by);
				$this->db->set("update_date","NOW()",false);
				$this->db->set("update_ip",$this->input->ip_address());
				$this->db->set("address_status","active");

			$this->db->insert("member_address");
			$guest_result['member_address_id'] = $this->db->insert_id();

			// $guest_result['member_address_id']	= 0;

		} else {

			$guest_result['member_id']			= $check_guest['member_id'];
			$member_address_id 					= $this->get_address_member($check_guest['member_id'])->row_array();
			$guest_result['member_address_id']	= $member_address_id['id'];

			$password = $this->randPassword(8);
			$guest_result['guest_code'] = $password;

			$this->db->set("password", md5($password));
			$this->db->where("member_id", $guest_result['member_id']);
			$this->db->update("member");

		}

		if(!$guest_result['member_id'] && !$guest_result['member_address_id']) {

			show_error("Cannot create member id");

		} else {
			
			// $this->db->set("sequence", $guest_result['member_id']);
			// $this->db->where("member_id", $guest_result['member_id']);
			// $this->db->update("member");
		}

		return $guest_result;
	}

	function update_profile(
		$email,
		$firstname,
		$lastname,
		$telephone,
		$gender,
		$birthdate,
		$post_by,
		$member_id){
		// Insert member detail

		if($email) {
			$this->db->set("username",$email);
			$this->db->set("email",$email);
		}

		if($firstname) {
			$this->db->set("firstname",$firstname);
		}

		if($lastname) {
			$this->db->set("lastname",$lastname);
		}
			
		if($telephone) {
			$this->db->set("telephone",$telephone);
		}

		if($gender) {
			$this->db->set("gender",$gender);
		}

		if($birthdate) {
			$this->db->set("birthdate",$birthdate);
		}
		
			$this->db->set("update_by",$post_by);
			$this->db->set("update_date","NOW()",false);
			$this->db->set("update_ip",$this->input->ip_address());

		$this->db->where("member_id", $member_id);
		$this->db->where('status <>', 'deleted');
		$member_id = $this->db->update("member");

		if(!$member_id) {

			show_error("Cannot update member id");

			return false;
		} else {

			return true;
		}

	}

	function update_password($member_id, $member_password, $post_by)
	{
		$this->db->set("password",md5($member_password));
		$this->db->set("update_by",$post_by);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());

		$this->db->where("member_id", $member_id);
		$this->db->where('status <>', 'deleted');
		$this->db->update("member");

		return true;
	}

	function reset_password($member_id)
	{
		$password = $this->randPassword(7);

		$this->db->set("password", md5($password));
		$this->db->set("update_by",$member_id);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where('member_id', $member_id);
		$this->db->where('status <>', 'deleted');
		$this->db->update("member");

		return $password;
	}

	function randPassword($length)
	{
		$chars = "1234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$password = "";
		while ($i <= $length) {
			$password .= $chars{mt_rand(0,(strlen($chars)-1))};
			$i++;
		}
		return $password;
	}

	function email_telephone_checking($data)
	{

		$member_id = $this->member_library->member_id;

		$this->db->select('*');
		$this->db->from('member');

		if((intval($data)) != 0) {

			$this->db->like('telephone', $data);
		} else {

			if($member_id) {

				$this->db->where('member_id !=', $member_id);
			}
			$this->db->where('email', $data);
		}

		$this->db->where('status', 'active');

		return $this->db->get();
	}

	function email_username_checking($data)
	{
		$this->db->select('*');
		$this->db->from('member');

		if($data) {

			$this->db->where('username', $data);
		}

		$this->db->where('status <>', 'deleted');

		return $this->db->get();
	}

	function country_name($lang_id)
	{
		$this->db->select('*');
		$this->db->from('system_countries');
		if($lang_id=="TH"){
			$this->db->order_by('countries_name_th', 'ASC');
		}else{
			$this->db->order_by('countries_name_en', 'ASC');
		}
		return $this->db->get();
	}

	function province_name($lang_id, $province_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('system_province');

		if($province_id) {
			$this->db->where('province_id', $province_id);
		}

		if($lang_id=="TH"){
			$this->db->order_by('province_name_th', 'ASC');
		}else{
			$this->db->order_by('province_name_en', 'ASC');
		}
		return $this->db->get();
	}

	function district_name($province_id, $lang_id, $district_id)
	{
		$this->db->select('*');
		$this->db->from('system_district');
		if($province_id) {
			$this->db->where('province_id',$province_id);
		}
		if($district_id) {
			$this->db->where('district_id',$district_id);
		} 

		if($lang_id=="TH"){
			$this->db->order_by('district_name_th', 'ASC');
		}else{
			$this->db->order_by('district_name_en', 'ASC');
		}
		return $this->db->get();
	}

	function sub_district_name($district_id, $lang_id, $sub_district_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('system_sub_district');

		if($district_id) {
			$this->db->where('district_id',$district_id);
		} 
		
		if($sub_district_id) {
			$this->db->where('sub_district_id',$sub_district_id);
		}

		if($lang_id=="TH"){
			$this->db->order_by('sub_district_name_th', 'ASC');
		}else{
			$this->db->order_by('sub_district_name_en', 'ASC');
		}
		return $this->db->get();
	}

	function postal_code($sub_district_id)
	{
		$this->db->select('postal_code');
		$this->db->from('system_postal_code');

		$this->db->where('sub_district_id', $sub_district_id);

		return $this->db->get();
	}
	
	function get_registration_promotion() {
		$result = $this->db->where('status', 'active')
							->order_by('id', 'DESC')
							->limit(1)
							->get('promotion_registration');
		
		return $result->row_array();
	}

	function check_coupon_code($coupon_code) {
		return $this->db->where('coupon_code', $coupon_code)->count_all_results('promotions_cart');
	}

	function set_cart_promotion($_param = array()) {
		if(is_array($_param) == false) {
			return false;
		}

		foreach ($_param as $key => $value) {
			$this->db->set($key, $value);
		}

		$this->db->insert("promotions_cart");
		
		$promotions_cart_id = $this->db->insert_id();

		if(!$promotions_cart_id) {
			show_error("Cannot create cart promotions");	
		}
		
		return $promotions_cart_id;
	}
}