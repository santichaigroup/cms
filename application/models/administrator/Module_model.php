<?php
class Module_model extends CI_Model {

	public function list_all_menu()
	{
		$this->db->select("*");
		$this->db->order_by("menu_id","asc");
		$result = $this->db->get("system_menu");
		$entry = array();

		foreach($result->result_array() AS $row ) {

			$sub_entry = array();
			$sub_entry['submenu_entry']=$this->list_all_submenu($row);
			$sub_entry['id'] 		= $row['menu_id'];
			$sub_entry['label'] 	= $row['menu_label'];
			$sub_entry['sequent'] 	= $row['menu_sequent'];
			$sub_entry['title'] 	= $row['menu_title'];
			$sub_entry['link'] 		= $row['menu_link'];
			$entry[] = $sub_entry;
			unset($sub_entry);

		}

		return $entry;
	}

	public function list_all_submenu($menu)
	{
		$this->db->select("*");
		$this->db->where("menu_id",$menu['menu_id']);
		$this->db->order_by("menu_sequent","asc");
		$submenu = $this->db->get("system_submenu");
		$entry = array();

		foreach($submenu->result_array() as $row){
			
			$sub_entry = array();
			$sub_entry['submenu_entry']=array();
			$sub_entry['id'] 		= $row['submenu_id'];
			$sub_entry['label'] 	= $row['menu_label'];
			$sub_entry['sequent'] 	= $row['menu_sequent'];
			$sub_entry['title'] 	= $row['menu_title'];
			$sub_entry['link'] 		= $row['menu_link'];
			$entry[] = $sub_entry;
			unset($sub_entry);
		}

		return $entry;
	}

	public function list_all_layout()
	{
		$this->db->select("*");
		$this->db->order_by("layout_id","asc");
		$query = $this->db->get("system_layout");

		return $query;
	}

	public function add_menu($data)
	{
		$this->db->set("menu_label", $data['menu_label']);
		$this->db->set("menu_title", $data['menu_title']);
		$this->db->set("menu_icon", $data['menu_icon']);
		$this->db->set("menu_link", $data['menu_link']);
		$this->db->set("menu_seo", $data['menu_seo']."/index/TH");
		$this->db->set("menu_sequent", $data['menu_sequent']);
		$this->db->set("menu_status", 'active');
		
		$this->db->insert("system_menu");
		$type_id = $this->db->insert_id();

		if(!$type_id) {
			show_error("Cannot create  type id");	
		}

		return $type_id;
	}

	public function add_sub_menu()
	{

	}

}
?>