<?php 
class Articles_model extends CI_Model {

	public function dataTable($lang_id)
	{
		$this->db->select('*'); 
		$this->db->from('articles_id', 'articles_content');
		
		$this->db->join("articles_content","articles_content.main_id = articles_id.main_id", "left");

		$this->db->where("articles_content.lang_id", $lang_id);
		$this->db->where("articles_id.main_status <>", "deleted");
		$this->db->where("articles_content.content_status <>", "deleted");
		$this->db->order_by("articles_id.sequence, articles_id.post_date", "DESC");
		$this->db->limit(1000);
		return $this->db->get();
	}

	public function getAllContent()
	{
		$this->db->select('main_id');
		$this->db->where('main_status <>','deleted');
		$this->db->order_by('main_id','DESC');
		return $this->db->get('articles_id');
	}

	public function getDetail($articles_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('articles_id', 'articles_content');
		
		if($lang_id){
			$this->db->join("articles_content","articles_content.main_id = articles_id.main_id AND articles_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("articles_content","articles_content.main_id = articles_id.main_id AND articles_content.content_id = articles_id.default_main_id");
		}
		
		$this->db->where("articles_id.main_id",$articles_id);
		$this->db->where("articles_id.main_status <>",'deleted');
		$this->db->order_by("articles_id.main_id","desc");
		$this->db->limit(1000);
		return  $this->db->get()->row_array();
	}

	public function addData()
	{
		$this->db->select("count(*) AS countSequence");
		$this->db->where("main_status <>","deleted");
		$countRow = $this->db->get("articles_id")->row_array();

		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->set("sequence", ($countRow['countSequence']+1) );
		$this->db->insert("articles_id");

		$articles_id = $this->db->insert_id();
		if(!$articles_id) {
			show_error("Cannot create articles id");	
		}

		return $articles_id;
	}

	public function setDate($articles_id)
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$articles_id);

		return $this->db->update("articles_id");	
	}

	public function setStatus($main_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$main_id);
		$this->db->where("lang_id",$lang_id);

		return $this->db->update("articles_content");	
	}

	public function setDefaultContent($articles_id,$lang_id,$content_id)
	{
		$this->db->set("default_main_id",$content_id);
		$this->db->where("main_id",$articles_id);

		return $this->db->update("articles_id");
	}

	public function addLanguage($articles_id,$lang_id)
	{
		$this->db->where("main_id",$articles_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("articles_content");
		
		if($has->num_rows() == 0) {
			$this->db->set("main_id",$articles_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("articles_content");
			return $this->db->insert_id();
		} else {
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
	}

	function updateContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('main_id',$data['main_id']);
		$this->db->where('lang_id',$data['lang_id']);

		$update_content = $this->db->update('articles_content', $data);

		return $update_content;
	}

	function deleteContent(array $data = array())
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("main_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("main_id",$data['main_id']);

		return $this->db->update("articles_id");
	}

	public function getDetail_img($main_id, $type, $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg');

		$this->db->select('*');
		$this->db->from('articles_id');

		// $this->db->join("articles_content","articles_content.main_id = articles_id.main_id AND articles_content.content_id = articles_id.default_main_id");
		$this->db->join("articles_attachment","articles_attachment.default_main_id = articles_id.main_id ","right");

		if($type) {

			$this->db->where("articles_attachment.type", $type);
		}
			
		$this->db->where("articles_id.main_id",$main_id);
		$this->db->where_in("articles_attachment.attachment_type", $type_file);
		$this->db->where("articles_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("articles_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("articles_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}

	public function getDetail_file($main_id,$lang_id=NULL)
	{
		$type_file = array('pdf','doc','docx','rar','zip');

		$this->db->select('*');
		$this->db->from('articles_id');

		$this->db->join("articles_content","articles_content.main_id = articles_id.main_id AND articles_content.content_id = articles_id.default_main_id");
		$this->db->join("articles_attachment","articles_attachment.default_main_id = articles_id.main_id ","right");

		$this->db->where("articles_id.main_id",$main_id);
		$this->db->where_in("articles_attachment.attachment_type", $type_file);
		$this->db->where("articles_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("articles_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("articles_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}
	
	function insertContent($articles_id,$attachment_name,$type,$lang_id=NULL)
	{
		if($attachment_name) {
			$attachment_type = str_replace('.','',strstr($attachment_name,'.'));

			$this->db->set("attachment_name",$attachment_name);
			$this->db->set("attachment_type",$attachment_type);
		}
		$this->db->set("default_main_id",$articles_id);
		$this->db->set("type",$type);
		$this->db->set("lang_id",$lang_id);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		
		return $this->db->insert("articles_attachment");
	}

	public function updateAttachmentContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('attachment_id',$data['attachment_id']);
		// $this->db->where('lang_id',$data['lang_id']);

		return $this->db->update('articles_attachment', $data);
	}
	
	function deleteImage($img_id)
	{
		$this->db->where("attachment_id",$img_id);
		return $this->db->delete("articles_attachment");
	}
	
	function setDefaultImg($default_main_id,$attachment_name,$lang_id)
	{
		$this->db->set("content_thumbnail",$attachment_name);

		$this->db->where("content_id",$default_main_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("articles_content");
	}

	function setSequenceAttachment($id ,$i)
	{
		$this->db->set("sequence",$i);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("attachment_id",$id);
		
		return $this->db->update("articles_attachment");
	}

	function setSequence($main_id,$new_sequence)
	{	
		$this->db->set("sequence",$new_sequence);
		$this->db->where("main_id",$main_id);
		return $this->db->update("articles_id");
	}

} 
?>