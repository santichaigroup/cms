<?php 
class Template_model extends CI_Model {

	public function dataTable($lang_id)
	{
		$this->db->select('*'); 
		$this->db->from('template_id', 'template_content');
		
		$this->db->join("template_content","template_content.main_id = template_id.main_id", "left");

		$this->db->where("template_content.lang_id", $lang_id);
		$this->db->where("template_id.main_status <>", "deleted");
		$this->db->where("template_content.content_status <>", "deleted");
		$this->db->order_by("template_id.main_id", "ASC");
		$this->db->limit(1000);
		return $this->db->get();
	}

	public function getAllContent()
	{
		$this->db->select('main_id');
		$this->db->where('main_status <>','deleted');
		$this->db->order_by('main_id','DESC');
		return $this->db->get('template_id');
	}

	public function getDetail($template_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('template_id', 'template_content');
		
		if($lang_id){
			$this->db->join("template_content","template_content.main_id = template_id.main_id AND template_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("template_content","template_content.main_id = template_id.main_id AND template_content.content_id = template_id.default_main_id");
		}
		
		$this->db->where("template_id.main_id",$template_id);
		$this->db->where("template_id.main_status <>",'deleted');
		$this->db->order_by("template_id.main_id","desc");
		$this->db->limit(1000);
		return  $this->db->get()->row_array();
	}

	public function addData()
	{
		$this->db->select("count(*) AS countSequence");
		$this->db->where("main_status <>","deleted");
		$countRow = $this->db->get("template_id")->row_array();

		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->set("sequence", ($countRow['countSequence']+1) );
		$this->db->insert("template_id");
		$template_id = $this->db->insert_id();
		if(!$template_id) {
			show_error("Cannot create template id");	
		}
		return $template_id;
	}

	public function setDate($template_id,$template_date)
	{
		$template_date = date("Y-m-d",strtotime(date("Y-m-d")));
		$this->db->set("main_date",$template_date);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$template_id);
		return $this->db->update("template_id");	
	}

	public function setStatus($main_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("content_id",$main_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("template_content");	
	}

	public function setDefaultContent($template_id,$lang_id,$content_id)
	{
		$this->db->set("default_main_id",$content_id);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$template_id);
		return $this->db->update("template_id");
	}

	public function addLanguage($template_id,$lang_id)
	{
		$this->db->where("main_id",$template_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("template_content");
		
		if($has->num_rows() == 0){
			$this->db->set("main_id",$template_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("template_content");
			return $this->db->insert_id();
		}else{
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
		
	}

	function updateContent($template_id,$lang_id,$subject,$content_detail,$content_title,$content_keyword,$content_description,$update_date=NULL)
	{
		// ------------------------- SEO Url -----------------------------
		// $subject_specific_word	=	preg_replace("/[^a-zA-Z0-9\wก-๙]+/u", "-", $subject);
		/*
		$english_check_word	=	preg_replace("/[^a-zA-Z]+/", "", $subject);
		if($english_check_word && $lang_id != "TH") {

			$subject_specific_word	=	preg_replace("/[^a-zA-Z0-9]+/", "-", $subject);
			// $count_word = strrpos($subject_specific_word, '-');
			// $subject_specific_word = substr_replace($subject_specific_word, '', $count_word);
			$subject_repeat =	strtolower($subject_specific_word);

			$request_path	=	"unbounded-answers/content/".$subject_repeat;
			$target_path	=	"unbounded/content/".$unbounded_id;

			$this->db->set("request_path", $request_path);
			$this->db->set("target_path", $target_path);
			$this->db->where("target_path",$target_path);
			
			if( $this->db->get("system_url_rewrite")->num_rows() ) {

				$this->db->where("target_path",$target_path);
				$this->db->update("system_url_rewrite");
			} else {

				$this->db->insert("system_url_rewrite");
			}
		} else {

			$subject_repeat = NULL;
		}
		*/
		// ------------------------- SEO Url -----------------------------
		$subject_repeat = NULL;

		$this->db->set("content_subject",$subject);
		$this->db->set("content_detail",$content_detail);
		$this->db->set("content_title",$content_title);
		$this->db->set("content_keyword",$content_keyword);
		$this->db->set("content_description",$content_description);

		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		// if($update_date) {
		// 	$this->db->set("update_date",$update_date);
		// } else {
		// 	$this->db->set("update_date","NOW()",false);
		// }
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$template_id);
		$this->db->where("lang_id",$lang_id);

		$update_content = $this->db->update("template_content");

		$this->db->set("content_seo", $subject_repeat);
		$this->db->where("main_id",$template_id);
		$this->db->update("template_content");

		return $update_content;
	}

	function updateContentStatus($main_id,$lang_id,$status)
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("content_status",$status);
		$this->db->where("main_id",$main_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("template_content");
	}

	function deleteContent($main_id,$lang_id,$status)
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("main_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("main_id",$main_id);
		return $this->db->update("template_id");
	}
	
	function deleteContentSelect($content_id,$lang_id)
	{
		$this->db->where("content_id", $content_id);
		$this->db->where("lang_id", $lang_id);
		$this->db->limit(1);
		$main_id = $this->db->get("template_content")->row_array();

		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("main_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("main_id",$main_id['main_id']);
		return $this->db->update("template_id");		
	}

	public function getDetail_img($main_id, $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png');

		$this->db->select('*');
		$this->db->from('template_id');

		$this->db->join("template_content","template_content.main_id = template_id.main_id AND template_content.content_id = template_id.default_main_id");
		$this->db->join("template_attachment","template_attachment.default_main_id = template_id.main_id ","right");
			
		$this->db->where("template_id.main_id",$main_id);
		$this->db->where_in("template_attachment.attachment_type", $type_file);
		$this->db->where("template_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("template_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("template_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}

	public function getDetail_file($main_id,$lang_id=NULL)
	{
		$type_file = array('pdf','doc','docx','rar','zip');

		$this->db->select('*');
		$this->db->from('template_id');

		$this->db->join("template_content","template_content.main_id = template_id.main_id AND template_content.content_id = template_id.default_main_id");
		$this->db->join("template_attachment","template_attachment.default_main_id = template_id.main_id ","right");

		$this->db->where("template_id.main_id",$main_id);
		$this->db->where_in("template_attachment.attachment_type", $type_file);
		$this->db->where("template_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("template_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("template_attachment.sequence","ASC");
		$this->db->limit(1000);
		return  $this->db->get();					
	}
	
	function imsertContent($template_id,$attachment_name,$lang_id=NULL)
	{
		if($attachment_name) {
			$attachment_type = str_replace('.','',strstr($attachment_name,'.'));

			$this->db->set("attachment_name",$attachment_name);
			$this->db->set("attachment_type",$attachment_type);
		}
		$this->db->set("default_main_id",$template_id);
		$this->db->set("lang_id",$lang_id);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		
		return $this->db->insert("template_attachment");
	}
	function deleteImage($img_id)
	{
		$this->db->where("attachment_id",$img_id);
		return $this->db->delete("template_attachment");
	}
	
	function setDefaultImg($default_main_id,$attachment_name,$lang_id)
	{
		$this->db->set("content_thumbnail",$attachment_name);

		$this->db->where("content_id",$default_main_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("template_content");
	}

	function setSequence($id ,$i)
	{
		$this->db->set("sequence",$i);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("attachment_id",$id);
		
		return $this->db->update("template_attachment");
	}

	function set_sequence($main_id,$new_sequence)
	{	
		$this->db->set("sequence",$new_sequence);
		$this->db->where("main_id",$main_id);
		return $this->db->update("template_id");
	}

} 
?>