<?php
class Member_library{
	private $ci;
	private $setting = array();
	private $admin_session_id = "";
	private $admin_session= array();
	private $breadcrumb= array();
	private $member_isSignin = false;
	private $admin_islogin = false;
	private $admin_issuper = NULL;
	public 	$admin_id = NULL;
	public 	$member_id = NULL;
	public function __construct()
	{
		@ob_start();
		$this->ci = & get_instance();
		$this->ci->load->library('parser');
		//$this->ci->load->library('database');
		$this->setting['body_entry']="";
		$this->setting['get']=array();
		$this->setting['title'] = "Member Profile";	
		$this->setting['asset_url'] = base_url("public/");
		$this->setting['site_url'] = site_url();	
		$this->setting['base_url'] = base_url();	
		$this->setting['admin_url'] = admin_url();	
		$this->setting['current_url'] = site_url($this->ci->uri->uri_string());
		$this->setting['navi_title'] = NULL;
		$this->setting['navi_icon'] = NULL;
		$this->setting['toolbar'] = array();
		$this->setting['company_name']=NULL;
		$this->setting['member_username']=NULL;
		
		$this->member_session_id = "member_" . md5($this->ci->input->server("HTTP_HOST"));
		$this->member_session = $this->ci->session->userdata($this->member_session_id);

		$this->admin_session_id = "admss_" . md5($this->ci->input->server("HTTP_HOST"));
		$this->admin_session = $this->ci->session->userdata($this->admin_session_id);
		$this->admin_session = json_decode(base64_decode($this->admin_session),true);

		if($this->admin_session['user_id']){
			$this->admin_id = $this->admin_session['user_id'];
		}
		
		if($this->member_session){
			$this->member_session=json_decode(base64_decode($this->member_session),true);
			$this->member_id = $this->member_session['member_id'];
			$this->member_isSignin=true;
			$this->_getmemberinfo();
		}

		if(strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") !== false || strpos($_SERVER['HTTP_USER_AGENT'],"Android") !== false){
			$this->setting['desktop']=false;
			$this->setting['mobile']=true;
			$this->setting['tablet']=false;
		}else if(strpos($_SERVER['HTTP_USER_AGENT'],"iPad") !== false){
			$this->setting['desktop']=false;
			$this->setting['mobile']=false;
			$this->setting['tablet']=true;
		}else{
			$this->setting['desktop']=true;
			$this->setting['mobile']=false;
			$this->setting['tablet']=false;
		}

		$this->ci->load->helper('cookie');

		$_store_select_cookie = get_cookie('store_select');

		$_store_select = $this->ci->uri->segment(1) == 'home-cook' ? 1 : 2; 

		if(in_array($this->ci->uri->segment(1), array('home-cook', 'food-service'))) {
			if(empty($_store_select_cookie) || $_store_select != $_store_select_cookie) {
				$_store_select = $this->ci->uri->segment(1) == 'home-cook' ? 1 : 2; 
			}
			else {
				$_store_select = $_store_select_cookie;
			}
		}
		else if($_store_select_cookie) {
			$_store_select = $_store_select_cookie;
		}
		else {
			$_store_select = 2;
		}

		$cookie = array(
			'name'   => 'store_select',
			'value'  => $_store_select,
			'expire' => 35478000, 
			'secure' => false
		);

		set_cookie($cookie);

		$this->ci->session->set_userdata('product_type', $_store_select);
	}

	public function userdata($name){
		if(!$this->admin_session){
			return NULL;
		}
		if(!isset($this->admin_session[$name])){
			return NULL;
		}
		return $this->admin_session[$name];
	}

	public function memberdata($name){
		if(!$this->member_session){
			return NULL;
		}
		if(!isset($this->member_session[$name])){
			return NULL;
		}
		return $this->member_session[$name];
	}

	public function set_userdata($name,$value){
		$this->member_session[$name]=$value;
		$setdata = base64_encode(json_encode($this->member_session));
		$this->ci->session->set_userdata($this->member_session_id,$setdata);
	}

	public function setLogin($user_id)
	{
		$this->set_userdata('member_id',$user_id);

	}
	public function logout()
	{
		$this->ci->session->unset_userdata($this->member_session_id);
		$this->ci->session->sess_destroy();
	}
	public function isSignin()
	{
		return $this->member_isSignin;
	}
	public function isSession_login()
	{
		return $this->ci->session->userdata('session_login');
	}
	public function forceSignin()
	{
		if($this->isSignin()==false){
			redirect(site_url("member/signin/EN"));
		}
	}
	public function assign($name,$value)
	{
		$this->setting[$name] = $value;
	}
	public function setTitle($title,$icon=NULL)
	{
		$this->setting['navi_icon'] = $icon;
		$this->setting['navi_title'] = $title;
		$this->setting['title'] = $title . " Member Sign In";
	}
	public function setDetail($detail)
	{
		$this->setting['page_detail'] = $detail;
	}

	public function authMember($username,$password)
	{

		// if($this->isSession_login()) {

		// 	$this->ci->session->sess_expiration = (60*60*24*365*2);
		// }

		$this->ci->db->where("username",$username);
		$this->ci->db->where("password",$password);
		// $this->ci->db->where("status","active");

		return $this->ci->db->get("member")->row_array();
	}
	
	public function getuserinfo_byemail($email)
	{
		$this->ci->db->where("email",$email);	
		return $this->ci->db->get("member")->row_array();
	}

	public function getuserinfo_byusername($email)
	{
		if((intval($username)) != 0){
			$this->ci->db->where("telephone",$email);
		} else {
			$this->ci->db->where("email",$email);
		}
		return $this->ci->db->get("member")->row_array();
	}

	public function getuserinfo($user_id)
	{

		// $this->ci->db->where("member_id",$user_id);	
		// return $this->ci->db->get("member")->row_array();
		$this->ci->db->select('member.*, member_class.class_name AS class_name, member_class.class_detail AS class_detail, member_class.class_key AS class_key');
		$this->ci->db->from('member', 'member_content');
		$this->ci->db->join("member_class","member.member_class=member_class.id", "left");
		
		$this->ci->db->where("member.member_id",$user_id);
		$this->ci->db->where("member.status <>",'deleted');
		return  $this->ci->db->get()->row_array();

	}

	public function check_username($username)
	{
		if((intval($username)) != 0){
			$this->ci->db->where("telephone",$email);
		} else {
			$this->ci->db->where("email",$email);
		}
		$this->ci->db->where("status <>","delete");	
		return $this->ci->db->count_all_results("member");
	}

	private function _getuserinfo()
	{
		$user_id = $this->userdata("user_id");
		$this->ci->db->select("system_users.user_id,system_users.username,system_users.user_fullname,system_users.user_email,system_users.user_mobileno,system_users.user_status,system_users.user_joindate,system_users.user_group,system_group.company_id");
		$this->ci->db->join("system_group","system_group.group_id=system_users.user_group");
		$this->ci->db->where("system_users.user_id",$user_id);
		$this->setting['user_info'] = $this->ci->db->get("system_users")->row_array();
		if($this->setting['user_info']){
			$grp = $this->getGroupDetail($this->setting['user_info']['user_group']);
			$this->setting['user_company']= $grp['company_id'];
			if(!$this->setting['user_info']){
				$this->admin_islogin=false;	
				return false;
			}
			return $this->setting['user_info'];
		} else {
				$this->admin_islogin=false;	
				return false;
		}
	}

	private function _getmemberinfo()
	{
		$user_id = $this->memberdata("member_id");
		$this->ci->db->select("member.member_id,member.telephone,member.firstname,member.email,member.lastname,member.status,member.post_date,member.post_by");
		$this->setting['user_info'] = $this->ci->db->get("member")->row_array(); 
		if(!$this->setting['user_info']){
			$this->member_isSignin=false;	
			return false;
		}
		return $this->setting['user_info'];
	}

	public function getLanguageList()
	{
		return $this->ci->db->get("system_language")->result_array();	 
	}
	public function getLanguagename($lang_id)
	{
		$this->ci->db->where("lang_id",$lang_id);
		$lang = $this->ci->db->get("system_language")->row_array();	 
		return $lang['lang_name'];
	}
	public function getLanguageflag($lang_id)
	{
		$this->ci->db->where("lang_id",$lang_id);
		$lang = $this->ci->db->get("system_language")->row_array();	 
		return $lang['lang_flag'];
	}

	public function getUser($user_id)
	{
		$this->ci->db->where("user_id",$user_id);
		$user = $this->ci->db->get("system_users")->row_array();	 
		return $user['user_fullname'];	
	}

	public function sub_district_name($district_id, $lang_id, $sub_district_id=NULL)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('system_sub_district');

		if($district_id) {
			$this->ci->db->where('district_id',$district_id);
		} 
		
		if($sub_district_id) {
			$this->ci->db->where('sub_district_id',$sub_district_id);
		}

		if($lang_id=="TH"){
			$this->ci->db->order_by('sub_district_name_th', 'ASC');
		}else{
			$this->ci->db->order_by('sub_district_name_en', 'ASC');
		}
		return $this->ci->db->get();
	}

	public function postal_code($sub_district_id)
	{
		$this->ci->db->select('postal_code');
		$this->ci->db->from('system_postal_code');

		$this->ci->db->where('sub_district_id', $sub_district_id);

		return $this->ci->db->get();
	}

	public function district_name($province_id, $lang_id, $district_id)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('system_district');
		if($province_id) {
			$this->ci->db->where('province_id',$province_id);
		}
		if($district_id) {
			$this->ci->db->where('district_id',$district_id);
		} 

		if($lang_id=="TH"){
			$this->ci->db->order_by('district_name_th', 'ASC');
		}else{
			$this->ci->db->order_by('district_name_en', 'ASC');
		}
		return $this->ci->db->get();
	}

	public function province_name($lang_id, $province_id=NULL)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('system_province');

		if($province_id) {
			$this->ci->db->where('province_id', $province_id);
		}

		if($lang_id=="TH"){
			$this->ci->db->order_by('province_name_th', 'ASC');
		}else{
			$this->ci->db->order_by('province_name_en', 'ASC');
		}
		return $this->ci->db->get();
	}

	public function substr_utf8($str,$start_p,$end_p) { 

		$str = str_replace("&nbsp;", "",strip_tags($str));
		$str = preg_replace('/^([A-Za-z0-9]+)\+([A-Za-z0-9]+)\+(.*)/', '\\1+\\2', $str);

        $stringLeng = mb_strlen($str, 'utf-8');
        return mb_substr($str, $start_p, $end_p, 'utf-8');
 	}

	public function verify_captcha($loading=NULL)
	{
		if($loading=="yes") {
			
			$text = "abcdefhjmnprtuwxyz123456789";
			$vals = array(
						'word' => substr(str_shuffle($text),0,5),
						'font_size' => 20,
						'img_path' => './public/uploads/captcha/',
						'img_url' => site_url().'public/uploads/captcha/',
						'font_path' =>'./public/fonts/Verdana.ttf', 
						'img_width' => '160',
						'img_height' => '50',
						'expiration' => 7200
						);
			$cap = create_captcha($vals);
			$this->ci->session->set_userdata('captcha',base64_encode(base64_encode($cap['word'])));
			if($this->ci->input->get('vercapt')=='getcapt' && $this->ci->input->get('vercapt')!='')
			{
				echo $cap['image'];//Get Core Ajax
			}else{
				return $cap['image'];
				//return base64_encode(base64_encode($cap['word']));
			}
		}
	}

	public function getAllUser()
	{
		$_users = $this->ci->db->get("member")->result_array();	 
		return $_users;	
	}

    public function fullThaiDate(&$data, $lang="EN") {

        if(!$data){
                $data = "-";
        }
        else if($data == "0000-00-00") {
                $data = "-";
        }
        else {

        	if($lang=="TH") {

		        $m = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
		        $day = date("d",strtotime($data));
		        $month = date("m",strtotime($data));
		        $year = date("Y",strtotime($data));
		        $year = ceil($year+543);
		        $month = $m[ceil($month-1)];
		    } else {

		        $m = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		        $day = date("d",strtotime($data));
		        $month = date("m",strtotime($data));
		        $year = date("Y",strtotime($data));
		        $year = ceil($year);
		        $month = $m[ceil($month-1)];
		    }
	        $data = sprintf("%d %s %s",$day,$month,$year);
        }

        return $data;
    }
}