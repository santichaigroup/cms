<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Cache_library {

	private $ci;
	private $_data = array();

	public function __construct()
	{
		$this->ci = & get_instance();
		$uri = $this->ci->uri->segment_array();

		// $this->checkAndCache($uri, $this->ci->session->session_id);
	}

	private function checkAndCache($uri_array, $session_id)
	{
		$uri_path = "";
		foreach ($uri_array as $key => $rs_uri) {

			$uri_path .= $rs_uri.(count($uri_array)!=$key?"/":"");
		}

		$this->ci->load->driver('cache');

		if($this->ci->cache->apc->is_supported()) {

	        if ($this->ci->cache->apc->get($uri_path)) {

	        } else {

				$this->ci->cache->apc->save($uri_path, $this->ci->session->session_id, 30);
	        }

	    } else {

	    	echo "APC not support";exit();
	    }
	}
}