/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

// http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Toolbar

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'th';
	// config.uiColor = '#AADC6E';
	// config.toolbar_Full =
	// [
	// 	{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
	// 	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	// 	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	// 	{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
	//         'HiddenField' ] },
	// 	'/',
	// 	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	// 	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	// 	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	// 	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	// 	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	// 	'/',
	// 	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	// 	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	// 	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
	// ];
	
	// Add Fonts
	config.extraPlugins = 'justify';
	config.toolbar = 'MyToolbar';

	config.toolbar_MyToolbar =
	[
		{ name: 'document', items : [ 'Source','Print'] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
	 	       'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', '-', 'NumberedList','BulletedList',
		'-','Outdent','Indent','-','Blockquote','CreateDiv'] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'insert', items : [ 'Table' ] }
		// { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
	];

	// config.contentsCss = base_url+'public/panel/css/styles.css';
	// config.font_names = 'SCG Fonts/scg_fonts;' + config.font_names;

	// config.toolbar_Basic =
	// [
	// 	['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About']
	// ];

	// config.width=800;
	// config.height=200;
	// config.removeButtons = 'Underline,Subscript,Superscript,Save,NewPage,DocProps,Preview,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,Image,Flash,Smiley,PageBreak,Iframe,About,ShowBlocks,-';

	//config.filebrowserBrowseUrl = admin_url+'filemanager/';
	config.ImageBrowser = false;
	config.filebrowserImageUploadUrl = admin_url+'filemanager/upload';
	config.baseurl = base_url;

	config.removeButtons = 'BrowseServer';
};
