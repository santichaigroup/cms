var _html = $('html'),
    _window = $(window);
    
$(document).ready(function() {

  // Main navigation
  $('[data-toggle="mainNavigation"]').click(function(event) {
    var $this = $(this);
    if (!_html.hasClass('mainNavigationOpen')) {
      $this.addClass('active');
      _html.addClass('mainNavigationOpen');
    }else {
      $this.removeClass('active');
      _html.removeClass('mainNavigationOpen');
    }
  });
  smoothScroll('[data-toggle="navSmoothScroll"]', function () {
    _html.removeClass('mainNavigationOpen');
    $('[data-toggle="mainNavigation"]').removeClass('active');
  });
  formFocus();
});

function formFocus () {
  // form focus
  container = '.form-control-floatText';
  $input =  $(container).find('.form-control');
  $input.focusout(function() {
    $(container).removeClass('focus');
  });
  $input.focus(function() {
    $(this).parents(container).addClass('focus');
  });
  $input.on('change keydown keyup',function(e) {
    if($(this).val().length > 0){
      $(this).parents(container).addClass('filled');
    }
    else{
      $(this).parents(container).removeClass('filled');
    }
  });
  $('.selectpicker').on('show.bs.select', function (e) {
    $(this).parents(container).addClass('focus');
    $(this).parents(container).removeClass('filled');
  });
  $('.selectpicker').on('hide.bs.select', function (e) {
    $(this).parents(container).removeClass('focus');
    if($(this).val().length > 0){
      $(this).parents(container).addClass('filled');
    }
  });
}
function smoothScroll (selector, beforeScroll) {
  $(selector).click(function(event) {
    event.preventDefault();
    var target = $(this).attr('href') || $(this).data('target');
    beforeScroll();
    $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 1000, function () {
    });
  });  
}

function detectOrientation (selector) {
  $(selector).each(function (index, element) {
    var img = this;
    var $img = $(this);
    $img.addClass('detectOrientation');
    $(window).load(function () {
      var orientation;
      if (img.naturalWidth > img.naturalHeight) {
        orientation = 'isLandscape';
      } else if (img.naturalWidth < img.naturalHeight) {
        orientation = 'isPortrait';
      } else {
        orientation = 'isSquare';
      }
      $img.addClass(orientation + ' detectOrientationDone');
    });
  });
}